type pair = {
    key:string;
    value:string
  }

type config = {
    model : Model.t;
    vars : Lang.F.var list;
    bvars : Lang.F.Vars.elt list;
    pool : Lang.F.pool;
    blabels : pair list;
  } 

(* the string list is the written formula in the console *)
val mk_term : config -> string list -> Lang.F.term
val mk_config : Wpo.t -> config
val make_term_from_lexpr : config -> Logic_ptree.lexpr -> Lang.F.term
(*val get_function_from_wpo : Wpo.t -> string -> Lang.F.Fun.t *)
(*val get_lfuns : Model.model -> Lang.F.Fun.t list*)
val find_lfun : Model.model -> string -> Lang.Fun.t
