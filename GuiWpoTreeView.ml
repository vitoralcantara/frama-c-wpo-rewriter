open Gobject.Data
open Rewriter_utils
open InfRules
open WpoRewriter_infRules_names
       
type triple = {
    gid : string;
    pretty : string;
    t : string
  }

let get_pixbuf verdict =
  let open VCS in
  let icn_stock name = [`STOCK_ID name] in
  let icn_status s = [`PIXBUF(Gtk_helper.Icon.get
				(Gtk_helper.Icon.Feedback s))]
  in
  match verdict with
  | Checked | NoResult -> icn_status Property_status.Feedback.Never_tried
  | Valid    -> icn_status Property_status.Feedback.Valid
  | Invalid  -> icn_status Property_status.Feedback.Invalid
  | Unknown  -> icn_status Property_status.Feedback.Unknown
  | Failed   -> icn_stock "gtk-dialog-warning"
  | Timeout | Stepout -> icn_stock "gtk-cut"
  | Computing _ -> icn_stock "gtk-execute"

let cols_tree = new GTree.column_list
let col_wpo_gid:triple GTree.column = cols_tree#add caml
let col_wpo_result:VCS.verdict GTree.column  = cols_tree#add caml

							     
class guiWpoTreeView () = object(self)
				  
  val wpo_tree_model = GTree.tree_store cols_tree
  val wpo_tree_view = GTree.view ()
  val scrolled_window = GBin.scrolled_window  ()
					      
  initializer

    scrolled_window#add wpo_tree_view#coerce;
    wpo_tree_view#set_model (Some wpo_tree_model#coerce);
    ignore (wpo_tree_view#connect#destroy ~callback: self#destroy);
    wpo_tree_view#selection#set_mode `BROWSE;  
    let func2 cell (model:GTree.model) (row:Gtk.tree_iter) :unit =
      let e = model#get ~row ~column:col_wpo_result in
      let res = get_pixbuf e in 
      cell#set_properties res
    in
    let cell_result = GTree.cell_renderer_pixbuf [] in
    let col1 = GTree.view_column ~title:"Result:" ~renderer:(cell_result,[]) () in
    col1#set_cell_data_func cell_result (func2 cell_result);
    
    let func cell (model:GTree.model) (row:Gtk.tree_iter) :unit =
      let e = model#get ~row ~column:col_wpo_gid in
      if wpo_tree_model#iter_depth row = 0 then cell#set_properties
						  [`TEXT e.pretty] 
      else cell#set_properties [`TEXT e.t]
    in
    let cell = GTree.cell_renderer_text [] in
    let col0 = GTree.view_column ~title:"Proof Obligations:"
				 ~renderer:(cell,[]) () in
    col0#set_cell_data_func cell (func cell);
    
    col0#set_resizable true;
    col1#set_resizable true;
    ignore (wpo_tree_view#append_column col1);
    ignore (wpo_tree_view#append_column col0);
    self#select_first_wpo ()

  method destroy () = wpo_tree_model#clear ();
			  
  method update_result_icon result iter =
    wpo_tree_model#set ~row: iter ~column: col_wpo_result result;
    iter

  method wpo_tree_view_changed (reload_function:Wpo.t -> unit) ():unit =
    try
      let row = self#get_first_selected_iter () in
      let gid = self#get_po_gid_from_row row in
      let wpo = get_wpo_by_po_gid gid in
      reload_function wpo;
      self#select wpo.Wpo.po_gid;
    with _ -> ()

  method select_first_wpo () =
    match wpo_tree_model#get_iter_first with
    |Some iter -> wpo_tree_view#selection#select_iter iter
    |None -> ()

  (*
    method on_wpo_tree_view_clicked guiWpoTreeView ev : bool =
      if GdkEvent.Button.button ev = 3 then
	(
	  let clicked_point = get_path_of_clicked_point (guiWpoTreeView#get_view ()) ev in
	  match clicked_point with
	  | Some path ->
	     (
	       let po_gid = guiWpoTreeView#get_wpo_gid_by_path path in
	       let menu = GMenu.menu () in
	       let prune_item = GMenu.menu_item
				  ~label:"Undo"
				  ~packing: menu#append () in
	       ignore
		 (prune_item#connect#activate ~callback: 
					      (undo po_gid guiWpoTreeView));
	       menu#popup ~button:
			  (GdkEvent.Button.button ev) 
			  ~time: (GdkEvent.Button.time ev);
	       true
	     )
	  | None -> false
	)
      else false
   *)
	       
  method register_reload_function (reload_function:Wpo.t -> unit) =
    wpo_tree_view#selection#connect#changed
      ~callback:(self#wpo_tree_view_changed reload_function)
      
  method pack (packer:GObj.widget -> unit) = packer scrolled_window#coerce

  method get_first_selected_iter ():Gtk.tree_iter =
    let selected_rows = wpo_tree_view#selection#get_selected_rows in
    match selected_rows with
    | [] -> raise (Failure "No row is selected")
    | path::_ -> wpo_tree_model#get_iter path
					 
  method add_wpo_in_wpo_tree_model (parent: Gtk.tree_iter option) (wpo: Wpo.po):Gtk.tree_iter =
    let part = WpPropId.parts_of_id wpo.Wpo.po_pid in
    let suffix = (
      if wpo.Wpo.po_leaves <> [] then "" 
      else match part with
	   | None -> ""
	   | Some (p,t) -> (string_of_int (succ p)) ^ "/" ^ (string_of_int t) 
    ) in
    match parent with
    | None ->(
      let iter = wpo_tree_model#append () in
      let result = get_verdict wpo in
      let v = {
	  gid = wpo.Wpo.po_gid;
	  pretty = (Pretty_utils.to_string Wpo.pp_title wpo) ^ " " ^ suffix;
	  t = "";
	} in
      wpo_tree_model#set ~row: iter ~column: col_wpo_gid v;
      self#update_result_icon result iter
    )
    | Some parent -> (
      match wpo.Wpo.po_partac with
      | None -> raise (Failure "po child in list view must have a parent")
      | Some (_,_,tac) ->
	 let iter = wpo_tree_model#append ~parent () in
	 let result = get_verdict wpo in
	 let v:triple = {
	     gid = wpo.Wpo.po_gid;
	     pretty = Pretty_utils.to_string Wpo.pp_title wpo;	   
	     t = ((name_of_tactic tac) ^ " " ^ suffix);
	   } in
	 wpo_tree_model#set ~row: iter ~column: col_wpo_gid v;
	 self#update_result_icon result iter
    )

  method get_selected_iter (view:GTree.view) () =
    let selection = view#selection in
    if selection#count_selected_rows = 1 then
      match selection#get_selected_rows with
      | [tree_path] ->
	 let tree_iter = wpo_tree_model#get_iter tree_path in
	 tree_iter
      | _ -> raise (Failure "A wpo should be selected in the wpo_tree_view")
    else raise (Failure "At least one element must be selected")

  method get_iter_from_gid (wpo_gid:triple) =
    let iter_ref = ref None in
    let compare _ iter =
      let gid = wpo_tree_model#get ~row: iter ~column: col_wpo_gid in
      if gid = wpo_gid then
	(
	  iter_ref := Some iter;
	  true
	)
      else
	false
    in
    wpo_tree_model#foreach compare;
    match !iter_ref with
    | Some iter -> iter
    | None -> raise (Failure "It should have a wpo in the WpoSet")

  method is_root (wpo_gid:triple) =
    let iter = self#get_iter_from_gid wpo_gid in
    if wpo_tree_model#iter_depth iter = 0 then
      true
    else
      false

  method add_wpos_in_wpo_view_from_wpo ?parent: (p: Gtk.tree_iter option = None) (wpo: Wpo.po) =
    let parent_iter = self#add_wpo_in_wpo_tree_model p wpo in
    let leaves = wpo.Wpo.po_leaves in 
    if List.length leaves > 0 then
      List.iter (self#add_wpos_in_wpo_view_from_wpo ~parent: (Some parent_iter)) leaves

  method reload () =
    wpo_tree_model#clear ();
    let inserted_wpos = ref [] in
    let add_in_view = fun wpo ->
      (
	let root = get_root wpo in
	if not
	     (List.exists (fun po_gid -> root.Wpo.po_gid = po_gid) !inserted_wpos)
	then
	  (
	    self#add_wpos_in_wpo_view_from_wpo root;
	    inserted_wpos := root.Wpo.po_gid:: !inserted_wpos;
	  )
      )
    in
    Wpo.iter_on_goals add_in_view
		      
  method select_wpo_in_wpo_tree (wpo_tree_view:GTree.view) po_gid path iter =
    let p = self#get_item_from_wpo_tree_model iter col_wpo_gid in
    if  p.gid = po_gid then
      (
	wpo_tree_view#expand_to_path path;
	wpo_tree_view#selection#select_iter iter;
	(*show_selected_wpo wpo_tree_view path;*)
	true
      )
    else
      false
	
  method select (po_gid:string):unit =
    match wpo_tree_model#get_iter_first with
    | None -> ()
    | Some _ -> ignore
		  (wpo_tree_model#foreach 
		     (self#select_wpo_in_wpo_tree wpo_tree_view po_gid))

  method selectNextUnproven (w:Wpo.po):unit =
    let found = ref false in
    ignore (
	wpo_tree_model#foreach(
	    fun path iter ->
	    let p = self#get_item_from_wpo_tree_model iter col_wpo_gid in
	    if not !found then (
	      if p.gid = w.Wpo.po_gid then found := true;
	      false
	    )
	    else (
	      let wpo = get_wpo_by_po_gid p.gid in
	      let res = get_result wpo in
	      if res.VCS.verdict = VCS.Valid then
		wpo_tree_view#expand_to_path path;
	      wpo_tree_view#selection#select_iter iter;
	      true
	    ))
      )
	   
  method get_item_from_wpo_tree_model (row:Gtk.tree_iter) (column:triple GTree.column) =
    wpo_tree_model#get ~row ~column

  method get_po_gid_from_row (row:Gtk.tree_iter):string =
    (self#get_item_from_wpo_tree_model row col_wpo_gid).gid

  method selected_wpo_in_wpo_tree_has_child (): bool =
    let wpo_selection = wpo_tree_view#selection in
    if wpo_selection#count_selected_rows <= 1 then
      match wpo_selection#get_selected_rows with
      | tree_path::_ ->
	 let tree_iter = wpo_tree_model#get_iter tree_path in
	 wpo_tree_model#iter_has_child tree_iter
      | [] -> raise (Failure "A wpo should be selected in the wpo_tree_view")
    else
      raise (Failure "Should be selected at max one row in the wpo_tree_view")

  method set_result (selected_path:Gtk.tree_path) result =
    let selected_iter = wpo_tree_model#get_iter selected_path in
    self#update_result_icon result selected_iter
			    
  method update_wpo_status_in_tree_view (wpo:Wpo.po) result =
    let selected_paths = wpo_tree_view#selection#get_selected_rows in
    match selected_paths with
    | [] -> raise (Failure "at least one PO must be selected in tree view")
    | hd::_tl -> 
       let selected_po_gid = self#get_wpo_gid_by_path hd in
       if selected_po_gid <> wpo.Wpo.po_gid then
	 (
	   raise (Failure "selected po in tree view does not match proven wpo")
	 )
       else
	 self#set_result hd result

  method grab_focus () = wpo_tree_view#misc#grab_focus ()
						       
  method get_wpo_gid_by_path (path:Gtk.tree_path):string  =
    let iter = wpo_tree_model#get_iter path in
    let p = self#get_item_from_wpo_tree_model iter col_wpo_gid in
    p.gid
      
end
			    
let getNewWpoTreeView () = new guiWpoTreeView ()

