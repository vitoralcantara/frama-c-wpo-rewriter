class guiConsole : unit ->
		   object
		     method get : unit -> GBin.scrolled_window
		     method grab_focus : unit -> unit
		     method set_key_press_action :
			      callback:(GdkEvent.Key.t -> bool) -> GtkSignal.id
		     method source : unit -> GSourceView2.source_buffer
		   end
