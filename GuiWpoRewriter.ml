open InfRules
open Gobject.Data
open WpoRewriter_infRules_names
open Rewriter_utils

(*Current wp shown in the GUI *)
let current_wp = ref None

(*Function called to update wpo status outside WPTrans *)
let update_function = ref (fun () -> ())

let get_current_wpo () =
  match !current_wp with
  | Some w -> w
  | _ -> raise Not_found

let get_pixbuf verdict =
  let open VCS in
  let icn_stock name = [`STOCK_ID name] in
  let icn_status s = [`PIXBUF(Gtk_helper.Icon.get
				(Gtk_helper.Icon.Feedback s))]
  in
  match verdict with
  | Checked | NoResult -> icn_status Property_status.Feedback.Never_tried
  | Valid    -> icn_status Property_status.Feedback.Valid
  | Invalid  -> icn_status Property_status.Feedback.Invalid
  | Unknown  -> icn_status Property_status.Feedback.Unknown
  | Failed   -> icn_stock "gtk-dialog-warning"
  | Timeout | Stepout -> icn_stock "gtk-cut"
  | Computing _ -> icn_stock "gtk-execute"

let tactic_filename = ref ""
let goal_source_buffer = GSourceView2.source_buffer ()
let prover_output_buffer = GSourceView2.source_buffer ()
let console_output_buffer = GSourceView2.source_buffer ()

let prover_output_label_string = "Provers output"
let console_output_label_string = "Console output"

let sig_list = new GTree.column_list
let sig_column = sig_list#add Gobject.Data.string
let signature_model = GTree.list_store sig_list
				       
(* hyps list *)
let cols_list = new GTree.column_list
let hyp_id_column = cols_list#add Gobject.Data.string
let hyp_column = cols_list#add Gobject.Data.string
let hyps_list_model = GTree.list_store cols_list

let rec fill_hyps_list_model ?h:(h=0) (hyps_strings:string list)  (model: GTree.list_store)
  =
  match hyps_strings with
  | [] -> ()
  | stp::others ->
     let row = model#append () in
     model#set ~row ~column: hyp_column (String.trim stp);
     let label = ("H" ^ (string_of_int h)) in
     model#set ~row ~column: hyp_id_column label;
     fill_hyps_list_model ~h:(h+1) others model

let rec fill_lemma_hyps_list_model ?h:(h=0) (preds:Lang.F.pred list)  
				   (model: GTree.list_store) =
  match preds with
  | [] -> ()
  | p::others->
     let row = model#append () in
     let predst = string_of_pred p in
     model#set ~row ~column: hyp_column (String.trim predst);
     let label = ("H" ^ (string_of_int h)) in
     model#set ~row ~column: hyp_id_column label;
     fill_lemma_hyps_list_model ~h:(h+1) others model     

let update_hyps_list_model hyps_strings =
  hyps_list_model#clear();
  fill_hyps_list_model hyps_strings hyps_list_model

let set_wpo_result wpo =
  let results = Wpo.get_results wpo in
  let rec pretty results =
    match results with
    |(p,r)::t ->
      let pp = (
	VCS.pp_prover Format.str_formatter p;
	Format.flush_str_formatter ()
      ) in
      let rr = string_of_result r in 
      let ps = pp ^ ":" ^ rr ^ "\n" in
      ps ^ (pretty t)
    | _ -> ""
  in
  let printstring = pretty results in
  prover_output_buffer#set_text printstring

let fill_console_with_last_commands buffer wpo () =
  let tactics = get_precursor_tactics wpo in
  let stlist = List.map (fun tac -> string_of_inf_rule tac) tactics  in
  (if stlist <> [] then (
     let ts = String.concat ";\n" stlist in
     buffer#set_text (ts ^ ";\n")
   )
   else buffer#set_text ""
  )

let rec fill_signature_model ?index:(i=0) (signatures: string list) 
			     (model: GTree.list_store) sig_column =
  match signatures with
  | [] -> ()
  | hd:: tl ->
     let row = model#append () in
     model#set ~row ~column: sig_column (String.trim hd);
     fill_signature_model ~index:(i+1) tl model sig_column
			  
let update_signature_list w = 
  let string_signatures = get_signature_reprs_in_string w in
  signature_model#clear ();
  fill_signature_model string_signatures signature_model sig_column

(*		       
let show_selected_wpo (wpo_tree_view:GTree.view) path =
  let selected_array = GTree.Path.get_indices path in
  let bounds = wpo_tree_view#get_visible_range () in
  match bounds with
  | None -> wpo_tree_view#scroll_to_cell path (wpo_tree_view#get_column 0)
  | Some (startp,endp) ->
     let start_array = GTree.Path.get_indices startp in
     let end_array = GTree.Path.get_indices endp in
     let si = Array.get selected_array 0 in
     let sti1 = Array.get start_array 0 in
     let sti2 = Array.get end_array 0 in
     if si < sti1 || si > sti2 then
       wpo_tree_view#scroll_to_cell path (wpo_tree_view#get_column 0)
     else ()
 *)
	    
let update_shown_wpo pretty_source last_commands_buffer wpo_name_frame guiWpoTreeView wpo:unit =
  current_wp := Some wpo;
  set_wpo_result wpo;
  (
    match wpo.Wpo.po_formula with
    |Wpo.GoalAnnot ga ->
      let (hyps,goal) = string_of_vc_annot ga in
      update_hyps_list_model hyps;
      goal_source_buffer#set_text goal;
      wpo_name_frame#set_label
	(Some (wpo.Wpo.po_gid ^ (" (Annot) (" ^ (Model.get_descr wpo.Wpo.po_model) ^ ")")))
    |Wpo.GoalLemma gl ->
      let (hyps,goal) = string_of_vc_lemma gl in
      update_hyps_list_model hyps;
      goal_source_buffer#set_text goal;      
      wpo_name_frame#set_label
	(Some (wpo.Wpo.po_gid ^ (" (Lemma) (" ^ (Model.get_descr wpo.Wpo.po_model) ^ ")")))
    |Wpo.GoalCheck _ -> raise (Failure "GoalCheck was not implemented")
  );
  update_signature_list wpo;
  fill_console_with_last_commands last_commands_buffer wpo ();
  guiWpoTreeView#select wpo.Wpo.po_gid;
  pretty_source#select (Some wpo)
		       
let get_path_of_clicked_point view ev: Gtk.tree_path option =
  let x = int_of_float (GdkEvent.Button.x ev) in
  let y = int_of_float (GdkEvent.Button.y ev) in
  match view#get_path_at_pos ~x ~y with
  | Some (path, _, _, _) -> Some path
  | None -> None

let execute_inf_rule guiWpoTreeView po_gid (tactic:tactic_s) =
  let new_wpo = execute_inf_rule po_gid tactic in
  let tactic_name = name_of_tactic tactic in
  let msg = tactic_name ^ " command succesfull" in
  console_output_buffer#set_text msg;
  guiWpoTreeView#reload ();
  guiWpoTreeView#select new_wpo.Wpo.po_gid

let split_conjunction_in_hypothesis wpo index wpo_tree_view () =
  execute_inf_rule wpo_tree_view wpo (Split_and_hyp index)

let contrapose_hypothesis_function wpo index wpo_tree_view () =
  execute_inf_rule wpo_tree_view wpo (Contrapose index)

let rewrite_branch wpo index wpo_tree_view () =
  let tactic=Rewrite_branch index in
  execute_inf_rule wpo_tree_view wpo tactic

let rewrite_if_then_else_goal wpo wpo_tree_view () = 
  let tactic = Rewrite_if_then_else_goal in
  execute_inf_rule wpo_tree_view wpo tactic
		 
let split_term_from_disjunction wpo index wpo_tree_view ():unit =
  let tactic = Split_term_disj_hyp index in
  execute_inf_rule wpo_tree_view wpo tactic

let remove_term_from_disjunction_goal index wpo wpo_tree_view ():unit =
  let tactic = (Remove_term_disj_goal (index)) in
  execute_inf_rule wpo_tree_view wpo tactic

let add_hypothesis_rewrite_function ~menu ~label ~action =
  let menuitem = GMenu.menu_item ~label ~packing: menu#append () in
  ignore (menuitem#connect#activate ~callback: action)

let add_hypothesis_menu_item wpo wpo_tree_view menu (step_index:int) =
  let step = get_step_by_order wpo step_index in
  if (is_branch step) then
    (
      add_hypothesis_rewrite_function
	~menu
	~label: "Rewrite Branch as a Have condition"
	~action: (rewrite_branch wpo.Wpo.po_gid step_index wpo_tree_view);
    )
  else (
    let pred = get_predicate_from_step step in
    if is_conjunction_of_terms (Lang.F.e_prop pred) then
      (
	add_hypothesis_rewrite_function
	  ~menu
	  ~label: "Split conjunction"
	  ~action: (split_conjunction_in_hypothesis wpo.Wpo.po_gid step_index wpo_tree_view);
      );
    if is_disjunction_of_terms (Lang.F.e_prop pred) then
      (
	add_hypothesis_rewrite_function
	  ~menu
	  ~label: "Split disjunction"
	  ~action: (split_term_from_disjunction wpo.Wpo.po_gid step_index wpo_tree_view);
      );
    add_hypothesis_rewrite_function
      ~menu
      ~label: "Contrapose hypothesis"
      ~action: (contrapose_hypothesis_function wpo.Wpo.po_gid step_index wpo_tree_view);
  )
	 
let create_rules_menu wpo_tree_view menu wpo_index _: unit =
  match !current_wp with
  | Some wpo ->
     ignore (add_hypothesis_menu_item wpo wpo_tree_view menu wpo_index)
  | None -> ()

let copy selection _ =
  let first = ref true in
  let string_text = ref "" in
  let get_strings _ iter = 
    if selection#iter_is_selected iter then
      if !first then (
	string_text := hyps_list_model#get ~row:iter ~column:hyp_column;
	first := false
      ) else (
	string_text := !string_text ^ "\n" ^
			 hyps_list_model#get ~row:iter ~column:hyp_column;
      ); false in  
  hyps_list_model#foreach get_strings;
  let atom = Gdk.Atom.clipboard in
  let clipboard = GtkBase.Clipboard.get atom in
  GtkBase.Clipboard.set_text clipboard !string_text

let make_po_from_selected_hypotheses wpo_tree_view selection _ =
  let wpo = get_current_wpo () in
  let selected_list = ref [] in
  let citer = ref (-1) in
  let get_hyps _ iter =
    incr citer;
    if selection#iter_is_selected iter then
      selected_list := !selected_list @ [!citer];
    false
  in
  hyps_list_model#foreach get_hyps;
  let tactic = (Subset_hyps (!selected_list)) in
  execute_inf_rule wpo_tree_view wpo.Wpo.po_gid tactic

let make_po_from_removed_hypotheses wpo_tree_view selection _ =
  let selected_list = ref [] in
  let citer = ref (-1) in
  let wpo = get_current_wpo () in
  let get_hyps _ iter =
    incr citer;
    if not (selection#iter_is_selected iter) then
      selected_list := !selected_list @ [!citer];
    false
  in
  hyps_list_model#foreach get_hyps;
  let tactic = (Subset_hyps (!selected_list)) in
  execute_inf_rule wpo_tree_view wpo.Wpo.po_gid tactic

let get_selected_hypothesis path =
  let iter = hyps_list_model#get_iter path in
  let hyps_string = hyps_list_model#get ~row: iter ~column: hyp_column in
  hyps_string

let 
  create_hyps_selection_menu (menu: GMenu.menu) hyps_list_view =
  let copy_item = GMenu.menu_item
		    ~label:"copy"
		    ~packing: menu#append () in
  ignore (copy_item#connect#activate ~callback:(copy hyps_list_view#selection))
	 
let undo po_gid guiWpoTreeView () =
  let wpo = get_wpo_by_po_gid po_gid in
  let pruned_wpo = cut_wpo_children wpo in
  guiWpoTreeView#reload ();
  guiWpoTreeView#select pruned_wpo.Wpo.po_gid

let on_wpo_tree_view_clicked guiWpoTreeView ev : bool =
  if GdkEvent.Button.button ev = 3 then
    (
      let clicked_point = get_path_of_clicked_point (guiWpoTreeView#get_view ()) ev in
      match clicked_point with
      | Some path ->
	 (
	   let po_gid = guiWpoTreeView#get_wpo_gid_by_path path in
	   let menu = GMenu.menu () in
	   let prune_item = GMenu.menu_item
			      ~label:"Undo"
			      ~packing: menu#append () in
	   ignore
	     (prune_item#connect#activate ~callback: 
					  (undo po_gid guiWpoTreeView));
	   menu#popup ~button:
		      (GdkEvent.Button.button ev) 
		      ~time: (GdkEvent.Button.time ev);
	   true
	 )
      | None -> false
    )
  else false

let on_hyps_list_view_clicked hyps_list_view ev : bool =
  if GdkEvent.Button.button ev = 3 then
    match (get_path_of_clicked_point hyps_list_view ev) with
    | Some path ->
       hyps_list_view#selection#select_path path;
       let menu = GMenu.menu () in
       ignore
	 (create_hyps_selection_menu menu hyps_list_view);
       menu#popup ~button:
		  (GdkEvent.Button.button ev) ~time: (GdkEvent.Button.time ev);
       true
    | None -> false
  else false

let create_hyps_list_view () =
  let hyps_list_view =
    GTree.view ~rules_hint: true 	       
	       ~model: hyps_list_model ~border_width:5 () in
  hyps_list_view#set_enable_search true;
  let renderer = GTree.cell_renderer_text [] in
  hyps_list_view#selection#set_mode `MULTIPLE;
  let col0 = GTree.view_column ~title:"" 
			       ~renderer:(renderer, ["text",hyp_id_column]) () 
  in
  let col1 = GTree.view_column ~title:"Hypotheses" 
			       ~renderer: (renderer, ["text", hyp_column ]) () 
  in
  ignore (hyps_list_view#append_column col0);
  ignore (hyps_list_view#append_column col1);
  hyps_list_view

let delete_event _ = false

let destroy () =
  current_wp := None;
  GMain.Main.quit ()

let rec execute_inf_rules wpo_tree_view tactics =
  match tactics with
  |[] -> ()
  |t::r -> execute_inf_rule wpo_tree_view t.id t.tactic;
	   execute_inf_rules wpo_tree_view r
			   
let scrolled_signature_window () =  
  let scrolled_window = GBin.scrolled_window () in
  let signature_view = GTree.view ~rules_hint: true 
				  ~model: signature_model ~border_width:5 () in
  let renderer = GTree.cell_renderer_text [] in
  signature_view#selection#set_mode `MULTIPLE;
  let col1 = GTree.view_column ~title:"Variables" 
			       ~renderer: (renderer, ["text", sig_column ]) () 
  in
  ignore (signature_view#append_column col1);
  scrolled_window#add signature_view#coerce;
  scrolled_window

let call_why3ide _wpo_tree_view () =
  let w = get_current_wpo () in
  let callback _w _prover result =
    let print_result = 
      VCS.pp_result Format.str_formatter result;
      Format.flush_str_formatter ()
    in
    prover_output_buffer#set_text print_result
  in
  let callback w p r =
    callback w p r in
  let task = Prover.wp_why3ide ~callback
			       (fun f -> Wpo.iter ~on_goal:f ()) in
  let kill () =
    Wpo.set_result w VCS.Why3ide VCS.no_result ;
    Task.cancel task;
  in
  Wpo.set_result w VCS.Why3ide (VCS.computing kill) ;
  let server = ProverTask.server () in
  Task.spawn server (Task.job task) ;
  Task.launch server

let call_prover w prover ~mode guiWpoTreeView () = 
  let callin _w (prover:VCS.prover) = (
    let iter = guiWpoTreeView#get_first_selected_iter () in
    guiWpoTreeView#update_result_icon (VCS.Computing (fun () -> ())) iter;
    let prover_name = VCS.name_of_prover prover in
    let timeout = Wp_parameters.Timeout.get () in
    let nprocess = Wp_parameters.Procs.get () in
    let nsteps = Wp_parameters.Steps.get () in
    let ndepth = Wp_parameters.Depth.get () in
    console_output_buffer#set_text 
      ("Executing " ^ prover_name ^ ".\n" ^ 
	 "Timeout:" ^ (string_of_int timeout) ^ ".\n" ^ 
	   "Processes: " ^ (string_of_int nprocess) ^ ".\n" ^ 
	     "Steps: " ^ (string_of_int nsteps) ^ ".\n" ^
	       "Depth: " ^ (string_of_int ndepth) ^ ".")
  ) in
  let callback w prover result = (
    let fa = w.Wpo.po_partac in
    (match fa with
     | None -> ()
     | Some (_,father_w,_) -> update_rewriting_status_for_parents father_w);
    let print_result = 
      VCS.pp_result Format.str_formatter result;
      Format.flush_str_formatter ()
    in
    prover_output_buffer#set_text print_result;
    guiWpoTreeView#reload ();
    let curw = get_current_wpo () in
    if curw.Wpo.po_gid = w.Wpo.po_gid then(
      (*if result.VCS.verdict = VCS.Valid then guiWpoTreeView#selectNextUnproven w else*)
      guiWpoTreeView#select w.Wpo.po_gid
    )
    else guiWpoTreeView#select curw.Wpo.po_gid;
    let prover_name = VCS.name_of_prover prover in
    console_output_buffer#set_text ("Finished " ^ prover_name ^ ":" ^ 
				      string_of_result result)
  )
  in
  let open VCS in
  let task = Prover.prove w ~callin ~mode ~callback prover in
  let kill () =
    Wpo.set_result w prover VCS.no_result ;
    Task.cancel task in
  Wpo.set_result w prover (VCS.computing kill) ;
  let server = ProverTask.server () in
  Task.spawn server (Task.job task) ;
  Task.launch server

exception Finished

let finished_function w result guiWpoTreeView = 
  let fa = w.Wpo.po_partac in
  (match fa with
   | None -> ()
   | Some (_,father_w,_) -> update_rewriting_status_for_parents father_w);
  let print_result = 
    VCS.pp_result Format.str_formatter result;
    Format.flush_str_formatter ()
  in
  prover_output_buffer#set_text print_result;
  guiWpoTreeView#reload ();
  guiWpoTreeView#select w.Wpo.po_gid

let call_provers w provers guiWpoTreeView () =
  let server = ProverTask.server () in
  let task = 
    Task.call 
      (fun () ->
       try
	 List.iter 
	   (fun p ->
	    let res = get_verdict w in
	    match res with
	    |VCS.Valid -> raise Finished
	    | _ -> 
	       let callin _w (prover:VCS.prover) = (
		 let iter =guiWpoTreeView#get_first_selected_iter () in
		 guiWpoTreeView#update_result_icon (VCS.Computing (fun () -> ())) iter;
		 let prover_name = VCS.name_of_prover prover in
		 console_output_buffer#set_text ("Executing " ^ prover_name ^ ".")
	       ) in
	       let callback _w prover _result = (
		 let prover_name = VCS.name_of_prover prover in
		 console_output_buffer#set_text ("Finished " ^ prover_name ^ ".")
	       ) in			
	       let tsk = Prover.prove w ~callin ~callback ~mode:VCS.BatchMode p in	  
	       let kill () =
		 Wpo.set_result w p VCS.no_result ;
		 Task.cancel tsk in
	       Wpo.set_result w p (VCS.computing kill) ;
	       Task.spawn server (Task.job tsk) ;
	       Task.launch server;
	       ignore (Task.wait tsk)
	   ) provers;
	 finished_function w ( get_result w) guiWpoTreeView
       with Finished -> 
	 finished_function w (get_result w) guiWpoTreeView
      ) in
  Task.run (task ())

let get_dir w = 
  let lists = Wpo.get_files w in
  match lists with
  |[] -> Filename.current_dir_name 
  |(_,file)::_tl -> Filename.dirname file


let save_tactics_as ():unit = () (* TODO *)
				
let load_tactics _wpo_tree_view () = () (*TODO*)

let erase_wpo_view () =
  hyps_list_model#clear ();
  goal_source_buffer#set_text ""

let reset_proof_obligations guiWpoTreeView () =
  reset_proof_obligations ();
  guiWpoTreeView#reload ();
  erase_wpo_view ()
		 
let show_signatures window:unit = 
  if window#misc#visible then
    window#misc#hide ()
  else 
    window#misc#show ()

exception Finished

let execute_console_command
      pretty_source
      wpo_name_frame
      (last_commands_buffer:GSourceView2.source_buffer) 
      (console_input_buffer:GSourceView2.source_buffer) 
      (guiWpoTreeView:GuiWpoTreeView.guiWpoTreeView)
      key:bool = 
  let wpo = !current_wp in
  let keyval = GdkEvent.Key.keyval key in
  if keyval = 65293 then (*keyval = ENTER *)
    (
      let command_text = console_input_buffer#get_text () in
      console_input_buffer#set_text "";
      let tc = WpoRewriter_console.execute_command wpo command_text 
      in
      let open WpoRewriter_console in
      let result = (
	match tc with
	|Load -> load_tactics guiWpoTreeView ();true
	|Save -> save_tactics_as (); true
	|Success msg -> console_output_buffer#set_text msg; true
	|InfRule (new_wpo,message) ->
	  console_output_buffer#set_text message;
	  guiWpoTreeView#reload ();
	  guiWpoTreeView#select new_wpo.Wpo.po_gid;
	  (*guiWpoTreeView#reload ();
	  update_shown_wpo pretty_source last_commands_buffer wpo_name_frame guiWpoTreeView
			   new_wpo;
	   *)
	  true
	|Reset message ->
	  console_output_buffer#set_text message;
	  (
	    match wpo with
	    | Some w -> let root = get_root w in
			undo root.Wpo.po_gid guiWpoTreeView ();
			true
	    | None -> true
	  )
	|Undo message ->
	  console_output_buffer#set_text message;
	  (
	    match wpo with
	    | Some w -> undo w.Wpo.po_gid guiWpoTreeView (); true
	    | None -> true
	  )
	|Failed message -> 
	  console_output_buffer#set_text message;
	  true
	|ResetAll ->
	  console_output_buffer#set_text "POs were reset";
	  reset_proof_obligations guiWpoTreeView ();
	  guiWpoTreeView#select_first_wpo ();
	  true
	|Mk_Lemma -> guiWpoTreeView#reload ();
		     true
	|Prover (prover,mode) -> (
	  match wpo with
	  | Some w -> (
	    match prover with 
	    |VCS.Coq  
	    |VCS.Why3 _ 
	    |VCS.AltErgo 
	    |VCS.Qed -> call_prover w prover ~mode guiWpoTreeView ();true
	    |VCS.Why3ide -> call_why3ide guiWpoTreeView ();true	
	    |VCS.Rewriter -> raise (Failure "Selected label is not a valid prover")
	  )
	  | None -> true
	)
	|ProveWithAll provers -> (
	  match wpo with
	  | None -> true
	  | Some w -> (
	    try
	      List.iter (fun pr -> 
			 let res = get_verdict w in
			 match res with 
			 | VCS.Valid -> raise Finished
			 | _ -> call_prover w pr ~mode:VCS.BatchMode guiWpoTreeView ()
			)provers;
	      true
	    with Finished -> true
	  ))
	| ProveUnproven prover ->
	   Wpo.iter_on_goals (
	       fun w -> let res = get_verdict w in
			match res with 
			|VCS.Valid -> ()
			| _ -> (
			  match prover with
			  |VCS.Coq  
			  |VCS.Why3 _ 
			  |VCS.AltErgo 
			  |VCS.Qed -> call_prover w prover ~mode:VCS.BatchMode guiWpoTreeView ()
			  | _ -> raise (Failure "Selected label is not a valid prover")
			)
	     );
	   true		
      )
      in
      !update_function ();
      result
    ) 
  else false  
	 
let create_file_menu ~packing guiWpoTreeView () =
  let file_menu = GMenu.menu ~packing () in
  let item = GMenu.menu_item
	       ~label:"Refresh Proof Obligations"
	       ~packing: file_menu#append () in
  ignore (item#connect#activate ~callback: guiWpoTreeView#reload);
  let item = GMenu.menu_item
	       ~label:"Reset Proof Obligations"
	       ~packing: file_menu#append () in
  ignore (item#connect#activate ~callback: (reset_proof_obligations guiWpoTreeView));
  let item = GMenu.menu_item ~label:"Quit" ~packing: file_menu#append () in
  item#connect#activate ~callback: GMain.Main.quit

let create_script_menu ~packing wpo_tree_view () =
  let file_menu = GMenu.menu ~packing () in
  let item =
    GMenu.menu_item ~label:"Save tactics" ~packing: file_menu#append () in
  ignore (item#connect#activate ~callback: (save_tactics_as));
  let item =
    GMenu.menu_item ~label:"Save tactics as" ~packing: file_menu#append () in
  ignore (item#connect#activate ~callback: (save_tactics_as));
  let item =
    GMenu.menu_item ~label:"Load tactics" ~packing: file_menu#append () in
  ignore (item#connect#activate ~callback: (load_tactics wpo_tree_view))

let contrapose_goal wpo wpo_tree_view ():unit =
  execute_inf_rule wpo_tree_view wpo.Wpo.po_gid (Contradict)

let split_goal_conjunction wpo wpo_tree_view (): unit =
  execute_inf_rule wpo_tree_view wpo.Wpo.po_gid Split_and_goal

let output_buffer_changed new_label_string label notebook tab_index () =
  if notebook#current_page <> tab_index then
    label#set_label ("<i>" ^ new_label_string ^ "</i>")

let notebook_outputs_switched_page console_output_label prover_output_label 
				   tab_index =
  if tab_index = 0 then console_output_label#set_label 
			  console_output_label_string
  else if tab_index = 1 then prover_output_label#set_label
			       prover_output_label_string

let create_goal_menu menu wpo_tree_view =
  match !current_wp with
  | Some w ->
     let menuitem =
       GMenu.menu_item ~label:"Contrapose Goal" ~packing: menu#append () in
     ignore (menuitem#connect#activate ~callback:
				       (contrapose_goal w wpo_tree_view)
	    );

     let goal = get_goal w in
     if is_conjunction_of_terms (Lang.F.e_prop goal) then
       (
	 let menuitem =
	   GMenu.menu_item ~label:"Split conjunction" ~packing: menu#append () 
	 in
	 ignore (menuitem#connect#activate ~callback: 
					   (split_goal_conjunction 
					      w wpo_tree_view))
       );

     let number_of_disjunctions = get_number_of_disjunctions
				    (Lang.F.e_prop goal) in
     if number_of_disjunctions > 1 then(
       for i = 0 to number_of_disjunctions -1 do
	 let label =
	   "Remove " ^ (string_of_int (i +1)) ^
	     "th goal from disjunction" in
	 let menuitem = GMenu.menu_item ~label ~packing: menu#append () in
	 ignore
	   (menuitem#connect#activate
	      ~callback: (remove_term_from_disjunction_goal i w.Wpo.po_gid 
							    wpo_tree_view));
       done
     );
     if is_condition (Lang.F.e_prop goal) then
       (
	 let menuitem = 
	   GMenu.menu_item ~label:"Rewrite Branch as a Have condition"
			   ~packing: menu#append () in
	 ignore (menuitem#connect#activate
		   ~callback: (
		     rewrite_if_then_else_goal w.Wpo.po_gid wpo_tree_view)
		)
       )
  | _ -> ()

let list_goal_functions wpo_tree_view ev =
  if GdkEvent.Button.button ev = 3 then
    (
      let menu = GMenu.menu () in
      ignore (create_goal_menu menu wpo_tree_view);
      menu#popup ~button:
		 (GdkEvent.Button.button ev) ~time: (GdkEvent.Button.time ev);
      true
    )
  else false

	 
(*let on_wpo_tree_view_changed pretty_source console_input_buffer
			     guiWpoTreeView frame () =
  try
    (*let row = get_first_selected_iter wpo_tree_view wpo_tree_model in
    let p = get_item_from_wpo_tree_model row col_wpo_gid in *)
    let row = guiWpoTreeView#get_first_selected_iter () in
    let gid = guiWpoTreeView#get_po_gid_from_row row in
    let wpo = get_wpo_by_po_gid gid in
    update_shown_wpo pretty_source console_input_buffer frame guiWpoTreeView wpo
  with Failure _ -> ()
 *)

let call_history (buffer:GSourceView2.source_buffer) ev:bool =
  let k = GdkEvent.Key.keyval ev in
  let s = GdkEvent.Key.state ev in
  if k = 65362 && List.exists (fun v -> v = `CONTROL) s then (* up *) (
    WpoRewriter_console.set_temp_command (buffer#get_text ());
    buffer#set_text (WpoRewriter_console.previous_history ());
    false
  )
  else if k = 65364 && List.exists (fun v -> v = `CONTROL) s then (* down *) (
    buffer#set_text (WpoRewriter_console.next_history ());
    false
  )
  else false

let mk_def_pane () =
  let def_text =
    ( 
      try
	let wpo = get_current_wpo () in
	get_def_text wpo
      with Not_found -> ""
    ) in
  let def_source = GSourceView2.source_buffer ~text:def_text () in
  let def_view = GSourceView2.source_view ~editable:false
					  ~wrap_mode:`WORD ~source_buffer:def_source () in
  let scrolled_window = GBin.scrolled_window () in
  scrolled_window#add def_view#coerce;
  scrolled_window

let configure_main_window () =
  let window = GWindow.window
		 ~title:"WPTrans"
		 ~position: `CENTER
		 ~height:600
		 ~width:1200
		 () in
  (ignore (window#event#connect#delete ~callback: delete_event));
  ignore (window#connect#destroy ~callback: destroy);
  let windowbox = GPack.box `VERTICAL ~border_width: 0 ~packing: window#add () in
  (window,windowbox)

let configure_menu_bar guiWpoTreeView=
  let menu_bar = GMenu.menu_bar () in
  let file_item =
    GMenu.menu_item ~label:"File" ~packing: menu_bar#append () in
  ignore (create_file_menu ~packing: file_item#set_submenu guiWpoTreeView ());
(*
  let script_item =
    GMenu.menu_item ~label:"Script" ~packing: menu_bar#append () in
 *)
(* to uncomment
  ignore (create_script_menu ~packing: script_item#set_submenu  guiWpoTreeView#get_view ());
 *) 
 menu_bar

let configure_right_column () =
  let prover_output_scroller = GBin.scrolled_window () in
  let _prover_output_view = GSourceView2.source_view 
			      ~source_buffer: prover_output_buffer ~wrap_mode:`WORD
			      ~editable: false ~border_width: 5 
			      ~packing:prover_output_scroller#add () in 
  let console_output_scroller = GBin.scrolled_window ~hpolicy:`AUTOMATIC 
						     ~vpolicy:`AUTOMATIC () in
  let _console_output_view = GSourceView2.source_view ~wrap_mode:`WORD
						      ~source_buffer: console_output_buffer
						      ~editable:false ~border_width:5 
						      ~packing: console_output_scroller#add ()
  in
  let console_output_label = GMisc.label ~text:console_output_label_string () in
  console_output_label#set_use_markup true;
  let consoles_box = GPack.paned `VERTICAL () in
  (*ignore (framebox#pack2 ~resize:true ~shrink:true consoles_box#coerce);*)
  let notebook_outputs = GPack.notebook ~packing:consoles_box#add2 () in
  let console_output_index = notebook_outputs#append_page 
			       ~tab_label:console_output_label#coerce
			       console_output_scroller#coerce in 
  let prover_output_label = GMisc.label ~text:prover_output_label_string () in
  prover_output_label#set_use_markup true;
  let prover_output_index = notebook_outputs#append_page 
			      ~tab_label:prover_output_label#coerce
			      prover_output_scroller#coerce in 
  ignore(console_output_buffer#connect#changed (
	     output_buffer_changed console_output_label_string  
				   console_output_label notebook_outputs console_output_index));
  ignore(prover_output_buffer#connect#changed (
	     output_buffer_changed prover_output_label_string 
				   prover_output_label notebook_outputs prover_output_index));
  ignore(notebook_outputs#connect#switch_page (
	     notebook_outputs_switched_page console_output_label 
					    prover_output_label));
  let commands_pack = GPack.paned `VERTICAL () in
  consoles_box#pack1 ~resize:true ~shrink:true commands_pack#coerce;
  let console_history_frame = GBin.frame ~label: "History:" ~packing:commands_pack#add1 () in
  let definitions_label = GMisc.label ~text:"Definitions" () in
  let history_label = GMisc.label ~text:"History" () in
  let definitions_pane = mk_def_pane () in
  let notebook_upper_right = GPack.notebook ~packing:console_history_frame#add () in   
  let scrolled_last_commands = GBin.scrolled_window () in
  ignore (notebook_upper_right#append_page ~tab_label:history_label#coerce 
					   scrolled_last_commands#coerce);

  ignore (notebook_upper_right#append_page ~tab_label:definitions_label#coerce
					   definitions_pane#coerce);
  let last_commands_view =      
    GSourceView2.source_view ~editable:true ~highlight_current_line:false
			     ~show_line_numbers:false ~show_line_marks:false 
			     ~border_width:5 ~wrap_mode:`WORD
			     ~packing:scrolled_last_commands#add () in
  let console_frame = GBin.frame ~label: "Console (ACSL syntax):" ~packing:commands_pack#add2 ()
  in   
  let gui_console = new GuiConsole.guiConsole () in
  console_frame#add (gui_console#get ())#coerce;
  let last_commands_input_buffer = last_commands_view#source_buffer in
  let console_input_buffer = gui_console#source () in
  (consoles_box,commands_pack,gui_console,console_input_buffer,last_commands_input_buffer)
    
let configure_left_column guiWpoTreeView =
  let left_paned_box = GPack.paned `VERTICAL ~width:250 () in
  guiWpoTreeView#pack left_paned_box#add1;
  (*left_paned_box#add1 (guiWpoTreeView#get_window ())#coerce;*)
  let scrolled_signature_window1 = scrolled_signature_window () in   
  left_paned_box#add2 scrolled_signature_window1#coerce;
(* to uncomment
  let wpo_tree_view = guiWpoTreeView#get_view () in
  ignore (wpo_tree_view#event#connect#button_press
	    ~callback: (on_wpo_tree_view_clicked guiWpoTreeView));
 *)

 (*ignore
    (left_paned_box#event#connect#any
       ~callback:
       (fun t ->
	print_endline ("position=" ^ (string_of_int left_paned_box#position));
	print_endline ("maxposition=" ^ (string_of_int left_paned_box#max_position));
	print_endline ("minposition=" ^ (string_of_int left_paned_box#min_position));

	false)); 
  *)  
left_paned_box
    
let configure_proof_obligation_column guiWpoTreeView =
  let pretty_source = new GuiGoal.pane () in
  let wpo_paned_box = GPack.paned `VERTICAL () in
  let po_notebook = GPack.notebook ~width:600  () in
  let po_label = GMisc.label ~text:"Proof Obligation" () in
  ignore (po_notebook#append_page ~tab_label:po_label#coerce wpo_paned_box#coerce);
  let pretty_label = GMisc.label ~text:"Pretty" () in
  ignore (po_notebook#append_page ~tab_label:pretty_label#coerce 
				  pretty_source#coerce); 
  let hypothesis_paned_box = GPack.paned `VERTICAL  () in
  wpo_paned_box#pack1 ~resize:true ~shrink:true hypothesis_paned_box#coerce ;
  let scrolled_hyps_list_window = GBin.scrolled_window () in
  let hyps_list_view = create_hyps_list_view () in
  scrolled_hyps_list_window#add hyps_list_view#coerce;
  hypothesis_paned_box#pack2 ~resize:true ~shrink:true 
			     scrolled_hyps_list_window#coerce;
  let goal_frame = GBin.frame ~label: "Goal:" () in
  wpo_paned_box#add2 goal_frame#coerce;
  let goal_view = GSourceView2.source_view ~source_buffer: goal_source_buffer
					   ~editable: false
					   ~wrap_mode:`WORD
					   ~highlight_current_line: false
					   ~show_line_numbers: false
					   ~show_line_marks: false
					   ~border_width: 5
					   ~packing: goal_frame#add () in
  ignore (goal_view#event#connect#button_press
	    ~callback: (list_goal_functions guiWpoTreeView));
  ignore (hyps_list_view#event#connect#button_press
	    ~callback: (on_hyps_list_view_clicked hyps_list_view)
	 );
  (po_notebook,wpo_paned_box,pretty_source)

    
let post_configuration_setup window (guiWpoTreeView:GuiWpoTreeView.guiWpoTreeView)
			     pretty_source last_commands_input_buffer
			     wpo_frame gui_console console_input_buffer = 
  (
    match !current_wp with
    |Some w ->
      guiWpoTreeView#select w.Wpo.po_gid;
	       pretty_source#select (Some w);
	       update_shown_wpo pretty_source last_commands_input_buffer wpo_frame 
				guiWpoTreeView w;
	       update_signature_list w;
    |None -> ()
  );   

  ignore (gui_console#set_key_press_action 
	    ~callback:(execute_console_command 
			 pretty_source
			 wpo_frame
			 last_commands_input_buffer
			 console_input_buffer 
			 guiWpoTreeView));

  let reload_function wpo =
    current_wp := Some wpo;
    set_wpo_result wpo;
    (
      match wpo.Wpo.po_formula with
      |Wpo.GoalAnnot ga ->
	let (hyps,goal) = string_of_vc_annot ga in
	update_hyps_list_model hyps;
	goal_source_buffer#set_text goal;
	wpo_frame#set_label
	  (Some (wpo.Wpo.po_gid ^ (" (Annot) (" ^ (Model.get_descr wpo.Wpo.po_model) ^ ")")))
      |Wpo.GoalLemma gl ->
	let (hyps,goal) = string_of_vc_lemma gl in
	update_hyps_list_model hyps;
	goal_source_buffer#set_text goal;      
	wpo_frame#set_label
	  (Some (wpo.Wpo.po_gid ^ (" (Lemma) (" ^ (Model.get_descr wpo.Wpo.po_model) ^ ")")))
      |Wpo.GoalCheck _ -> raise (Failure "GoalCheck was not implemented")
    );
    update_signature_list wpo;
    fill_console_with_last_commands last_commands_input_buffer wpo ();
    pretty_source#select (Some wpo)
  in
  
  ignore (guiWpoTreeView#register_reload_function (reload_function));
   

  (*let wpo_tree_view = guiWpoTreeView#get_view () in*)    
  (*ignore (wpo_tree_view#selection#connect#changed 
	    ~callback: (on_wpo_tree_view_changed 
			  pretty_source last_commands_input_buffer guiWpoTreeView wpo_frame));
   *)  

  let set_focus (guiWpoTreeView:GuiWpoTreeView.guiWpoTreeView) console_view ev = (
    let k = GdkEvent.Key.keyval ev in
    let s = GdkEvent.Key.state ev in
    if k = 49 then (*key 1*)
      if List.exists (fun v -> v = `CONTROL) s then(
	guiWpoTreeView#grab_focus ();
	true
      )
      else false
    else if k = 50 then (* key 2 *)
      if List.exists (fun v -> v = `CONTROL) s then(
	console_view#grab_focus ();
	true
      )else false
    else false
  ) in
  ignore (window#event#connect#key_press ~callback:(set_focus guiWpoTreeView gui_console));
  gui_console#grab_focus ()  

let dkey = Wp_parameters.register_category "wptrans" (* debugging key *)
let debug fmt = Wp_parameters.debug ~dkey fmt
			 
let build_rewriter () =
  let (window,windowbox) = configure_main_window () in
  (*
  let paned_box = GPack.paned `HORIZONTAL () in
  windowbox#add paned_box#coerce;
  let wpo_frame = GBin.frame ~packing:paned_box#add2 () in
  let framebox = GPack.paned `HORIZONTAL ~packing: wpo_frame#add () in
  let mainbox = GPack.hbox () in
  windowbox#pack ~expand:true ~fill:true mainbox#coerce;
   (*hypotheses and goal *)
   *)

  let guiWpoTreeView = GuiWpoTreeView.getNewWpoTreeView () in
  guiWpoTreeView#reload ();

  let menu_box = configure_menu_bar guiWpoTreeView in
  ignore (windowbox#pack ~expand: false ~fill: false ~padding:0 menu_box#coerce);

  let main_paned_box = GPack.paned `HORIZONTAL ~packing:(windowbox#pack ~expand:true ~fill:true) ()
  in
  let po_and_consoles_box = GPack.paned `HORIZONTAL ~packing:main_paned_box#add2 () in
  let wpo_frame = GBin.frame ~packing:po_and_consoles_box#add1 () in
  
  let (po_box,wpo_paned_box,pretty_source) = configure_proof_obligation_column guiWpoTreeView in
  
  (* proof obligations list and variables *)
  let left_box = configure_left_column guiWpoTreeView in
  
  (*history, definitions, console and outputs *)
  let (right_box,commands_pack,gui_console,console_input_buffer,last_commands_input_buffer) =
    configure_right_column () in

  main_paned_box#add1 left_box#coerce;
  wpo_frame#add po_box#coerce;
  po_and_consoles_box#add2 right_box#coerce;    
  
  post_configuration_setup window guiWpoTreeView pretty_source last_commands_input_buffer
			   wpo_frame gui_console console_input_buffer;
  
  window#show ();
  (*main_paned_box#set_position
      ((main_paned_box#max_position + main_paned_box#min_position)  / 2);*)
  commands_pack#set_position
    ((commands_pack#max_position + commands_pack#min_position)  / 2);
  (*po_and_consoles_box#set_position
    ((po_and_consoles_box#max_position + po_and_consoles_box#min_position)  / 2);*)
  left_box#set_position ((left_box#max_position + left_box#min_position)  / 2);
  wpo_paned_box#set_position ((wpo_paned_box#max_position + wpo_paned_box#min_position)  / 2);
  GMain.Main.main ()

let clean_source_buffers () =
  goal_source_buffer#set_text "";
  prover_output_buffer#set_text "";
  console_output_buffer#set_text ""
let console_output_buffer = GSourceView2.source_buffer ()

let popup_WPTrans ?wpo:(wpo=None) ?update_function:(f=(fun () -> ())) () =
  update_function := f;
  (
    match !current_wp with
    | None ->
       current_wp := wpo;
       clean_source_buffers ();
       build_rewriter ()
    | Some _ -> ()
  )
