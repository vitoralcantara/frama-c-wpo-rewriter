let build_prover_provider (console_input_buffer:GSourceView2.source_buffer)
			  provider_list name =
  let open GSourceView2 in
  let provider_ref = ref None in
  let do_provider () = match !provider_ref
    with None -> assert false | Some x -> x in
  let pop list = (
    let item s = source_completion_item ~label:s ~text:s () in
    let proposal s = (item s :> source_completion_proposal) in
    let proposals = List.map proposal list in
    proposals
  ) in
  let populate (ctx:source_completion_context) plist =
    let proposals = pop plist in
    ctx#add_proposals (do_provider ()) proposals true
  in
  let info_widget ():GObj.widget option =
    let label = GMisc.label ~text:"" () in 
    Some (label#coerce)
  in
  let provider =
    (*TODO: create auto complete for edit too*)
    let provider : custom_completion_provider = object
	method name = name
	method icon = None
	method populate (cc:source_completion_context):unit = 
	  let t = console_input_buffer#get_text () in
	  let calls = WpoRewriter_console.prove_command in
	  let orc = calls in 
	  let regexp = Str.regexp ("[ ]*"^orc^"[ ]+\\([-_A-Za-z]*\\)") in
	  if Str.string_match regexp t 0 then
	    let p = Str.replace_first regexp "\\1" t in
	    let l = String.length p in
	    let rec matches list = 
	      match list with
	      |hd::tl -> 
		if String.length hd < l then matches tl
		else let sub = String.sub hd 0 l in
		     if sub = p then hd::matches tl
		     else matches tl
	      |[] -> []
	    in populate cc (matches provider_list)
	method matched (_scc:source_completion_context) = true 
	method activation = []
	method info_widget (_scp:source_completion_proposal) = info_widget ()
	method update_info (_scp:source_completion_proposal
			   ) (_sci:source_completion_info) =  
	  ()
	method start_iter (_scc:source_completion_context
			  ) (_scp:source_completion_proposal) _iter = false
	method activate_proposal (_scp:source_completion_proposal) _iter = false
	method interactive_delay = 0
	method priority = 0
      end in
    GSourceView2.source_completion_provider provider
  in
  provider_ref := (Some provider);
  provider

let build_provider (console_input_buffer:GSourceView2.source_buffer)  
		   provider_list name =
  let open GSourceView2 in
  let provider_ref = ref None in
  let do_provider () = match !provider_ref
    with None -> assert false | Some x -> x in
  let pop list = (
    let item s = source_completion_item ~label:s ~text:s () in
    let proposal s = (item s :> source_completion_proposal) in
    let proposals = List.map proposal list in
    proposals
  ) in
  let populate (ctx:source_completion_context) plist =
    let proposals = pop plist in
    ctx#add_proposals (do_provider ()) proposals true
  in
  let info_widget ():GObj.widget option =
    let label = GMisc.label ~text:"" () in 
    Some (label#coerce)
  in
  let provider =
    let provider : custom_completion_provider = object
	method name = name
	method icon = None
	method populate (cc:source_completion_context):unit = 
	  let t = console_input_buffer#get_text () in
	  let regexp = Str.regexp "[ ]*\\([-_A-Za-z]*\\)" in
	  if Str.string_match regexp t 0 then
	    let p = Str.replace_first regexp "\\1" t in
	    let l = String.length p in
	    let rec matches list = 
	      match list with
	      |hd::tl -> 
		if String.length hd < l then matches tl
		else let sub = String.sub hd 0 l in
		     if sub = p then hd::matches tl
		     else matches tl
	      |[] -> []
	    in populate cc (matches provider_list)
	method matched (_scc:source_completion_context) = true 
	method activation = []
	method info_widget (_scp:source_completion_proposal) = info_widget ()
	method update_info (_scp:source_completion_proposal
			   ) (_sci:source_completion_info) =  
	  ()
	method start_iter (_scc:source_completion_context
			  ) (_scp:source_completion_proposal) _iter = false
	method activate_proposal (_scp:source_completion_proposal) _iter = false
	method interactive_delay = 0
	method priority = 0
      end in
    GSourceView2.source_completion_provider provider
  in
  provider_ref := (Some provider);
  provider
   
let tactic_provider (console_input_buffer:GSourceView2.source_buffer) = 
  let tactic_names = WpoRewriter_infRules_names.get_tactic_names () in
  build_provider console_input_buffer tactic_names "Tactics"

let command_provider console_input_buffer = 
  let command_names = WpoRewriter_console.get_command_names () in
  build_provider console_input_buffer command_names "Commands"

let prover_provider console_input_buffer =
  let prover_names = WpoRewriter_console.get_prover_names () in
  build_prover_provider console_input_buffer prover_names "Provers"

let call_history (buffer:GSourceView2.source_buffer) ev:bool =
  let k = GdkEvent.Key.keyval ev in
  let s = GdkEvent.Key.state ev in
  if k = 65362 && List.exists (fun v -> v = `CONTROL) s then (* up *) (
    WpoRewriter_console.set_temp_command (buffer#get_text ());
    buffer#set_text (WpoRewriter_console.previous_history ());
    false
  )
  else if k = 65364 && List.exists (fun v -> v = `CONTROL) s then (* down *) (
    buffer#set_text (WpoRewriter_console.next_history ());
    false
  )
  else false

class guiConsole () = 
   let scrolled_console = GBin.scrolled_window ~width:500 (*~packing:console_frame#add*)  () in
  let console_view =
     GSourceView2.source_view ~editable:true ~highlight_current_line:false
			      ~show_line_numbers:false ~show_line_marks:false 
			      ~border_width:5 ~wrap_mode:`WORD
			      ~packing:scrolled_console#add () in
  let buffer = console_view#source_buffer in
  object

initializer
   ignore (console_view#completion#add_provider (tactic_provider buffer));
   ignore (console_view#completion#add_provider (command_provider buffer));
   ignore (console_view#completion#add_provider (prover_provider buffer));

   let call_auto_completion ev = (
     let k = GdkEvent.Key.keyval ev in
     let s = GdkEvent.Key.state ev in
     if k = 32 then (* space *)
       if List.exists (fun v -> v = `CONTROL) s then (
	 let cline = buffer#line_count in
	 let itr = buffer#get_iter (`LINE cline) in
	 let ctx = console_view#completion#create_context itr in
	 ignore (console_view#completion#show 
		   [command_provider buffer;
		    tactic_provider buffer;
		    prover_provider buffer] 
		   ctx); 
	 true
       )
       else false
     else call_history buffer ev
   ) in
   ignore(console_view#event#connect
	    #key_press ~callback:(call_auto_completion));

    method get () = scrolled_console
    method source () = console_view#source_buffer
    method grab_focus () = console_view#misc#grab_focus ()
    method set_key_press_action ~callback = console_view#event#connect#key_press ~callback

end

let get_gui_console = new guiConsole
