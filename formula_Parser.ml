open Wpo
open Qed.Logic
open Lang.F
open Conditions
open Logic_ptree
open Rewriter_utils

type pair = {
    key:string;
    value:string
  }

let rec get_preds_include_branch_either step =
  match step.Conditions.condition with
  | Core pred | Init pred | Type pred | Have pred | When pred -> [pred]
  | Branch (p,seq1,seq2) ->  
     let preds1 = get_all_preds_from_sequence seq1 in
     let preds2 = get_all_preds_from_sequence seq2 in
     [p] @ preds1 @ preds2
  | Either seql  -> List.concat (List.map (get_all_preds_from_sequence)
					  seql)

and get_all_preds_from_sequence seq =
  List.concat ((List.map get_preds_include_branch_either) seq.seq_list)

open Logic_ptree

let rec tau_of_logic_type (l:Logic_ptree.logic_type):tau =
  match l with
  |LTinteger -> Qed.Logic.Int
  |LTreal -> Qed.Logic.Real
  |LTarray (l,_c) -> Qed.Logic.Array (Qed.Logic.Int,tau_of_logic_type l) 
  |LTfloat _ -> raise (Failure "LTfloat is not supported")
  |LTpointer _ -> raise (Failure "LTpointer is not supported")
  |LTvoid -> raise (Failure "LTvoid is not supported")
  |LTint _ -> raise (Failure "LTint is not supported")
  |LTenum _ -> raise (Failure "LTenum is not supported")
  |LTstruct _ -> raise (Failure "LTstruct is not supported")
  |LTunion _ -> raise (Failure "LTunion is not supported")
  |LTnamed _ -> raise (Failure "LTnamed is not supported")
  |LTarrow _ -> raise (Failure "LTarrow is not supported")
  |LTattribute _ -> raise (Failure "LTattribute is not supported")

let rec get_functions t:Lang.F.Fun.t list =
  match repr t with
  | Leq(a, b) -> (get_functions a) @ (get_functions b)
  | True -> []
  | False -> []
  | Not p -> get_functions p
  | Add xs -> List.concat (List.map (get_functions) xs)
  | Times(_, a) -> get_functions a
  | Qed.Logic.Eq(a, b) -> (get_functions a) @ (get_functions b)
  | Qed.Logic.Neq(a, b) ->(get_functions a) @ (get_functions b)
  | Qed.Logic.Lt(a, b) ->(get_functions a) @ (get_functions b)
  | If(a, b, c) -> (get_functions a) @ (get_functions b) @ (get_functions c)
  | And xs -> List.concat (List.map (get_functions)xs)
  | Or xs -> List.concat (List.map (get_functions)xs)
  | Fun(f, xs) -> f::List.concat (List.map (get_functions)xs)
  | Bind (_, _, b) -> get_functions (lc_repr b) 
  | Aget(a, b) -> (get_functions a) @ (get_functions b)
  | Aset(a, b, c) -> (get_functions a) @ (get_functions b) @ (get_functions c)
  | Rget(term, _) -> get_functions term 
  | Rdef fts -> let (_, xs) = List.split fts 
		in List.concat (List.map (get_functions) xs)
  | Apply(p, xs) -> get_functions p @  List.concat (List.map (get_functions) xs)
  | Kint _ -> []
  | Kreal _ -> []
  | Mul xs -> List.concat (List.map (get_functions) xs)
  | Div(a, b) -> (get_functions a) @ (get_functions b)
  | Mod(a, b) -> (get_functions a) @ (get_functions b)
  | Fvar _ -> []
  | Bvar (_,_) -> []
  | Imply(xs, p) -> get_functions p @  List.concat (List.map (get_functions) xs)

type config = {
    model : Model.t;
    vars : Lang.F.var list;
    bvars : Lang.F.Vars.elt list;
    pool : Lang.F.pool;
    blabels : pair list;
  } 

exception Found

let find_lfun m n =
  let found = ref None in
  try
    Lang.iter_on_builtin_funs 
      (fun k _ -> if name_of_fun k = n then (found := Some k;raise Found));
    Definitions.iter_on_functions 
      m (fun l _ -> if name_of_fun l = n then (found := Some l; raise Found));
    raise Not_found
  with Found -> 
    match !found with
    | None -> raise Not_found
    | Some lf -> lf


let rec make_term_from_lexpr (c:config) (term: Logic_ptree.lexpr):Lang.F.term =
  match term.lexpr_node with
  | PLvar varstring -> (
    let var_name () =
      try
	let k = List.find(fun el -> el.key = varstring) c.blabels in k.value
      with
	Not_found -> varstring
    in
    let v = var_name () in
    try 
      let c_var = List.find (fun vc -> let vc_name = name_of_var vc
				       in v = vc_name)  c.bvars in
      e_var c_var
    with Not_found ->
      try
	let vars = c.vars in
	let c_var = List.find (fun vc -> let vc_name = name_of_var vc
					 in v = vc_name) vars in
	e_var c_var
      with
	Not_found -> raise (Failure ("PO does not have variable " ^ v)))
  |PLapp (n,_,l) -> (  
    try 
      let n_func = find_lfun c.model n in
      let terms = List.map (make_term_from_lexpr c) l in
      e_fun n_func terms
    with Not_found -> 
      raise (Failure ("Function " ^ n ^ " was not found in this PO"))
  )
  |PLlambda (_q,_l) -> raise (Failure "PLlambda not implemented yet")
  |PLlet (_s,_l,_r) -> raise (Failure "PLlet not implemented yet")
  |PLunop (u,l) ->(
    let expr = make_term_from_lexpr c l in 
    match u with
    |Uminus -> e_mul e_minus_one expr
    |Ustar -> raise (Failure "Ustar not implemented yet")
    |Uamp -> raise (Failure "Uamp not implemented yet")
    |Ubw_not -> raise (Failure "Ubw_not not implemented yet") 
  )
  |PLconstant c -> ( 
    match c with
    | IntConstant i -> e_int (int_of_string i)
    | FloatConstant f -> e_real (Qed.R.of_string f)
    | StringConstant _s -> raise (Failure "String not implemented yet") 
    | WStringConstant _w -> raise (Failure "WString not implemented yet")
  )
  |PLrel (l,o,r) -> (
    let left = make_term_from_lexpr c l in
    let right = make_term_from_lexpr c r in 
    match o with
    | Eq -> e_eq left right
    | Lt -> e_lt left right
    | Gt -> e_lt right left
    | Le -> e_leq left right
    | Ge -> e_leq right left
    | Neq -> e_neq left right
  )
  |PLbinop (l,o,r) -> (
    let left = make_term_from_lexpr c l in 
    let right = make_term_from_lexpr c r in
    match o with
    |   Badd -> e_sum [left;right]
    |	Bsub -> e_sub left right
    |	Bmul -> e_mul left right
    |	Bdiv -> e_div left right
    |	Bmod -> e_mod left right
    |	Bbw_and -> e_and [left;right]
    |	Bbw_or -> e_or [left;right]
    |	Bbw_xor -> e_and [e_or [left;right];e_not (e_and [left;right])]
    |	Blshift -> raise (Failure "Blshift not implemented yet")
    |	Brshift -> raise (Failure "Brshift not implemented yet")
  )
  | PLdot (_l,_s) -> raise (Failure "dot not implemented yet")
  | PLarrow (_l,_s) -> raise (Failure "arrow not implemented yet")
  | PLarrget (l,r) -> let left = make_term_from_lexpr c l in
		      let right = make_term_from_lexpr c r in
		      e_get left right
  | PLor (l,r) -> e_or [make_term_from_lexpr c l;
			make_term_from_lexpr c r]
  | PLxor (l,r) -> 
     let left = make_term_from_lexpr c l in 
     let right = make_term_from_lexpr c r in
     e_and [e_or [left;right];e_not (e_and [left;right])]
  | PLimplies (l,r) -> e_imply [make_term_from_lexpr c l]
			       (make_term_from_lexpr c r)
  |PLiff (l,r) ->
    let left = make_term_from_lexpr c l in 
    let right = make_term_from_lexpr c r in
    e_and [e_imply [left] right;e_imply [right] left]
  |PLold  _l -> raise (Failure "Old not implemented yet")    
  |PLat (_l,_s) -> raise (Failure "At not implemented yet")
  |PLresult -> raise (Failure "PLresult not supporte in formulas") 
  |PLnull -> raise (Failure "Null not implemented yet")
  |PLcast (_l,_r) -> raise (Failure "PLcast not implemented yet")
  |PLrange (_l,_r) -> raise (Failure "PLrange not implemented yet")
  |PLupdate (l,p,w) -> 
    let a = make_term_from_lexpr c l in
    let b =
    (match p with
    | [p1] -> (match p1 with
	       |PLpathIndex le -> make_term_from_lexpr c le
	       |_ -> raise (Failure "Syntax error on array update")
	      );
    | _ -> raise (Failure "only one field can be update at a time")
    ) in
    let c = 
      (match w with
       |PLupdateTerm le -> make_term_from_lexpr c le 
       |_ -> raise (Failure "PLupdateCont is not implemented")
      ) in
    e_set a b c
  |PLsizeof _l -> raise (Failure "PLsizeof not implemented yet")
  |PLsizeofE _l -> raise (Failure "PLsizeofE not implemented yet")
  |PLcoercion (_l,_r) -> raise (Failure "PLcoercion not implemented yet")
  |PLinitIndex _l -> raise (Failure "PLinitIndex not implemented yet")
  |PLinitField _l -> raise (Failure "PLinitField not implemented yet")
  |PLtypeof _l -> raise (Failure "PLtypeof not implemented yet")
  |PLif (l,m,r) -> 
    let left = make_term_from_lexpr c l in
    let mid = make_term_from_lexpr c m in
    let right = make_term_from_lexpr c r in
    e_if left mid right
  |PLcoercionE (_l,_r) -> raise 
			    (Failure "PLcoercionE not implemented yet") 
  |PLtrue -> e_true
  |PLfalse -> e_false
  |PLunion _l -> raise (Failure "PLunion not implemented yet")
  |PLinter _l -> raise (Failure "PLinter not implemented yet")
  |PLempty -> raise (Failure "PLempty not implemented yet")
  |PLnamed (_s,_l) -> raise (Failure "PLnamed not implemented yet")
  |PLsubtype (_l,_r) -> raise (Failure "PLsubtype not implemented yet")
  |PLcomprehension (_l,_q,_r) -> 
    raise (Failure "PLcomprehension not implemented yet")
  |PLsingleton _l -> raise (Failure "PLsingleton not implemented yet")
  |PLinitialized (_s,_l) -> 
    raise (Failure "PLinitialized not implemented yet")
  |PLfresh (_o,_l,_r) -> raise (Failure "PLfresh not implemented yet")
  |PLseparated _l -> raise (Failure "PLseparated not implemented yet")
  |PLvalid_read (_s,_l) -> 
    raise (Failure "PLvalid_read not implemented yet")
  |PLallocable (_s,_l) -> 
    raise (Failure "PLallocable not implemented yet")
  |PLfreeable (_s,_l) -> raise (Failure "PLfreeable not implemented yet")
  |PLdangling (_s,_l) -> raise (Failure "PLdangling not implemented yet")
  |PLbase_addr (_s,_l) -> raise (Failure "PLbaseªddr not implemented yet")
  |PLoffset (_s,_l) -> raise (Failure "PLoffset not implemented yet")
  |PLblock_length (_s,_l) -> 
    raise (Failure "PLblock_length not implemented yet")
  |PLvalid (_s,_l) -> raise (Failure "PLvalid not implemented yet")
  |PLand (l,r) -> e_and [make_term_from_lexpr c l;make_term_from_lexpr c r]
  |PLnot l -> e_not (make_term_from_lexpr c l)
  |PLexists (q,l) ->
    let newlnames = ref [] in
    let new_vars (lt,name) =
      let t = tau_of_logic_type lt in 
      let v = fresh ~basename:name c.pool t in
      let kv = {
	  key=name;
	  value = name_of_var v
	} in
      newlnames := !newlnames @ [kv];
      Lang.F.add_var c.pool v;
      v
    in
    let bound_vars = List.map new_vars q in
    let c2 = {c with
	      blabels = c.blabels@(!newlnames);
	      bvars = c.bvars @ bound_vars;
	    } in
    let term = make_term_from_lexpr c2 l in
    e_exists bound_vars term
  |PLforall (q,l) -> 
    let newlnames = ref [] in
    let new_vars (lt,name) =
      let t = tau_of_logic_type lt in
      let v = fresh ~basename:name c.pool t in
      let kv = {
	  key=name;
	  value = name_of_var v
	} in
      newlnames := !newlnames @ [kv];
      Lang.F.add_var c.pool v;
      v
    in
    let bound_vars = List.map new_vars q in
    let c2 = {c with
	      blabels = c.blabels@(!newlnames);
	      bvars = c.bvars @ bound_vars;
	    } in
    let term = make_term_from_lexpr c2 l in
    e_forall bound_vars term
  |PLtype _l -> raise (Failure "PLtype not implemented yet")

let create_pool vars =
  let pool = Lang.new_pool () in
  Lang.F.add_vars pool vars;
  pool

let mk_term c args =
  let con = String.concat " " args in
  let lexb = Lexing.from_string con in
  try
    let lexpr = Logic_parser.lexpr_eof 
		  (fun lb -> (Logic_lexer.token lb) ) lexb in
    make_term_from_lexpr c lexpr
  with 
    Failure m ->  raise (Failure m)
  | Logic_utils.Not_well_formed (_loc,m) -> raise (Failure m)
  | Parsing.Parse_error ->
     raise (Failure ("Argument " ^ con ^ " is not syntactically correct."))

let mk_config wpo =
  let vars = get_vars wpo in
  let pool = create_pool vars in
  {
    vars = (Lang.F.Vars.elements vars);
    model = wpo.Wpo.po_model;
    bvars = [];
    pool = pool;
    blabels = [];
  } 
