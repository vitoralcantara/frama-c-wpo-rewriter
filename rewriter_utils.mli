(* Names and string_of functions *)

val string_of_term : Lang.F.term -> string
val string_of_pred : Lang.F.pred -> string
val string_of_sort : Qed.Logic.sort -> string
val name_of_fun : Lang.F.Fun.t -> string
val name_of_var : Lang.F.var -> string
val get_def_text : Wpo.po -> string
val string_of_wpo : Wpo.t -> (string list * string)
val string_of_step : Conditions.step -> string
val string_list_of_hyps : Conditions.step list -> string list 
val string_of_result : VCS.result -> string
val failed_msg : po_gid:string -> tactic:string -> string
val string_of_vc_lemma:Wpo.VC_Lemma.t -> string list * string
val string_of_vc_annot:Wpo.VC_Annot.t -> string list * string
							 
(* Other functions *)

(*val base_dfuns : Definitions.dfun list*)
(*val base_lfuns : Lang.F.Fun.t list*)
val reset_proof_obligations : unit -> unit
(*val get_model_lfuns : Model.model -> Lang.lfun list*)
val get_vars : Wpo.t -> Lang.F.Vars.t
val get_goal : Wpo.t -> Lang.F.pred
val get_hyps : Wpo.t -> Conditions.hypotheses
val get_predicate_from_step : Conditions.step -> Lang.F.pred
val is_branch : Conditions.step -> bool
val get_root : Wpo.t -> Wpo.t
val get_assumptions : Wpo.VC_Lemma.t -> Lang.F.pred list
val get_step_from_index : Wpo.t -> int -> Conditions.step
val po_not_found_message : string
val get_wpo_by_po_gid : string -> Wpo.t
val get_wpo_by_po_pid : WpPropId.prop_id -> Wpo.t
val replace_wpo_in_WpoSet : Wpo.t -> Wpo.t list -> unit
val get_verdict : Wpo.t -> VCS.verdict
val get_result : Wpo.t -> VCS.result

val get_step_by_order : Wpo.t -> int -> Conditions.step
val has_implication : Lang.F.term -> bool
val has_leq : Lang.F.term -> bool
val is_condition : Lang.F.term -> bool
val is_subterm : Lang.F.term -> Lang.F.term -> bool
val is_conjunction_of_terms : Lang.F.term -> bool
val is_disjunction_of_terms : Lang.F.term -> bool
val get_number_of_disjunctions : Lang.F.term -> int
val build_string_step_relation : Conditions.step list ->
				 (string * Conditions.step) list
val get_descendants : Wpo.t -> Wpo.t list
val cut_wpo_children : Wpo.t -> Wpo.t
val get_signature_reprs_in_string : Wpo.t -> string list
val update_rewriting_status_for_parents : Wpo.t -> unit
val find_var : Wpo.t -> name:string -> Lang.F.var
val update_parts : Wpo.t -> int -> int -> unit
