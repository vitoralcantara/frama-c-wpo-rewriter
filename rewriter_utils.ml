open Conditions
open Definitions
open Wpo
open Qed.Logic
open Lang.F
open WpoRewriter_infRules_names
open MemTyped
let po_not_found_message = "PO not found"

(***************)
(* Definitions *)
(***************)

let string_of_result r = 
  VCS.pp_result Format.str_formatter r;
  Format.flush_str_formatter ()

let name_of_var var = 
  pp_var Format.str_formatter var;
  String.trim (Format.flush_str_formatter ())

let string_of_field field =
  Field.pretty Format.str_formatter field;
  Format.flush_str_formatter ()

let string_of_tau t = 
  Tau.pretty Format.str_formatter t;
  Format.flush_str_formatter ()

let rec string_of_sort sort =
  match sort with
  | Sprop -> "Prop"
  | Sbool -> "Bool"
  | Sint -> "int"
  | Sreal -> "real"
  | Sdata -> "data"
  | Sarray sort -> "Array[" ^ (string_of_sort sort) ^ "]"

let name_of_fun f =
  Fun.pretty Format.str_formatter f;
  Format.flush_str_formatter ()

let string_of_term (term:term) =
  Lang.F.pp_term Format.str_formatter term;
  let fmt = Format.flush_str_formatter () in
  String.trim fmt

let string_of_pred pred = string_of_term (e_prop pred)
  
module FunElt =
  struct 
    type t = Definitions.dfun    
    let compare f1 f2 = Lang.F.Fun.compare 
			  f1.Definitions.d_lfun f2.Definitions.d_lfun
  end

module LemmaElt =
  struct 
    type t = Definitions.dlemma
    let compare d1 d2 = String.compare d1.Definitions.l_name d2.Definitions.l_name
  end

module FunSet = Set.Make(FunElt)
module LemmaSet = Set.Make(LemmaElt)

(*
let rec string_of_term term =
match repr term with
  | Leq(a, b) -> (string_of_term a) ^ (" <= ") ^ (string_of_term b)
  | True -> "true"
  | False -> "false"
  | Not p -> "!" ^ (string_of_term p)
  | Add xs -> String.concat " + " (List.map (string_of_term) xs)
  | Mul xs -> String.concat " * " (List.map (string_of_term) xs)
  | Times(t, a) -> (Lang.F.Z.to_string t) ^ " * " ^ (string_of_term a)
  | Eq(a, b) -> (string_of_term a) ^ " == " ^ (string_of_term b)
  | Neq(a, b) -> (string_of_term a) ^ " != " ^ (string_of_term b)
  | Lt(a, b) ->  (string_of_term a) ^ " <= " ^ (string_of_term b)
  | If(a, b, c) -> "if(" ^ (string_of_term a) ^ "){\n\t" ^ (string_of_term b) ^ 
		     "\n} else {\n\t" ^ (string_of_term c)
  | And ts -> String.concat " && " (List.map (string_of_term) ts)
  | Or ts -> String.concat " || " (List.map (string_of_term) ts)
  | Fun(f, ts) -> (name_of_fun f) ^ "(" ^
		    String.concat "," (List.map (string_of_term) ts) ^
		      ")"
  | Bind (q, v, b) -> 
     let prefix = (match q with
		   |Forall -> "\\forall"
		   |Exists -> "\\exists"
		   |Lambda -> "\\lambda"
		  ) in
     prefix ^ " " ^ (string_of_sort (Qed.Kind.of_tau v)) ^
       ":" ^ (string_of_tau v) ^ "; " ^ (string_of_term (lc_repr b))
  | Aget(a, b) -> (string_of_term a) ^ "[" ^ (string_of_term b) ^ "]"
  | Aset(a, b, c) -> "{ " ^ (string_of_term a) ^ " \\with " ^ (string_of_term b) ^
		       " = { " ^ (string_of_term c) ^ " }"
  | Rget(term, var) ->  (string_of_term term) ^ "." ^ (string_of_field var)
  | Rdef fts -> 
     "{" ^ (String.concat "; " 
			  (List.map (fun (field,term) -> (string_of_field field) 
							 ^ " = " ^ (string_of_term term))fts))
     ^ "}"
  | Apply(_p, _hs) -> print_endline "apply";
		      raise (Failure ("string of Apply is not implemented. No parsed term: " ^ 
					string_of_term term
			    )) 
  | Kint k -> Lang.F.Z.to_string k
  | Kreal k -> Qed.R.to_string k
  | Div(a, b) -> (string_of_term a) ^ " / " ^ (string_of_term b)
  | Mod(a, b) ->(string_of_term a) ^ " % " ^ (string_of_term b)
  | Fvar v -> name_of_var v
  | Bvar (i,_t) -> "#" ^ (string_of_int i)
  | Imply(hs, p) -> let left = String.concat " ==> " (List.map (string_of_term) hs) in
		    left ^ " ==> " ^ (string_of_term p)
 *)

let get_lemma_term dlemma = 
  e_forall dlemma.l_forall
	   (e_imply (List.map (fun p -> e_prop p) dlemma.l_assumptions)
		    (e_prop dlemma.l_lemma))

let string_of_dlemma dlemma =
  let name = dlemma.l_name in
  let lterm = get_lemma_term dlemma in
  "Lemma " ^ name ^ " = " ^ (string_of_term lterm)

let string_of_tau tau = string_of_sort (Qed.Kind.of_tau tau)

let string_of_dfun dfun =
  let name = name_of_fun dfun.d_lfun in
  let params = List.map 
		 (fun v -> 
		  (name_of_var v) ^ ":" ^ (string_of_sort (sort_of_var v))) 
		 dfun.d_params in
  let param_string = String.concat "," params in
  match dfun.d_definition with
  | Logic tau -> "function " ^ name ^ ": " ^ (string_of_tau tau) 
  | Value (tau,r,term) -> (
    let prefix = match r with
      |Def -> "function"
      |Rec -> "rec function"
    in
    prefix ^ " " ^ name ^ "(" ^ param_string ^ "): " ^ (string_of_tau tau)
    ^ " = " ^ (string_of_term term)
  )
  |Predicate (r,pred) -> (
    let prefix = match r with
      |Def -> "function"
      |Rec -> "rec function" 
    in
    prefix ^ " " ^ name ^ "(" ^ param_string ^ "): " ^ (string_of_pred pred)
  )
  |Inductive (dlemmas) ->
    let dl_strings = List.map (fun dl -> dl.l_name) dlemmas in
    let dls = String.concat "," dl_strings in
    "Inductive" ^ name ^ "(" ^ param_string ^ "): " ^ dls

let get_assumptions lemma =
  lemma.Wpo.VC_Lemma.lemma.Definitions.l_assumptions

let get_step_from_index w (hyps_index:int) =
  match w.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (hyps, _) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     List.nth hyps hyps_index
  | Wpo.GoalLemma _ -> raise (Failure "GoalLemma not implemented yet")
  | Wpo.GoalCheck _ -> raise (Failure "GoalCheck not implemented yet")


let rec get_root wpo =
  match wpo.Wpo.po_partac with
  | None -> wpo
  | Some (_,p,_) -> get_root p

let delete_leaves root = root.Wpo.po_leaves <- []

let reset_proof_obligations () =
  let replace_by_parent wpo =
    if wpo.Wpo.po_partac <> None then
      Wpo.remove wpo;
    let root = get_root wpo in
    delete_leaves root;
    Wpo.add root
  in
  Wpo.iter_on_goals replace_by_parent
		    
let rec find_wpo_in_root wpo wpo_gid =
  let wpo_ref = ref None in
  if wpo.Wpo.po_gid = wpo_gid then
    Some wpo
  else
    let leaves = wpo.Wpo.po_leaves in
    let find wpo =
      (
	let result = find_wpo_in_root wpo wpo_gid in
	if result <> None then
	  wpo_ref := result
      )
    in
    List.iter find leaves;
    !wpo_ref


let pp_lfuns m =
  let str = ref "" in
  Lang.iter_on_builtin_funs 
    (fun k _v -> str := !str ^ "function " ^ (name_of_fun k) ^ "\n\n");
  Definitions.iter_on_functions 
    m (fun _l d -> str := !str ^ string_of_dfun d ^ "\n\n");
  !str

let get_def_text wpo =
  let lemmas = ref LemmaSet.empty in
  let funs_string = pp_lfuns wpo.Wpo.po_model in
  Definitions.iter_on_lemmas 
    wpo.Wpo.po_model (fun _lname lemma -> lemmas := LemmaSet.add lemma !lemmas);
  let string_of_lemmas = List.map (fun l -> string_of_dlemma l) (LemmaSet.elements !lemmas) in
  let text = String.concat "\n\n" ("Functions:"::funs_string::"Lemmas:"::string_of_lemmas) in
  text

let contains s1 s2 =
  let re = Str.regexp_string s2 in
  try 
    ignore (Str.search_forward re s1 0); 
    true
  with Not_found -> false


let get_goal wpo =
  match wpo.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (_, goal) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     goal
  | Wpo.GoalLemma lemma -> lemma.Wpo.VC_Lemma.lemma.Definitions.l_lemma
  | Wpo.GoalCheck _ -> raise (Failure "GoalCheck not implemented yet")

let get_hyps wpo =
  match wpo.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (hyps, _) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     hyps
  | Wpo.GoalLemma _ -> []
  | Wpo.GoalCheck _ -> []


let is_branch step =
  let condition = step.condition in
  match condition with
  |Branch (_,_,_) -> true
  |_ -> false


let replace_if_else_by_connectives cond pred1 pred2: Lang.F.pred =
  let first_case = Lang.F.p_conj [cond; pred1] in
  let neg_cond = p_not cond in
  let second_case = Lang.F.p_conj [neg_cond; pred2] in
  Lang.F.p_disj [first_case; second_case]

let rec apply_fun_in_list (f:pred -> pred -> pred) (preds:pred list):pred =
  match preds with
  |[] -> raise (Failure "Cannot apply function on empty array");
  |[p] -> p
  |p::o -> f p (apply_fun_in_list f o)	     
	     
let rec make_predicate_from_sequence seq: Lang.F.pred =
  let step_list = seq.seq_list in
  let predicates = List.map (get_predicate_from_step) step_list in
  Lang.F.p_conj predicates

and get_predicate_from_step step =
  match step.Conditions.condition with
  | Core pred | Init pred | Type pred | Have pred | When pred -> pred
  | Branch (cond,seq1,seq2) ->     
     let pred1 = make_predicate_from_sequence seq1 in
     let pred2 = make_predicate_from_sequence seq2 in
     replace_if_else_by_connectives cond pred1 pred2
  | Either sl ->
     apply_fun_in_list
       p_or (List.map (
		 fun sq ->
		 let stepl = sq.seq_list in
		 apply_fun_in_list
		   p_and (List.map (get_predicate_from_step) stepl)
	       ) sl
	    )

let rec add_varsSet hypotheses =
  match hypotheses with
  |[] -> Vars.empty
  | hyp::other_hyps ->
     let varsSet = add_varsSet other_hyps in
     let pred = (
       if not (is_branch hyp) then 
	 get_predicate_from_step hyp 
       else
	 get_predicate_from_step hyp
     ) in 
     let vS = varsp pred in
     Vars.union vS varsSet  

let get_vars wpo =
  let goal = get_goal wpo in
  let hypotheses = get_hyps wpo in
  let varsSet = add_varsSet hypotheses in
  let goal_vars = varsp goal in
  let varsSet = Vars.union goal_vars varsSet in
  varsSet

let find_var wpo ~name =
  let vars = get_vars wpo in
  try
    List.find
      (fun (v:Lang.F.Vars.elt) ->
       if name_of_var v = name then true
       else false
      ) (Lang.F.Vars.elements vars)
  with Not_found ->
    raise (Failure ("Variable " ^ name ^ " was not found in the context of this proof obligation"))

let get_best_result_from_result previous_result current_result =
  match previous_result.VCS.verdict with
  |VCS.Valid -> VCS.Valid
  |VCS.NoResult 
  |VCS.Invalid
  |VCS.Unknown 
  |VCS.Timeout
  |VCS.Stepout
  |VCS.Computing _
  |VCS.Checked
  |VCS.Failed -> current_result.VCS.verdict

let result_to_string result =
  match result.VCS.verdict with
  |VCS.Valid -> "Valid"
  |VCS.NoResult -> "NoResult"
  |VCS.Invalid -> "Invalid"
  |VCS.Unknown -> "Unknown"
  |VCS.Timeout -> "Timeout"
  |VCS.Stepout -> "Stepout"
  |VCS.Computing _ -> "Computing"
  |VCS.Failed -> "Failed"
  |VCS.Checked -> "Checked"


let compare res1 res2 =
  if res1 = res2 then 0 else
    match res1,res2 with
    |_,VCS.Valid -> 1
    |VCS.Valid,_ -> -1
    |VCS.Checked,_ -> -1
    |_,VCS.Checked -> 1
    |VCS.Computing _,_ -> -1
    |_, VCS.Computing _ -> 1
    |VCS.Stepout ,_ -> -1
    |_, VCS.Stepout -> 1
    |VCS.Invalid,_-> -1
    |_,VCS.Invalid -> 1
    |VCS.Timeout,_ -> -1
    |_,VCS.Timeout -> 1
    |VCS.Unknown,_ -> -1
    |_,VCS.Unknown -> 1
    |VCS.Failed,_ -> -1
    |_,VCS.Failed -> 1
    |VCS.NoResult,_ -> -1
			  
let get_result wpo =
  let results = Wpo.get_results wpo in
  let best_result = ref VCS.no_result in
  List.iter 
    (fun pair_result ->
     let (_prover,result) = pair_result in
     let verdict = result.VCS.verdict in
     if compare !best_result.VCS.verdict verdict = 1 then
       best_result := result
    ) results;  
  !best_result

let get_verdict wpo =
  let results = Wpo.get_results wpo in
  let best_result = ref VCS.NoResult in
  List.iter 
    (fun pair_result ->
     let (_prover,result) = pair_result in
     let verdict = result.VCS.verdict in
     if compare !best_result verdict = 1 then
       best_result := verdict
    ) results;  
  !best_result

(*
updates status for all wpo's antecessors
 *)
let rec update_rewriting_status_for_parents wpo:unit =
  let valid = ref true in
  let no_result = ref true in
  List.iter (
      fun w -> 
      match (get_verdict w) with
      |VCS.Valid -> no_result := false;
      |VCS.NoResult -> valid := false;
      |_ -> no_result := false; valid := false
    )wpo.Wpo.po_leaves;
  (if !valid then Wpo.set_result wpo VCS.Rewriter VCS.valid
   else if !no_result then Wpo.set_result wpo VCS.Rewriter VCS.no_result
   else Wpo.set_result wpo VCS.Rewriter VCS.unknown	     
  );
  match wpo.Wpo.po_partac with
  |None -> ()
  |Some (_,par,_) -> update_rewriting_status_for_parents par

let name_of_var_type var =
  let tau = Lang.F.tau_of_var var in
  ignore (Lang.F.pp_tau Format.str_formatter tau);
  Format.flush_str_formatter ()
							 
let get_signature_in_string var =
  let var_name = name_of_var var in
  let var_type_string = name_of_var_type var in
  var_name ^ ":" ^ var_type_string

		     (*
  Lang.F.pp_var Format.str_formatter var;
  Format.flush_str_formatter ()
			     Lang
		      *)	     
  (*let name = String.trim (name_of_var var) in
  let sort = sort_of_var var in
  let sort_string = string_of_sort sort  in
  name ^ " : " ^ sort_string*)

let get_signature_reprs_in_string wpo =
  let vars_list = Lang.F.Vars.elements (get_vars wpo) in
  let vars_signatures  = List.map (get_signature_in_string) vars_list in
  vars_signatures
   
(* TODO: talk about this function *)
let update_parts wpo current sum = 
  let wpo_prop = wpo.Wpo.po_pid in
  let update cur_w:unit = 
    let prop_cur_w = cur_w.Wpo.po_pid in
    if (WpPropId.compare_prop_id_kind_and_property wpo_prop prop_cur_w) = 0 then (
      let part = WpPropId.parts_of_id prop_cur_w in (
	match part with 
	|None -> raise (Failure "Wpo should be a part")
	|Some (p,t) ->	  
	  if p >= current then
	    WpPropId.update_parts cur_w.Wpo.po_pid (Some (p+sum,t+sum))
	  else WpPropId.update_parts cur_w.Wpo.po_pid (Some (p,t+sum))
    ))
    else ()
  in
  Wpo.iter_on_goals update

let rec get_descendants wpo =
  let children = wpo.Wpo.po_leaves in
  let total_descendants = ref children in
  let length = List.length children in
  for i = 0 to (length-1) do
    let child = List.nth children i in
    let descendants = get_descendants child in
    total_descendants := !total_descendants @ descendants
  done;
  !total_descendants

let rec remove_leaves_from_WpoSet wpo =
  let leaves = wpo.Wpo.po_leaves in
  let remove_leaf leaf =
    Wpo.remove leaf;
    remove_leaves_from_WpoSet leaf
  in
  List.iter remove_leaf leaves

let cut_wpo_children wpo:Wpo.t =
  let length = List.length wpo.Wpo.po_leaves in
  if length >= 1 then (
    remove_leaves_from_WpoSet wpo;
    let max:int ref = ref ~-1 in
    List.iter (
	fun w -> 
	let wpo_part = WpPropId.parts_of_id w.Wpo.po_pid in
	match wpo_part with
	|None -> ()
	| Some (pt,_tt) -> if pt > !max then max := pt
      ) wpo.Wpo.po_leaves;
    let diff = ~-(length - 1) in
    let wpo_part = WpPropId.parts_of_id wpo.Wpo.po_pid in (
      match wpo_part with
      | Some _ -> update_parts wpo !max diff
      | None -> ()
    );
  );
  Wpo.add wpo;
  wpo.Wpo.po_leaves <- [];
  wpo
    
let rec string_of_step_list sl = String.concat "\n" (List.map (string_of_step) sl)

and string_of_sequences (seqs:Conditions.sequence list) =
  match seqs with
  |[] -> ""
  |[s] -> string_of_step_list s.seq_list
  |s::t -> string_of_step_list s.seq_list ^ "\n or \n" ^
	     string_of_sequences t
    
and string_of_step step  =
  match step.Conditions.condition with
  | Core p
  | Init p
  | Type p
  | Have p
  | When p -> string_of_pred p
  | Branch(p,a,b) ->
     "If " ^ (string_of_pred p) ^
       "{\n " ^ (string_of_step_list a.seq_list) ^ "\n} else {\n " ^ 
       (string_of_step_list b.seq_list) ^ "\n}"
  | Either cases -> "Either:\n " ^ string_of_sequences cases

let get_number_of_disjunctions pred =
  match Lang.F.repr pred with
  | Or preds -> List.length preds
  | _ -> 1

let string_list_of_hyps hypotheses = 
  let func hyp =
    let hyp_string = string_of_step hyp in hyp_string
  in
  List.map (func) hypotheses

let build_string_step_relation hypotheses = 
  let func hyp =
    let hyp_string = string_of_step hyp in (hyp_string, hyp)
  in
  List.map (func) hypotheses

let is_conjunction_of_terms pred =
  match repr pred with
  | And _ -> true
  | _ -> false

let is_disjunction_of_terms pred =
  match repr pred with
  |Or _ -> true
  |_ -> false

let string_of_vc_lemma vclemma =
  let env = Lang.F.env (List.fold_right Lang.F.Vars.add vclemma.VC_Lemma.lemma.l_forall
					Lang.F.Vars.empty) in
  Lang.F.pp_epred env Format.str_formatter vclemma.VC_Lemma.lemma.l_lemma;
  let goal_string = Format.flush_str_formatter () in
  let hyps_strings = List.map
		       (fun pred ->
			Lang.F.pp_epred env Format.str_formatter pred;
			Format.flush_str_formatter ()
		       ) vclemma.VC_Lemma.lemma.l_assumptions in
  (hyps_strings,goal_string)

    let rec string_of_hyp step =
  let fmt = Format.str_formatter in
  let pred = get_predicate_from_step step in
  let vars = Lang.F.varsp pred in
  let env = Lang.F.env vars in
  let _ = (
    match step.Conditions.condition with
    | Core p
    | Init p
    | Type p 
    | Have p 
    | When p -> pp_clause fmt env "" p
    | Branch(p,a,b) ->
       begin
         Format.fprintf fmt "@ @[<hov 2>@{<bf>If@}: %a@]"
			(Lang.F.pp_epred env)
			p ;
         if a.seq_list<>[] then
	   (Format.pp_print_string fmt "\nThen:";
	    List.iter(fun v -> Format.pp_print_string
				 fmt (string_of_hyp v)) a.seq_list;
			Format.pp_print_string fmt "\n}");
         if b.seq_list<>[] then
	   (Format.pp_print_string fmt "\nElse{";
	    List.iter(fun v -> Format.pp_print_string
				 fmt (string_of_hyp v)) b.seq_list; 
	    Format.pp_print_string fmt "\n}");
       end
    | Either cases ->
       begin
         Format.fprintf fmt "@[<hv 0>@[<hv 2>@{<bf>Either@} {" ;
         List.iter 
           (fun seq -> 
            Format.fprintf fmt "@ @[<hv 2>@{<bf>Case@}:" ;
            pp_block fmt env seq.seq_list ;
            Format.fprintf fmt "@]" ;
           ) cases ;
         Format.fprintf fmt "@]@ }@]" ;
       end
  )
  in Format.flush_str_formatter ()
				
let string_of_vc_annot vc_annot =
  let sequent = Wpo.GOAL.compute_descr vc_annot.VC_Annot.goal in
  let env = Lang.F.env (Conditions.vars_sequent sequent) in
  let (hyps,goal) = sequent in
  let hyps_strings = List.map(string_of_step) hyps in
(* let hyps_strings = List.map (fun h -> pp_step Format.str_formatter env h;
					  Format.flush_str_formatter ()) hyps in*)
    let goal_string = (Lang.F.pp_epred env Format.str_formatter goal;
		       Format.flush_str_formatter ()) in
    (hyps_strings,goal_string)
    
let rec has_leq pred =
  match Lang.F.repr pred with
  | Leq(_, _) -> true
  | True -> false
  | False -> false
  | Fvar _ -> false
  | Bvar (_,_) -> false
  | Not p -> has_leq p
  | Kint _ -> false
  | Kreal _ -> false
  | Add xs -> List.exists (has_leq) xs
  | Mul xs -> List.exists (has_leq) xs
  | Div(a, b) -> if has_leq a then true else has_leq b
  | Mod(a, b) -> if has_leq a then true else has_leq b
  | Times(_, a) -> has_leq a
  | Eq(a, b) -> if has_leq a then true else has_leq b
  | Neq(a, b) -> if has_leq a then true else has_leq b
  | Lt(a, b) -> if has_leq a then true else has_leq b
  | Aget(a, k) -> if has_leq a then true else has_leq k
  | Aset(a, k, v) ->
     if has_leq a then true
     else if has_leq k then true
     else has_leq v
  | Rget(r, _) -> has_leq r
  | Rdef fts ->
     let (_, terms) = List.split fts in
     List.exists (has_leq) terms
  | If(a, b, c) ->
     if has_leq a then true
     else if has_leq b then true
     else has_leq c
  | And ts -> List.exists (has_leq) ts
  | Or ts -> List.exists (has_leq) ts
  | Imply(hs, p) -> if List.exists (has_leq) hs then true else has_leq p
  | Apply(e, es) -> if has_leq e then true else List.exists (has_leq) es
  | Fun(_, ts) -> List.exists (has_leq) ts
  | Bind (_, _, c) -> has_leq (lc_repr c)


let get_step_by_order wpo step_index =
  match wpo.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (hyps, _) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     List.nth hyps step_index
  | Wpo.GoalLemma _ -> raise (Failure "function is not available to GoalLemma")
  | Wpo.GoalCheck _ -> raise (Failure "function is not available to GoalCheck")


let replace_wpo_in_WpoSet old_wpo new_wpo_list =
  old_wpo.Wpo.po_leaves <- old_wpo.Wpo.po_leaves @ new_wpo_list;
  Wpo.remove old_wpo; 
  List.iter (Wpo.add) new_wpo_list


let rec get_wpo_by_po_gid_in_list po_gid wpo_list =
  match wpo_list with
  | [] -> None
  | hd:: tl -> let result = get_wpo_by_po_gid_in_wpo po_gid hd in
	       match result with
	       | None -> get_wpo_by_po_gid_in_list po_gid tl
	       | Some wpo -> Some wpo

and get_wpo_by_po_gid_in_wpo po_gid wpo =
  if wpo.Wpo.po_gid = po_gid then Some wpo else
    let leaves = wpo.Wpo.po_leaves in
    get_wpo_by_po_gid_in_list po_gid leaves


let rec get_wpo_by_po_pid_in_list po_pid wpo_list =
  match wpo_list with
  | [] -> None
  | hd:: tl -> let result = get_wpo_by_po_pid_in_wpo po_pid hd in
	       match result with
	       | None -> get_wpo_by_po_pid_in_list po_pid tl
	       | Some wpo -> Some wpo

and get_wpo_by_po_pid_in_wpo po_pid wpo =
  let pretty_po_pid = (
    WpPropId.pretty Format.str_formatter po_pid;
    Format.flush_str_formatter ()
  ) in
  let pretty_wpo_po_pid = (
    WpPropId.pretty Format.str_formatter wpo.Wpo.po_pid;
    Format.flush_str_formatter ()
  ) in
  if pretty_po_pid = pretty_wpo_po_pid then Some wpo 
  else
    let leaves = wpo.Wpo.po_leaves in
    get_wpo_by_po_pid_in_list po_pid leaves

let get_wpo_by_po_gid po_gid =
  let selected_wpo = ref None in
  let get_wpo wpo =
    (
      let root = get_root wpo in
      let result = get_wpo_by_po_gid_in_wpo po_gid root in
      match result with
      | None -> ()
      | Some _ -> selected_wpo := result
    )
  in
  Wpo.iter_on_goals get_wpo;
  match !selected_wpo with
  | None -> raise (Failure ("PO: " ^ po_gid ^ " was not found"))
  | Some w -> w


let failed_msg ~po_gid ~tactic = 
  "Tactic:" ^ tactic ^ " cannot be used in PO:" ^ po_gid

let lemma_to_prove (lemma: Wpo.VC_Lemma.t) =
  let l = lemma.Wpo.VC_Lemma.lemma.Definitions.l_lemma in
  let s = Format.str_formatter in
  Lang.F.pp_pred s l;
  Format.flush_str_formatter ()


let is_condition pred =
  match repr pred with
  |If (_,_,_) -> true
  |_ -> false  

let rec has_implication pred =
  match Lang.F.repr pred with
  | Leq(a,b) -> has_implication a || has_implication b
  | True -> false
  | False -> false
  | Fvar _ -> false
  | Bvar (_,_) -> false
  | Not p -> has_implication p
  | Kint _ -> false
  | Kreal _ -> false
  | Add xs -> List.exists (has_implication) xs
  | Mul xs -> List.exists (has_implication) xs
  | Div(a, b) -> has_implication a || has_implication b
  | Mod(a, b) -> has_implication a || has_implication b
  | Times(_, a) -> has_implication a
  | Eq(a, b) -> has_implication a || has_implication b
  | Neq(a, b) -> has_implication a || has_implication b
  | Lt(a, b) -> has_implication a || has_implication b
  | Aget(a, k) -> has_implication a || has_implication k
  | Aset(a, k, v) -> has_implication a || has_implication k || has_implication v
  | Rget(r, _) -> has_implication r
  | Rdef fts -> let (_, terms) = List.split fts in List.exists (has_implication) terms
  | If(a, b, c) -> has_implication a || has_implication b || has_implication c
  | And ts -> List.exists (has_implication) ts
  | Or ts -> List.exists (has_implication) ts
  | Imply(_, _) -> true
  | Apply(e, es) -> has_implication e || List.exists (has_implication) es
  | Fun(_, ts) -> List.exists (has_implication) ts
  | Bind (_, _, c) -> has_implication (lc_repr c)

let rec is_subterm subterm term =
  if Lang.F.equal subterm term then true else
  match Lang.F.repr term with
  | Leq(a,b) -> is_subterm subterm a || is_subterm subterm b
  | True -> false
  | False -> false
  | Fvar _ -> false
  | Bvar (_,_) -> false
  | Not p -> is_subterm subterm p
  | Kint _ -> false
  | Kreal _ -> false
  | Add xs -> List.exists (is_subterm subterm) xs
  | Mul xs -> List.exists (is_subterm subterm) xs
  | Div(a, b) -> is_subterm subterm a || is_subterm subterm b
  | Mod(a, b) -> is_subterm subterm a || is_subterm subterm b
  | Times(_, a) -> is_subterm subterm a
  | Eq(a, b) -> is_subterm subterm a || is_subterm subterm b
  | Neq(a, b) -> is_subterm subterm a || is_subterm subterm b
  | Lt(a, b) -> is_subterm subterm a || is_subterm subterm b
  | Aget(a, k) -> is_subterm subterm a || is_subterm subterm k
  | Aset(a, k, v) -> is_subterm subterm a || is_subterm subterm k || is_subterm subterm v
  | Rget(r, _) -> is_subterm subterm r
  | Rdef fts -> let (_, terms) = List.split fts in List.exists (is_subterm subterm) terms
  | If(a, b, c) -> is_subterm subterm a || is_subterm subterm b || is_subterm subterm c
  | And ts -> List.exists (is_subterm subterm) ts
  | Or ts -> List.exists (is_subterm subterm) ts
  | Imply(_, _) -> true
  | Apply(e, es) -> is_subterm subterm e || List.exists (is_subterm subterm) es
  | Fun(_, ts) -> List.exists (is_subterm subterm) ts
  | Bind (_, _, c) -> is_subterm subterm (lc_repr c)
				      
let string_of_VC_lemma lemma =
  let lemma_string = lemma_to_prove lemma in
  ([], lemma_string)

let string_of_wpo w =
  match w.Wpo.po_formula with
  | GoalAnnot annot ->
     let (hyps, goal) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     let goal_string = string_of_pred goal in
     print_endline("goal=" ^ goal_string);
     (string_list_of_hyps hyps, goal_string)
  | GoalLemma lemma -> string_of_VC_lemma lemma 
  | GoalCheck _ -> raise (Failure "GoalCheck not implemented yet")


let pretty po_pid = 
  WpPropId.pretty Format.str_formatter po_pid;
  Format.flush_str_formatter ()


let get_wpo_by_po_pid po_pid =
  let selected_wpo = ref None in
  let get_wpo wpo =
    (
      let root = get_root wpo in
      let result = get_wpo_by_po_pid_in_wpo po_pid root in
      match result with
      | None -> ()
      | Some _ -> selected_wpo := result
    )
  in
  Wpo.iter_on_goals get_wpo;
  match !selected_wpo with
  | None -> raise (Failure ("PO: " ^ (pretty po_pid) ^ " was not found"))
  | Some w -> w
