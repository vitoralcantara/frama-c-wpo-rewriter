open WpoRewriter_infRules_names
(**Auxiliary functions *)
val get_precursor_tactics : Wpo.t -> tactic_s list
val string_of_inf_rule: tactic_s -> string
val execute_inf_rule : po_gid:string -> tactic_s -> Wpo.t
