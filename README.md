# README #

### What is this repository for? ###

This repository contains a plugin for Frama-C, a platform for source-code analysis of C software, and WP, a Frama-C's plugin for deductive veri
cation. Called WPTrans, this extension allows generated proof obligations from WP to be manipulated through inference rules and sent, at any step of proof, to SMT solvers or any other interactive theorem prover. Some proof obligations may be proved automatically, while others can be too complex to be solved by SMT solvers, requiring the users of Frama-C and WP to handle them manually: this approach usually requires a signi
cant
experience in proof strategies. Some interactive theorem provers provide communication
with automatic provers. However, this connection can be complex and incomplete, leaving
the user with the manual proof option only. The strength of WPTrans is to combine the
features of automatic and interactive theorem provers in a precise and complete way, with
a simple manipulation language. Thus, the user can simplify the proof obligations enough
so its proof may be concluded with an SMT solver, being the proof partially manual and
partially automatic. Nevertheless, the plugin is directly linked do WP, facilitating the
installation of the extension in Frama-C. This tool is also a gateway to other possible
features, which we discuss herein.

It is currently under development, without a fixed version number.

### How do I get set up? ###

First you need to have Frama-C Magnesium source-code (http://frama-c.com/download.html).
Once downloaded and extracted the repository you have to do the following steps:

1. Rename the repo (**frama-c-wpo-rewriter**) into **wp**
2. Replace the original Frama-C _wp_ folder with the folder from previous step
3. Reinstall Frama-C (**./configure** must be executed)

And everything is ready to go!

### Who do I talk to? ###

Vítor Almeida
vit.alcantara@gmail.com