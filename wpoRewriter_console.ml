open WpoRewriter_infRules_names
open InfRules
open Rewriter_utils
open Formula_Parser

let prover_of_name = function
  | "" | "none" -> None
  | "qed" | "Qed" -> Some VCS.Qed
  | "alt-ergo" | "altgr-ergo" -> Some VCS.AltErgo
  | "coq" | "coqide" -> Some VCS.Coq
  | "why3ide" -> Some VCS.Why3ide
  | s -> 
      match Extlib.string_del_prefix "why3:" s with
      | Some "" -> None
      | Some "ide" -> Some VCS.Why3ide
      | Some s' -> Some (VCS.Why3 s')
      | None -> Some (VCS.Why3 s)

let name_of_prover = function
  | VCS.Why3ide -> "why3"
  | VCS.Why3 s -> s
  | VCS.AltErgo -> "alt-ergo"
  | VCS.Coq -> "coq"
  | VCS.Qed -> "qed"
  | VCS.Rewriter -> "rewriter"

type command = InfRule of Wpo.t * string  | Failed of string |
	       Undo of string | Reset of string | Prover of (VCS.prover*VCS.mode) |
	       ProveUnproven of VCS.prover | ResetAll | Save | Load | Mk_Lemma
	       | ProveWithAll of VCS.prover list
	       | Success of string

(*some command labels*)
let reset_command = "reset"
let undo_command = "undo"
let prove_command = "call"
let prove_with_all_provers_command = "callp"
let edit_command = "edit"
let reset_all_command = "reset_all"
(*let save_command = "save"
let load_command = "load"*)
let prove_unproven_command = "call_all_wpos"
let set_timeout_cmd = "set_timeout"
let set_processes_cmd = "set_processes"
let set_depth_cmd = "set_depth"
let set_steps_cmd = "set_steps"

let commands_history = ref []
let history_index = ref ~-1

let temp_command = ref ""

let set_temp_command s = if !history_index = -1 then temp_command := s

let add_command_in_history command_string =
  if not (List.exists (fun s -> command_string = s) !commands_history) then (
    commands_history := command_string::!commands_history;
    history_index := -1;
    temp_command := ""
  )

let previous_history () : string = 
  if !commands_history = [] then (
    history_index := -1;
    !temp_command
  )
  else (
    incr history_index;
    let length = List.length !commands_history in
    if !history_index >= length  then (
      history_index := length - 1;
    );
    List.nth !commands_history !history_index
  )

let next_history () : string =
  decr history_index;
  if !history_index < 0 then (
    history_index := -1;
    !temp_command
  )
  else 
    List.nth !commands_history !history_index

(*All command names must be inserted in this list too*)
let get_command_names () =
  let list = [reset_command;undo_command;prove_command;
	      reset_all_command;(*save_command;*)edit_command;(*load_command;*)
	      prove_unproven_command;prove_with_all_provers_command;
	      set_timeout_cmd;set_processes_cmd;set_depth_cmd;set_steps_cmd]
  in list

let get_prover_names ():string list =
   let prover_names =  ref [] in
   let get_detected_provers names (dp:ProverWhy3.dp list option) = 
     match dp with
     | None -> ()
     |Some p -> List.iter(fun (d:ProverWhy3.dp) -> 
			  names := !names @ ["why3:" ^ d.ProverWhy3.dp_name]) p;
   in
   let t = ProverWhy3.detect_why3_task (get_detected_provers prover_names) in
   Task.run t;
   ignore (Task.wait t);
   prover_names := !prover_names @ 
   [name_of_prover VCS.Coq;
   name_of_prover VCS.Why3ide;
   name_of_prover VCS.Qed] ;
  let alt_ergo = name_of_prover VCS.AltErgo in
  if not (List.exists(fun n -> n = alt_ergo)!prover_names) then
    prover_names := alt_ergo :: !prover_names;
  !prover_names

 let parse_term wpo args =
  let config = mk_config wpo in
  let exp = mk_term config args in
  exp
	   
let execute_inf_rule_proof_by_cases args wpo =
  let case_predicate = parse_term wpo args in
  let new_wpo = execute_inf_rule wpo.Wpo.po_gid (Proof_by_cases case_predicate) in
  new_wpo

let rec build_int_list i n =
  if i = n then []
  else i::build_int_list (i+1) n
			 
let rec setminus l1 l2 =
  match l1 with
  |h::t ->
    if (List.exists(fun a -> a=h) l2) then
      setminus t l2
    else h::setminus t l2
  |[] -> []

let length_of_hyps wpo =
  match wpo.Wpo.po_formula with
  |Wpo.GoalAnnot annot ->      
    let (hyps,_) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
    List.length hyps
  |Wpo.GoalLemma lemma -> List.length (get_assumptions lemma)
  | _ -> raise (Failure "This type of PO does not have hypotheses")
	       
let rec execute_inf_rule_remove_hypotheses arguments wpo =
  let indexes = List.map int_of_string arguments in
  let count = length_of_hyps wpo in
  let ol = build_int_list 0 count in
  let reslist = setminus ol indexes in
  let tactic = Subset_hyps (reslist) in 
  let new_wpo = execute_inf_rule wpo.Wpo.po_gid tactic in
  new_wpo

let rec execute_inf_rule_make_wpo_with_subset_of_hypotheses arguments wpo =
  let indexes = List.map int_of_string arguments in
  let new_wpo = execute_inf_rule wpo.Wpo.po_gid (Subset_hyps (indexes)) in
  new_wpo

let execute_inf_rule_split_goal_conjunction wpo =
  let new_wpo = execute_inf_rule wpo.Wpo.po_gid (Split_and_goal) in
  new_wpo

let execute_inf_rule_remove_term_disj_goal arguments wpo =
  match arguments with
  |[] -> raise (Failure "No index is provided to remove term")
  |argument::_ ->
    let index = int_of_string argument in
    execute_inf_rule wpo.Wpo.po_gid (Remove_term_disj_goal (index))

let execute_inf_rule_split_term_disj_hypothesis arguments wpo =
    let hyp_index = List.hd (List.map int_of_string arguments) in
    execute_inf_rule wpo.Wpo.po_gid (Split_term_disj_hyp (hyp_index))

let execute_inf_rule_modus_ponens_hyps arguments wpo = 
    let index = List.hd (List.map int_of_string arguments) in
    let tactic = Modus_ponens_hyps index in
    execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_split_hypothesis_conjunction arguments wpo =
    let index = List.hd (List.map int_of_string arguments) in
    let tactic = Split_and_hyp (index) in
    execute_inf_rule wpo.Wpo.po_gid tactic    

let execute_inf_rule_contrapose_hypothesis (arguments:string list) wpo =
    let index = List.hd (List.map int_of_string arguments) in
    let tactic = (Contrapose (index)) in
    execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_contrapose_goal wpo =
  let tactic = Contradict in
    execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_rewrite_branch arguments wpo =
    let index = List.hd (List.map int_of_string arguments) in
    let tactic = Rewrite_branch (index) in
    execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_rewrite_if_then_else_goal wpo =
  let tactic = Rewrite_if_then_else_goal in
  execute_inf_rule wpo.Wpo.po_gid tactic

exception Finished

let read_replace_arg text =
  let text = String.trim text in
  let level = ref 0 in
  let res = ref "" in
  try
    String.iter (fun c -> let s = Char.escaped c in
			  res := !res ^ s;
			  if c = ')' then 
			    if !level = 1 then raise Finished else decr level
			  else if c = '(' then incr level 
			  else ()
		) text;
    !res
  with Finished -> !res
     

let rec read_WITH_term text =
  let text = String.trim text in
  if String.sub text 0 4 = "with" then
    String.sub text 4 ((String.length text )-4)
  else raise (Failure "WITH keyword was not found")   

let read_replace_args text =
  try
    let st1 = read_replace_arg text in
    let text = String.sub text (String.length st1) ((String.length text) - String.length st1)
    in
    let text = read_WITH_term text in
    let st2 = read_replace_arg text in
    (st1,st2)
with
Invalid_argument _ -> raise (Failure "Invalid_Argument when parsing Replace tactic")

let execute_inf_rule_replace args wpo =
  let arg = String.concat " " args in
  let (t1s,t2s) = read_replace_args arg in
    let config = mk_config wpo in 
    let term1 = mk_term config [t1s] in
    let term2 = mk_term config [t2s] in
    let tactic = Replace (term1,term2) in
    execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_rewrite_hyp args wpo:Wpo.t =
  match args with
  |label::others -> (
    let index = int_of_string label in
    match others with
    |[dir] ->
      let direction = direction_of_string dir in
      execute_inf_rule wpo.Wpo.po_gid (Rewrite_Hyp (index,direction,None))
    |dir::subtermstrings ->
      let direction = direction_of_string dir in
      let term = parse_term wpo subtermstrings in
      execute_inf_rule wpo.Wpo.po_gid (Rewrite_Hyp (index,direction,Some term))
    |[] -> execute_inf_rule wpo.Wpo.po_gid (Rewrite_Hyp (index,Right,None))
  )
  | _ -> raise (Failure "Wrong arguments for tactic rewrite_hyp")

let execute_inf_rule_rewrite_lemma args wpo =
  match args with
  |lem_name::others -> (
    match others with
    |[dir] -> 
      let direction = direction_of_string dir in
      execute_inf_rule wpo.Wpo.po_gid (Rewrite_Lemma (lem_name,direction,None))
    |dir::subtermstrings ->
      let direction = direction_of_string dir in
      let term = parse_term wpo subtermstrings in
      execute_inf_rule wpo.Wpo.po_gid (Rewrite_Lemma (lem_name,direction,Some term))      
    |[] -> execute_inf_rule wpo.Wpo.po_gid (Rewrite_Lemma (lem_name,Right, None))
  )
  | _ -> raise (Failure "Wrong arguments for tactic rewrite_lemma")

let execute_inf_rule_nat_ind args wpo =
  match args with
  |[] -> raise (Failure "An integer variable must be defined for nat induction")
  |[name] ->
    let v = find_var wpo ~name in
    execute_inf_rule wpo.Wpo.po_gid (Nat_ind (v,None))
  |name::term_strings ->
    let v = find_var wpo ~name in
    let t1 = Some (parse_term wpo term_strings) in
    execute_inf_rule wpo.Wpo.po_gid (Nat_ind (v,t1))
		   
let execute_inf_rule_int_ind args wpo =
  match args with
  |name1::termStrings -> (
    let v1 = find_var wpo ~name:name1 in
    let t1 = parse_term wpo termStrings in
    execute_inf_rule wpo.Wpo.po_gid (Int_ind (v1,t1))
  )
  |_ -> raise (Failure "Incorrect parameters for integer induction")
		
let execute_inf_rule_unfold args wpo =
  match args with
  | [] -> raise (Failure "No function name is provided for unfold")
  | label::_others -> (
    try 
      let f = Formula_Parser.find_lfun wpo.Wpo.po_model label in 
    let tactic = Unfold f in
      execute_inf_rule wpo.Wpo.po_gid tactic
    with Not_found -> raise (Failure ("Function " ^ label ^ " is not defined"))
  )

let execute_inf_rule_unfold_all args wpo =
  match args with
  | [] -> raise (Failure "No function name is provided for unfold")
  | label::_others -> (
    try 
      let f = Formula_Parser.find_lfun wpo.Wpo.po_model label in
    let tactic = UnfoldAll f in
      execute_inf_rule wpo.Wpo.po_gid tactic
    with Not_found -> raise (Failure ("Function " ^ label ^ " is not defined"))
  )

let execute_inf_rule_assert_ args wpo =
  let config = mk_config wpo in
  let term = mk_term config args in
  let tactic = Assert term in
  execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_use_equality_in_hypothesis args wpo =
 match args with
  |sterm1::sterm2::scond::_other_args ->
    let et = execute_inf_rule ~po_gid:wpo.Wpo.po_gid in
    let config = mk_config wpo in
    let term1 = mk_term config [sterm1] in
    let term2 = mk_term config [sterm2] in
    if scond = "G" then
      let tactic = Use_equality_in_hypothesis (term1,term2,Goal) in
      et tactic
    else if scond = "AH" then
      let tactic = Use_equality_in_hypothesis (term1,term2,AllHyp) in
      et tactic
    else 
	let index = List.hd (List.map int_of_string args) in
	let tactic = Use_equality_in_hypothesis (term1,term2,Hyp index) in
	et tactic
  |_ -> raise (Failure "Not valid arguments to use equality in hypothesis")

let execute_inf_rule_modus_ponens arguments wpo =
  match arguments with
  |[] -> raise (Failure "No hypothesis was selected")
  | label::_other_args -> 
     let index = int_of_string label in
     let tactic = Modus_ponens_hyps index in
     execute_inf_rule wpo.Wpo.po_gid tactic
     

let execute_inf_rule_instantiate_universal_quantifier_hyps arguments wpo =
  match arguments with
  | [] -> raise (Failure "No hypothesis was selected")
  | label::exps ->
     let exp = parse_term wpo exps in
     let index = int_of_string label in
     let tactic = Instantiate_universal_quantifier_hyps (index,exp) in
     execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_remove_existential_quantifier_goal args wpo =
    let exp = parse_term wpo args in
    let tactic = Instantiate_existential_quantifier_goal (exp) in
     execute_inf_rule wpo.Wpo.po_gid tactic

let execute_inf_rule_intro wpo =
  let tactic = Intro in
  execute_inf_rule wpo.Wpo.po_gid tactic
		 
let execute_inf_rule_to_goal arguments wpo =
     let index = List.hd (List.map int_of_string arguments) in
     let tactic = To_Goal index in
     execute_inf_rule wpo.Wpo.po_gid tactic

let rec execute_inf_rule_intros  wpo =
  try
    let new_wpo = execute_inf_rule_intro wpo in
    execute_inf_rule_intros new_wpo
  with Failure _ -> wpo

let execute_inf_rule_apply_hypothesis_in_goal arguments wpo =
    let index = List.hd (List.map int_of_string arguments) in
    let tactic = Apply_hypothesis_in_goal (index) in
    execute_inf_rule wpo.Wpo.po_gid tactic
		   
let execute_inf_rule_command w command arguments =
  if w.Wpo.po_leaves <> [] 
  then Failed "cannot apply tactic in non-child proof obligation"
  else let ts = command ^ " successfully applied." in
       let open WpoRewriter_infRules_names in
       if command = subset_hyps then
	 let w = execute_inf_rule_remove_hypotheses arguments w in InfRule (w,ts)
       else if command = modus_ponens_hyps then
	 let w = execute_inf_rule_modus_ponens_hyps arguments w in InfRule (w,ts)
       else if command = split_and_goal then
	 let w = execute_inf_rule_split_goal_conjunction w in InfRule (w,ts)
       else if command = remove_term_disj_goal then
	 let w = execute_inf_rule_remove_term_disj_goal arguments w in InfRule (w,ts)
       else if command = split_term_disj_hyp then
	 let w = execute_inf_rule_split_term_disj_hypothesis arguments w in InfRule (w,ts)
       else if command = split_and_hyp then
	 let w = execute_inf_rule_split_hypothesis_conjunction arguments w in InfRule (w,ts)
       else if command = contrapose then
	 let w = execute_inf_rule_contrapose_hypothesis arguments w in InfRule (w,ts)
       else if command = contradict then
	 let w = execute_inf_rule_contrapose_goal w in InfRule (w,ts)
       else if command = rewrite_branch then
	 let w = execute_inf_rule_rewrite_branch arguments w in InfRule (w,ts)
       else if command = rewrite_if_then_else_goal then
	 let w = execute_inf_rule_rewrite_if_then_else_goal w in InfRule (w,ts)
       else if command = instantiate_existential_quantifier_goal then 
	 let w = execute_inf_rule_remove_existential_quantifier_goal arguments w in InfRule (w,ts)
       else if command = proof_by_cases then
	 let w = execute_inf_rule_proof_by_cases arguments w in InfRule (w,ts)
       else if command = intro then
	 let w = execute_inf_rule_intro w in InfRule (w,ts)
       else if command = apply_hypothesis_in_goal then
	 let w = execute_inf_rule_apply_hypothesis_in_goal arguments w in InfRule (w,ts)
       else if command = instantiate_universal_quantifier_hyps then
	 let w = execute_inf_rule_instantiate_universal_quantifier_hyps arguments w in InfRule (w,ts)
       else if command = modus_ponens_hyps then
	 let w = execute_inf_rule_modus_ponens arguments w in InfRule (w,ts)
       else if command = use_equality_in_hypothesis then
	 let w = execute_inf_rule_use_equality_in_hypothesis arguments w in InfRule (w,ts)
       else if command = assert_ then
	 let w = execute_inf_rule_assert_ arguments w in InfRule (w,ts) 
       else if command = unfold then
	 let w = execute_inf_rule_unfold arguments w in InfRule (w,ts)
       else if command = nat_ind then
	 let w = execute_inf_rule_nat_ind arguments w in InfRule (w,ts)
       else if command = int_ind then
	 let w = execute_inf_rule_int_ind arguments w in InfRule (w,ts)
       else if command = rewrite_lemma then
	 let w = execute_inf_rule_rewrite_lemma arguments w in InfRule (w,ts)
       else if command = rewrite_hyp then
	 let w = execute_inf_rule_rewrite_hyp arguments w in InfRule (w,ts)
       else if command = replace then
	 let w = execute_inf_rule_replace arguments w in InfRule (w,ts)
       else if command = intros then
	 let w = execute_inf_rule_intros w in InfRule (w,ts)
       else if command = to_goal then
	 let w = execute_inf_rule_to_goal arguments w in InfRule (w,ts)
       else if command = unfold_all then
	 let w = execute_inf_rule_unfold_all arguments w in InfRule (w,ts)
       else (
	 Failed ("Syntax error: non-existent command:" ^ command)
       )

let set_timeout args =
  match args with
  | [i] -> Wp_parameters.Timeout.set (int_of_string i);
	   Success ("Prover timeout set: " ^ i)
  | _ -> Failed ("Syntax error: wrong arguments for timeout")

let set_processes args =
  match args with
  | [i] -> Wp_parameters.Procs.set (int_of_string i);
	   Success ("Number of processes set: " ^ i)
  | _ -> Failed ("Syntax error: wrong arguments for processes")

let set_depth args =
  match args with
  | [i] -> Wp_parameters.Depth.set (int_of_string i);
	   Success ("Depth set: " ^ i)
  | _ -> Failed ("Syntax error: wrong arguments for depth")

let set_steps args =
  match args with
  | [i] -> Wp_parameters.Timeout.set (int_of_string i);
	   Success ("Number of steps set: " ^ i)
  | _ -> Failed ("Syntax error: wrong arguments for setting steps")

let execute_command (wpo:Wpo.t option) command_string:command =
  add_command_in_history command_string;
  let words = Str.split (Str.regexp "[ \t]+") command_string in
  try(
  match words with
  | [] -> Failed "Command is empty"
  | command::(arguments:string list) ->
     if command = set_timeout_cmd then
       set_timeout arguments
     else if command = set_processes_cmd then
       set_processes arguments
     else if command = set_depth_cmd then
       set_depth arguments
     else if command = set_steps_cmd then
       set_steps arguments
     (*else if command = load_command then Load
     else if command = save_command then Save*)
     else if command = reset_all_command then ResetAll
     else if command = prove_command then(
       match wpo with
       | Some w -> 
	  if w.Wpo.po_leaves = [] then (
	    match arguments with
	    |[] -> Prover (VCS.AltErgo,VCS.BatchMode)
	    |p::[] -> let prover = VCS.prover_of_name p in
		      (match prover with
		       | Some pr -> Prover (pr,VCS.BatchMode)
		       | None -> Failed "There is no prover with this name"
		      )
	    |_ -> Failed "Wrong arguments for proving command"
	  )
	  else raise (Failure "Cannot call prover in PO that is not a leaf")
       |None -> Failed "No wpo was selected for prover"
     )
     else if command = prove_with_all_provers_command then(
       match wpo with
       | Some w -> if w.Wpo.po_leaves = [] then 
		     let provers = ref [] in
		     List.iter(fun p -> match prover_of_name p with
					| None -> ()
					| Some VCS.Rewriter -> ()
					| Some pr -> provers := pr::!provers
			      ) (get_prover_names ());
		     ProveWithAll !provers
		   else raise (Failure "Cannot call prover in PO that is not a leaf")
       |None -> Failed "No wpo was selected for prover"
     )
     else if command = prove_unproven_command then (
       match wpo with
       | Some w -> 
	  if w.Wpo.po_leaves = [] then (
	    match arguments with
	    |[] -> ProveUnproven VCS.AltErgo
	    |p::[] -> let prover = VCS.prover_of_name p in
		      (match prover with
		       | Some pr -> ProveUnproven pr
		       | None -> Failed "There is no prover with this name"
		      )
	    |_ -> Failed "Wrong arguments for proving command"
	  )
	  else raise (Failure "Cannot call prover in PO that is not a leaf")
       |None -> Failed "No wpo was selected for prover"
     )
     else if command = edit_command then(
       match wpo with
       | Some w -> 
	  if w.Wpo.po_leaves = [] then (
	    match arguments with
	    |p::[] -> let prover = VCS.prover_of_name p in
		      (match prover with
		       | Some pr -> Prover (pr,VCS.EditMode)
		       | None -> Failed "There is no prover with this name"
		      )
	    |_ -> Failed "Wrong arguments for proving command"
	  )
	  else raise (Failure "Cannot call prover in PO that is not a leaf")
       |None -> Failed "No wpo was selected for prover"
     )					   
     else if command = reset_command then(
       Reset "Reset commmand successful";
     )
     else if command = undo_command then(
       Undo "Undo command successful";
     )					  
     else (
       match wpo with
       | Some w -> execute_inf_rule_command w command arguments
       | None -> Failed ("No wpo was selected for " ^ command)
     )
  )
  with
  |Failure message -> Failed message
				 
