type tactic_name = string

let subset_hyps = "weakening"
let split_and_goal = "split"
let split_and_hyp = "splith"
let split_term_disj_hyp = "splitdh"
let remove_term_disj_goal = "splitdo"     
let contrapose = "contrapose"
let contradict = "contradict"
let rewrite_branch = "restructure"
let rewrite_if_then_else_goal = "rewrite_if_then_else_goal"
let instantiate_universal_quantifier_hyps = "allh"
let instantiate_existential_quantifier_goal = "existo"
let remove_existential_quantifier_hyps = "existh"
let proof_by_cases = "case"
let intro = "intro"
let apply_hypothesis_in_goal = "apply"
let modus_ponens_hyps = "mp"
let use_equality_in_hypothesis = "equ_h"
let assert_ = "assert"
let unfold = "unfold"
let nat_ind = "nat_ind"
let int_ind = "int_ind"
let rewrite_lemma = "rewritel"
let rewrite_hyp = "rewriteh"
let replace = "replace"
let intros = "intros"
let to_goal = "to_goal"
let unfold_all = "unfold_all"
		   
type cond = Goal | AllHyp | Hyp of int
type direction = Left | Right
type incr_or_decr = Increase | Decrease

let string_of_incr_of_decr d =
  match d with
  | Increase -> "+"
  | Decrease -> "-"

let incr_or_decr_of_string s =
  if s = "+" then Increase
  else if s = "-" then Decrease
  else raise (Failure "String is not a signal to increase or to decrease")
		  
let string_of_direction d =
  match d with
  | Left -> "<-"
  | Right -> "->"

let direction_of_string s =
  if s = "<-" then Left
  else if s = "->" then Right
  else raise (Failure (s ^ " is not a direction"))

type tactic_s =
| To_Goal of int
| Replace of (Lang.F.term * Lang.F.term)
| Rewrite_Lemma of (string * direction * Lang.F.term option)
| Rewrite_Hyp of (int * direction * Lang.F.term option)
| Nat_ind of (Lang.F.var * Lang.F.term option)
| Int_ind of (Lang.F.var * Lang.F.term)
| Use_equality_in_hypothesis of Lang.F.term * Lang.F.term * cond
| Modus_ponens_hyps of int
| Instantiate_universal_quantifier_hyps of int * Lang.F.term
| Instantiate_existential_quantifier_goal of Lang.F.term
| Rewrite_if_then_else_goal  
| Rewrite_branch of int
| Subset_hyps of int list
| Split_and_goal
| Split_term_disj_hyp of int
| Remove_term_disj_goal of int
| Split_and_hyp of int
| Contrapose of int
| Contradict
| Proof_by_cases of Lang.F.term
| Apply_hypothesis_in_goal of int
| Intro
| Assert of Lang.F.term
| Unfold of Lang.F.Fun.t
| UnfoldAll of Lang.F.Fun.t

type tactic_inst = {
  id : string;
  tactic : tactic_s
}

let get_tactic_names () =
[use_equality_in_hypothesis;
modus_ponens_hyps;
subset_hyps;
split_and_goal;
split_and_hyp;
split_term_disj_hyp;
remove_term_disj_goal;
contrapose;
contradict;
rewrite_branch;
rewrite_if_then_else_goal;
instantiate_universal_quantifier_hyps;
instantiate_existential_quantifier_goal;
proof_by_cases;
intro;
apply_hypothesis_in_goal;
assert_;unfold;nat_ind;int_ind;rewrite_lemma;
replace;intros;to_goal;unfold_all;rewrite_hyp]

let name_of_tactic (tactic:tactic_s) =
  match tactic with
  |Rewrite_Hyp _ -> rewrite_hyp
  |UnfoldAll _ -> unfold_all
  |To_Goal _ -> to_goal
  |Replace _ -> replace
  |Rewrite_Lemma _ -> rewrite_lemma
  |Nat_ind _ -> nat_ind
  |Int_ind _ -> int_ind
  |Unfold _ -> unfold
  |Assert _ -> assert_
  |Use_equality_in_hypothesis (_,_,_) -> use_equality_in_hypothesis
  |Modus_ponens_hyps _ -> modus_ponens_hyps
  |Instantiate_universal_quantifier_hyps (_,_) -> 
    instantiate_universal_quantifier_hyps
  |Instantiate_existential_quantifier_goal _ -> 
    instantiate_existential_quantifier_goal 
  |Rewrite_if_then_else_goal -> rewrite_if_then_else_goal  
  |Rewrite_branch _ -> rewrite_branch
  |Subset_hyps _ -> subset_hyps
  |Split_and_goal -> split_and_goal
  | Split_term_disj_hyp _ -> split_term_disj_hyp
  | Remove_term_disj_goal _ -> remove_term_disj_goal
  | Split_and_hyp _ -> split_and_hyp
  | Contrapose _ -> contrapose
  | Contradict -> contradict
  | Proof_by_cases _ -> proof_by_cases
  | Apply_hypothesis_in_goal _ -> apply_hypothesis_in_goal
  | Intro -> intro
