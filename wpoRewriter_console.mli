open WpoRewriter_infRules_names

type command = InfRule of Wpo.t * string  | Failed of string |
	       Undo of string | Reset of string | Prover of (VCS.prover*VCS.mode) |
	       ProveUnproven of VCS.prover | ResetAll | Save | Load | Mk_Lemma
	       | ProveWithAll of VCS.prover list
	       | Success of string

(* Second argument is the commnd string. *)
val execute_command   : Wpo.t option -> string -> command
val get_command_names : unit -> string list
val get_prover_names  : unit -> string list
val previous_history  : unit -> string
val next_history : unit -> string
val prove_command : string
val edit_command : string
val set_temp_command : string -> unit
