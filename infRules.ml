open Conditions
open Definitions
open Wpo
open Qed.Logic
open Lang.F
open WpoRewriter_infRules_names
open Rewriter_utils

(**Auxiliary functions *)

let po_not_found_message = "PO not found"

let get_quantifier_tau term =
  match repr term with
  | Bind (q, v, _) ->
     (match q with Forall -> v  | _ -> raise (Failure "Not universal quantifier"))
  | _ -> raise (Failure "Is not a quantifier")

let mk_var vars ?basename tau =
  let pool = Lang.new_pool () in
  add_vars pool vars;
  fresh pool ?basename tau

let failed_msg ~po_gid ~tactic =
  "Tactic:" ^ tactic ^ " cannot be used in PO:" ^ po_gid

let tau_of_sort = function
  | Sint -> Int
  | Sreal -> Real
  | Sbool -> Bool
  | Sprop | Sdata | Sarray _ -> raise Not_found

type part_answer = Yes of (int*int) | No

(*----------------------------*)
(*    Miscellaneous functions *)
(*----------------------------*)

let rec get_leaves wpo =
  if List.length wpo.Wpo.po_leaves = 0 then [wpo]
  else
    let res = ref [] in
    for i = 0 to List.length wpo.Wpo.po_leaves -1 do
      res := !res @ ( get_leaves (List.nth wpo.Wpo.po_leaves i))
    done;
    !res

let rec remove_leaves_from_WpoSet wpo =
  let leaves = wpo.Wpo.po_leaves in
  let remove_leaf leaf =
    Wpo.remove leaf;
    remove_leaves_from_WpoSet leaf
  in
  List.iter remove_leaf leaves

let cut_wpo_children wpo:Wpo.t =
  remove_leaves_from_WpoSet wpo;
  let length = List.length wpo.Wpo.po_leaves in
  if length > 1 then (
    let max:int ref = ref ~-1 in
    List.iter (
	fun w ->
	let wpo_part = WpPropId.parts_of_id w.Wpo.po_pid in
	match wpo_part with
	|None -> ()
	| Some (pt,_tt) -> if pt > !max then max := pt
      ) wpo.Wpo.po_leaves;
    let diff = ~-(length - 1) in
    let wpo_part = WpPropId.parts_of_id wpo.Wpo.po_pid in (
      match wpo_part with
      | Some _ -> update_parts wpo !max diff
      | None -> ()
    );
  );
  wpo.Wpo.po_leaves <- [];
  Wpo.add wpo;
  wpo

let ind = ref 0

let get_conjunction_terms t =
  match Lang.F.repr t with
  | Qed.Logic.And ts -> ts
  | _ -> []

let replace_pred_in_condition (condition: Conditions.condition) pred =
  match condition with
  | Core _ -> Conditions.Core pred
  | Init _ -> Conditions.Init pred
  | Type _ -> Conditions.Type pred
  | Have _ -> Conditions.Have pred
  | When _ -> Conditions.When pred
  | Branch _ -> Have pred
  | Either (_seq_list) -> Have pred

let make_hypothesis_from_another_hypothesis (step: Conditions.step) pred = {
    (step) with condition = (replace_pred_in_condition step.Conditions.condition pred)
  }

let make_steps step new_preds =
  List.map (fun pred ->
	    make_hypothesis_from_another_hypothesis step pred
	   ) new_preds

let replace_elt list ~old ~new_elts ~comparison =
  let found = ref false in
  let (left,right) =
    List.partition (fun e -> if (comparison e old) then found := true;!found) list in
  let right = match right with
    |[] -> right
    |_a::b -> b
  in left @ new_elts @ right


let rec sublist b e l =
  if e < b then [] else
    match l with
      [] -> failwith "sublist"
    | h :: t ->
       let tail = if e=0 then [] else sublist (b-1) (e-1) t in
       if b>0 then tail else h :: tail

let rec subst_steps hyps index new_preds =
  let left = sublist 0 (index-1) hyps in
  let right = sublist (index+1) ((List.length hyps) - 1) hyps in
  left @ new_preds @ right

let subst_terms list (term:Lang.F.term) (new_terms:Lang.F.term list) =
  replace_elt list ~old:term ~new_elts:new_terms ~comparison:(fun a b -> a = b)

let replace_wpo_in_WpoSet old_wpo new_wpo_list =
  old_wpo.Wpo.po_leaves <- old_wpo.Wpo.po_leaves @ new_wpo_list;
  Wpo.remove old_wpo;
  List.iter (Wpo.add) new_wpo_list

let rec get_wpo_by_po_gid_in_list po_gid wpo_list =
  match wpo_list with
  | [] -> None
  | hd:: tl -> let result = get_wpo_by_po_gid_in_wpo po_gid hd in
	       match result with
	       | None -> get_wpo_by_po_gid_in_list po_gid tl
	       | Some wpo -> Some wpo

and get_wpo_by_po_gid_in_wpo po_gid wpo =
  if wpo.Wpo.po_gid = po_gid then Some wpo else
    let leaves = wpo.Wpo.po_leaves in
    get_wpo_by_po_gid_in_list po_gid leaves

let rec get_wpo_by_po_pid_in_list po_pid wpo_list =
  match wpo_list with
  | [] -> None
  | hd:: tl -> let result = get_wpo_by_po_pid_in_wpo po_pid hd in
	       match result with
	       | None -> get_wpo_by_po_pid_in_list po_pid tl
	       | Some wpo -> Some wpo

and get_wpo_by_po_pid_in_wpo po_pid wpo =
  let pretty_po_pid = (
    WpPropId.pretty Format.str_formatter po_pid;
    Format.flush_str_formatter ()
  ) in
  let pretty_wpo_po_pid = (
    WpPropId.pretty Format.str_formatter wpo.Wpo.po_pid;
    Format.flush_str_formatter ()
  ) in
  if pretty_po_pid = pretty_wpo_po_pid then Some wpo
  else
    let leaves = wpo.Wpo.po_leaves in
    get_wpo_by_po_pid_in_list po_pid leaves

let get_wpo_by_po_gid po_gid =
  let selected_wpo = ref None in
  let get_wpo wpo =
    (
      let root = get_root wpo in
      let result = get_wpo_by_po_gid_in_wpo po_gid root in
      match result with
      | None -> ()
      | Some _ -> selected_wpo := result
    )
  in
  Wpo.iter_on_goals get_wpo;
  match !selected_wpo with
  | None -> raise (Failure ("PO: " ^ po_gid ^ " was not found"))
  | Some w -> w

let pretty po_pid =
  WpPropId.pretty Format.str_formatter po_pid;
  Format.flush_str_formatter ()


let get_wpo_by_po_pid po_pid =
  let selected_wpo = ref None in
  let get_wpo wpo =
    (
      let root = get_root wpo in
      let result = get_wpo_by_po_pid_in_wpo po_pid root in
      match result with
      | None -> ()
      | Some _ -> selected_wpo := result
    )
  in
  Wpo.iter_on_goals get_wpo;
  match !selected_wpo with
  | None -> raise (Failure ("PO: " ^ (pretty po_pid) ^ " was not found"))
  | Some w -> w


type lem_skel =
  {
    ls_parent : Wpo.t;
    ls_lemma : Wpo.VC_Lemma.t;
    ls_hypotheses : (Lang.F.pred list) option;
    ls_goal : Lang.F.pred option;
    ls_tactic : tactic_s;
  }

type annot_skel =
  {
    as_parent : Wpo.t;
    as_annot : Wpo.VC_Annot.t;
    as_hypotheses : hypotheses option;
    as_goal : Lang.F.pred option;
    as_tactic : tactic_s;
  }

module Build_Wpo :  sig

    val mk_lemma_children : Wpo.t -> lem_skel list -> Wpo.t list
    val mk_lemma_child : lem_skel -> Wpo.t
    val mk_annot_child : annot_skel -> Wpo.t
    val mk_annot_children : Wpo.t -> annot_skel list -> Wpo.t list

  end = struct

    let update_parts current_wpo elements =
      let new_length = List.length elements in
      match WpPropId.parts_of_id current_wpo.Wpo.po_pid with
      | None -> (0,new_length)
      | Some (index,total) -> update_parts current_wpo index (new_length-1);
			      (index,total+(new_length-1))

    let rec get_biggest_index w =
      let max = ref 0 in
      (match w.Wpo.po_partac with
       | None -> ()
       | Some (i,_,_) ->  if i > !max then max := i
      );
      List.iter(fun p -> let qmax = get_biggest_index p in
			 if  qmax > !max then max := qmax) w.Wpo.po_leaves;
      !max

    let make_annot_child ~annot ~(parent:Wpo.t) ?hypotheses:(hypotheses=None)
			 ?goal:(goal=None) ?mk_part:(mk_part=No)
			 ~(tactic:tactic_s) :Wpo.t =
      let (h,g) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
      let n_h = match hypotheses with
	|None -> h
	|Some hs -> hs
      in
      let (n_g) = match goal with
	|None -> g
	|Some gs -> gs
      in
      let old_po_pid:WpPropId.prop_id = (parent.Wpo.po_pid) in
      let new_po_pid = (
	match mk_part with
	|No -> old_po_pid
	|Yes part -> WpPropId.mk_part old_po_pid part
      ) in
      let root = get_root parent in
      let new_index = (1 + get_biggest_index root) in
      let new_wpo =
	{
	  parent with Wpo.po_formula =
			Wpo.GoalAnnot
			  {
			    (annot) with
			    Wpo.VC_Annot.goal = Wpo.GOAL.make(n_h, n_g);
			  };
		      Wpo.po_gid = root.Wpo.po_gid ^ "_" ^ (string_of_int new_index);
		      Wpo.po_partac = Some (new_index,parent,tactic);
		      Wpo.po_leaves = [];
		      Wpo.po_pid = new_po_pid
	} in
      replace_wpo_in_WpoSet parent [new_wpo];
      new_wpo

    let mk_annot_child l =
      make_annot_child ~parent:l.as_parent ~annot:l.as_annot ~hypotheses:l.as_hypotheses
		       ~goal:l.as_goal ~mk_part:No ~tactic:l.as_tactic

    let mk_annot_children current_wpo (annots:annot_skel list):Wpo.t list =
      if List.length annots <= 1 then
	List.map (fun l -> make_annot_child ~parent:l.as_parent ~annot:l.as_annot
					    ~hypotheses:l.as_hypotheses
					    ~goal:l.as_goal ~mk_part:No
					    ~tactic:l.as_tactic
		 ) annots
      else (
	let (base,total) = update_parts current_wpo annots in
	List.mapi (fun i l -> make_annot_child ~parent:l.as_parent ~annot:l.as_annot
					       ~hypotheses:l.as_hypotheses
					       ~goal:l.as_goal ~mk_part:(Yes (base+i,total))
					       ~tactic:l.as_tactic
		  ) annots
      )

    let is_digit c = Char.code c >= 48 && Char.code c <= 57

    let make_lemma_child ~lemma ~(parent:Wpo.t) ?hypotheses:(hypotheses=None)
			 ?goal:(goal=None) ?mk_part:(mk_part=No)
			 ~(tactic:tactic_s)  :Wpo.t =
      let h = lemma.Wpo.VC_Lemma.lemma.Definitions.l_assumptions in
      let n_h =
	match hypotheses with
	|None -> h
	|Some hs -> hs
      in
      let g = lemma.VC_Lemma.lemma.Definitions.l_lemma in
      let (n_g) = match goal with
	|None -> g
	|Some gs -> gs
      in
      let old_po_pid:WpPropId.prop_id = (parent.Wpo.po_pid) in
      let new_po_pid = (
	match mk_part with
	|No -> old_po_pid
	|Yes part -> WpPropId.mk_part old_po_pid part
      ) in
      let root = get_root parent in
      let new_index = (1 + get_biggest_index root) in
      let new_wpo =
	{
	  (parent) with Wpo.po_formula =
			  Wpo.GoalLemma
			    {
			      (lemma) with
			      Wpo.VC_Lemma.lemma =
				{
				  lemma.Wpo.VC_Lemma.lemma with
				  Definitions.l_assumptions = n_h;
				  Definitions.l_lemma = n_g
				}
			    };
			Wpo.po_gid = root.Wpo.po_gid ^ "_" ^ (string_of_int new_index);
			Wpo.po_partac = Some (new_index,parent,tactic);
			Wpo.po_leaves = [];
			Wpo.po_pid = new_po_pid
	}
      in
      replace_wpo_in_WpoSet parent [new_wpo];
      new_wpo

    let mk_lemma_child l =
      make_lemma_child ~parent:l.ls_parent ~lemma:l.ls_lemma ~hypotheses:l.ls_hypotheses
		       ~goal:l.ls_goal ~mk_part:No ~tactic:l.ls_tactic

    let mk_lemma_children current_wpo (lems:lem_skel list):Wpo.t list =
      if List.length lems <= 1 then
	List.map (fun l -> make_lemma_child ~parent:l.ls_parent ~lemma:l.ls_lemma
					    ~hypotheses:l.ls_hypotheses
					    ~goal:l.ls_goal ~mk_part:No ~tactic:l.ls_tactic
		 ) lems
      else(
	let (base,total) = update_parts current_wpo lems in
	List.mapi (fun i l -> make_lemma_child ~parent:l.ls_parent ~lemma:l.ls_lemma
					       ~hypotheses:l.ls_hypotheses
					       ~goal:l.ls_goal ~mk_part:(Yes (base+i,total))
					       ~tactic:l.ls_tactic
		  ) lems
      )
  end

let get_step_by_order wpo step_index =
  match wpo.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (hyps, _) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     List.nth hyps step_index
  | Wpo.GoalLemma _ -> raise (Failure "function is not available to GoalLemma")
  | Wpo.GoalCheck _ -> raise (Failure "function is not available to GoalCheck")

let isRewritten w = match w.Wpo.po_partac with
  | None -> "false"
  | Some _ -> "true"

(*------*)
(* split conjunction hypothesis *)
(*------*)

open Build_Wpo

let split_conj_hyp_annot t (wpo:Wpo.t) annot index =
  let hyp_list = get_hyps wpo in
  let step = List.nth hyp_list index in
  let pred = get_predicate_from_step step in
  let new_preds = get_conjunction_terms (e_prop pred) in
  let new_steps = List.map (make_hypothesis_from_another_hypothesis step)
			   (p_bools new_preds) in
  let new_hyps = subst_steps hyp_list index new_steps in
  mk_annot_child {
      as_parent = wpo;
      as_annot = annot;
      as_hypotheses = (Some new_hyps);
      as_goal = None;
      as_tactic = t;
    }

let split_conj_hyp_lemma t wpo lemma hyp =
  let new_preds = get_conjunction_terms hyp in
  let hyp_list = get_assumptions lemma in
  let new_hyps = subst_terms (e_props hyp_list) hyp new_preds in
  mk_lemma_child {
      ls_parent = wpo;
      ls_lemma = lemma;
      ls_hypotheses = (Some (p_bools new_hyps));
      ls_goal = None;
      ls_tactic = t;
    }

let split_conjunction_in_hypothesis t wpo index =
  let tactic = split_and_hyp in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    split_conj_hyp_lemma t wpo lemma (e_prop pred)
  |GoalAnnot annot ->
    split_conj_hyp_annot t wpo annot index
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic))


let rec replace_elt list old_elt new_elt =
  match list with
  | [] -> []
  | hd:: tl -> if hd == old_elt then new_elt::tl else
		 hd:: (replace_elt tl old_elt new_elt)

let get_number_of_disjunctions pred =
  match Lang.F.repr pred with
  | Or preds -> List.length preds
  | _ -> 1

let rec remove_element_from_list_by_index ?current_index: (c_index =0)
					  preds index_to_delete =
  match preds with
  | [] -> []
  | hd:: tl ->
     if c_index = index_to_delete then tl
     else
       hd:: (
       remove_element_from_list_by_index tl index_to_delete ~current_index: (c_index +1)
     )

(*-----------------------------------*)
(* remove term from disjunction goal *)
(*-----------------------------------*)

let rem_term_from_disj_goal_annot t wpo annot index:Wpo.t =
  let goal = get_goal wpo in
  match repr (e_prop goal) with
  | Or preds ->
     let preds_with_removed_term = remove_element_from_list_by_index
				     preds index in
     let new_goal = e_or preds_with_removed_term in
     mk_annot_child {
	 as_parent = wpo;
	 as_annot = annot;
	 as_hypotheses = None;
	 as_goal = Some (p_bool new_goal);
	 as_tactic = t;
       }
  | _ -> raise (Failure "the goal must be a disjunction")

let rem_term_from_disj_goal_lemma t wpo lemma index:Wpo.t =
  let goal = get_goal wpo in
  match repr (e_prop goal) with
  | Or preds ->
     let preds_with_removed_term = remove_element_from_list_by_index
				     preds index in
     let new_goal = e_or preds_with_removed_term in
     let s = {
	 ls_parent = wpo;
	 ls_lemma = lemma;
	 ls_hypotheses = None;
	 ls_goal = (Some (p_bool new_goal));
	 ls_tactic = t;
       } in
     mk_lemma_child s
  | _ -> raise (Failure "the goal must be a disjunction")

let remove_term_from_disjunction_goal t wpo index =
  match wpo.Wpo.po_formula with
  |GoalAnnot annot -> rem_term_from_disj_goal_annot t wpo annot index
  |GoalLemma lemma -> rem_term_from_disj_goal_lemma t wpo lemma index
  | _ -> raise (Failure "This type of PO is not implemented yet for this tactic")


(* ------------------------------- *)
(* Split disjunction in hypothesis *)
(* ------------------------------- *)


let split_disjunction_in_pred pred =
  match repr pred with
  |Or preds -> preds
  |_ -> raise (Failure "predicate is not a disjunction")

let split_disjunction_in_step step =
  let pred = get_predicate_from_step step in
  let new_preds = split_disjunction_in_pred (e_prop pred) in
  make_steps step (p_bools new_preds)

let split_disj_in_hyp_annot t wpo annot (step:Conditions.step):Wpo.t list =
  let hyps = get_hyps wpo in
  let new_steps:step list = split_disjunction_in_step step in
  let s = List.map ( fun new_step ->
		     let new_hyps = replace_elt hyps step new_step in
		     {
		       as_parent = wpo;
		       as_annot = annot;
		       as_hypotheses = Some new_hyps;
		       as_goal = None;
		       as_tactic = t;
		     }
		   ) new_steps in
  mk_annot_children wpo s

let split_disj_in_hyp_lemma t wpo lemma hyp :Wpo.t list =
  let hyps = get_assumptions lemma in
  let new_steps = split_disjunction_in_pred hyp in
  let s = List.map ( fun new_step ->
		     let new_hyps = replace_elt hyps (p_bool hyp) new_step
		     in
		     {
		       ls_parent = wpo;
		       ls_lemma = lemma;
		       ls_hypotheses = Some new_hyps;
		       ls_goal = None;
		       ls_tactic = t;
		     }
		   ) (p_bools new_steps) in
  mk_lemma_children wpo s

let split_disjunction_in_hyp t wpo index =
  let tactic = split_term_disj_hyp in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    split_disj_in_hyp_lemma t wpo lemma (e_prop pred)
  |GoalAnnot annot ->
    let hyp = get_step_from_index wpo index in
    split_disj_in_hyp_annot t wpo annot hyp
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic))

(*---------------------------*)
(* split lower or equal than *)
(*---------------------------*)

let rec split_leq_in_pred pred =
  match Lang.F.repr pred with
  | Leq(a, b) ->
     let parsed_a = split_leq_in_pred a in
     let parsed_b = split_leq_in_pred b
     in let lt = Lang.F.p_lt parsed_a parsed_b in
	let eq = Lang.F.p_equal parsed_a parsed_b in
	e_prop (Lang.F.p_disj [lt; eq])
  | True -> pred
  | False -> pred
  | Not p -> e_not (split_leq_in_pred p)
  | Add xs -> e_sum (List.map (split_leq_in_pred) xs)
  | Times(t, a) -> e_times t (split_leq_in_pred a)
  | Eq(a, b) -> e_eq (split_leq_in_pred a) (split_leq_in_pred b)
  | Neq(a, b) -> e_neq (split_leq_in_pred a) (split_leq_in_pred b)
  | Lt(a, b) -> e_lt (split_leq_in_pred a) (split_leq_in_pred b)
  | If(a, b, c) ->
     let parsed_a = split_leq_in_pred a in
     let parsed_b = split_leq_in_pred b in
     let parsed_c = split_leq_in_pred c in
     e_if parsed_a parsed_b parsed_c
  | And ts -> e_and (List.map (split_leq_in_pred) ts)
  | Or ts -> e_or (List.map (split_leq_in_pred) ts)
  | Fun(f, ts) ->
     let parsed_ts = List.map (split_leq_in_pred) ts in
     e_fun f parsed_ts
  | Bind (q, v, b) ->
     let term = lc_repr b in
     let parsed_t = split_leq_in_pred term in
     c_bind q v parsed_t
  | Aget(a, b) ->
     let parsed_a = split_leq_in_pred a in
     let parsed_b = split_leq_in_pred b in
     e_get parsed_a parsed_b
  | Aset(a, b, c) ->
     let parsed_a = split_leq_in_pred a in
     let parsed_b = split_leq_in_pred b in
     let parsed_c = split_leq_in_pred c in
     e_set parsed_a parsed_b parsed_c
  | Rget(term, var) ->
     let parsed_term = split_leq_in_pred term in
     e_getfield parsed_term var
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (split_leq_in_pred) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = split_leq_in_pred p in
     let parsed_hs = List.map (split_leq_in_pred) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> pred
  | Kreal _ -> pred
  | Mul xs -> e_prod (List.map (split_leq_in_pred) xs)
  | Div(a, b) -> e_div (split_leq_in_pred a) (split_leq_in_pred b)
  | Mod(a, b) -> e_mod (split_leq_in_pred a) (split_leq_in_pred b)
  | Fvar _ -> pred
  | Bvar (_,_) -> pred
  | Imply(hs, p) ->
     let parsed_hs = List.map (split_leq_in_pred) hs in
     let parsed_p = split_leq_in_pred p in
     e_imply parsed_hs parsed_p

let rec remove_elt list el =
  match list with
  | [] -> []
  | hd:: tl -> if hd == el then tl else
		 hd:: (remove_elt tl el)

(*---------------------------*)
(* Subset of hypotheses      *)
(*---------------------------*)


let get_index_of_step_list w step =
  match w.Wpo.po_formula with
  | Wpo.GoalAnnot annot ->
     let (hyps, _) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     let index = ref (-1) in
     List.iteri (fun i s -> if step == s then index := i) hyps;
     !index
  | Wpo.GoalLemma _ -> raise (Failure "GoalLemma not implemented yet")
  | Wpo.GoalCheck _ -> raise (Failure "GoalCheck not implemented yet")

let mk_wpo_from_hyp_indexes t wpo indexes: Wpo.t =
  match wpo.Wpo.po_formula with
  |GoalAnnot annot ->
    let hyps = List.map (get_step_from_index wpo) indexes in
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = Some hyps;
	as_goal = None;
	as_tactic = t;
      }
  |GoalLemma lemma ->
    let hyps = List.map (List.nth (get_assumptions lemma)) indexes in
    mk_lemma_child {
	ls_lemma = lemma;
	ls_tactic = t;
	ls_hypotheses = (Some hyps);
	ls_parent = wpo;
	ls_goal = None;
      }
  | _ -> raise (Failure (failed_msg ~po_gid:wpo.Wpo.po_gid ~tactic:(name_of_tactic t)))

let rec instantiate_quantifier_aux (exp:Lang.F.term) pred =
  match repr pred with
  | Bvar (i,_b) -> if i=0 then exp else pred
  | Leq(a, b) ->
     let parsed_a = instantiate_quantifier_aux exp a in
     let parsed_b = instantiate_quantifier_aux exp b in
     e_prop (p_leq parsed_a parsed_b)
  | True -> pred
  | False -> pred
  | Not p -> e_not (instantiate_quantifier_aux exp p)
  | Add xs -> e_sum (List.map (instantiate_quantifier_aux exp) xs)
  | Times(t, a) -> e_times t (instantiate_quantifier_aux exp a)
  | Eq(a, b) -> e_eq (instantiate_quantifier_aux exp a)
		     (instantiate_quantifier_aux exp b)
  | Neq(a, b) -> e_neq (instantiate_quantifier_aux exp a)
		       (instantiate_quantifier_aux exp b)
  | Lt(a, b) -> e_lt (instantiate_quantifier_aux exp a)
		     (instantiate_quantifier_aux exp b)
  | If(a, b, c) ->
     let parsed_a = instantiate_quantifier_aux exp a in
     let parsed_b = instantiate_quantifier_aux exp b in
     let parsed_c = instantiate_quantifier_aux exp c in
     e_if parsed_a parsed_b parsed_c
  | And ts -> e_and (List.map (instantiate_quantifier_aux exp) ts)
  | Or ts -> e_or (List.map (instantiate_quantifier_aux exp) ts)
  | Fun(f, ts) ->
     let parsed_ts = List.map (instantiate_quantifier_aux exp) ts in
     e_fun f parsed_ts
  | Bind (q, v, b) -> c_bind q v (lc_repr b)
  | Aget(a, b) ->
     let parsed_a = instantiate_quantifier_aux exp a in
     let parsed_b = instantiate_quantifier_aux exp b in
     e_get parsed_a parsed_b
  | Aset(a, b, c) ->
     let parsed_a = instantiate_quantifier_aux exp a in
     let parsed_b = instantiate_quantifier_aux exp b in
     let parsed_c = instantiate_quantifier_aux exp c in
     e_set parsed_a parsed_b parsed_c
  | Rget(term, v) ->
     let parsed_term = instantiate_quantifier_aux exp term in
     e_getfield parsed_term v
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (instantiate_quantifier_aux exp) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = instantiate_quantifier_aux exp p in
     let parsed_hs = List.map (instantiate_quantifier_aux exp) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> pred
  | Kreal _ -> pred
  | Mul xs -> e_prod (List.map (instantiate_quantifier_aux exp) xs)
  | Div(a, b) -> e_div (instantiate_quantifier_aux exp a)
		       (instantiate_quantifier_aux exp b)
  | Mod(a, b) -> e_mod (instantiate_quantifier_aux exp a)
		       (instantiate_quantifier_aux exp b)
  | Fvar _ -> pred
  | Imply(hs, p) ->
     let parsed_hs = List.map (instantiate_quantifier_aux exp) hs in
     let parsed_p = instantiate_quantifier_aux exp p in
     e_imply parsed_hs parsed_p

let instantiate_quantifier binder (exp:Lang.F.term) pred =
  match repr pred with
  | Bind (quant,v,b) ->
     if quant = binder then
       if tau_of_sort (sort exp) = v then (
	 let b = lc_repr b in
	 instantiate_quantifier_aux exp b
       )
       else
	 raise (Failure "Tau of bound var and expression are not equals")
     else
       raise (Failure "Incorrect quantifier for instantiation")
  | _ -> raise (Failure "Selected term is not a quantification")

(*--------------------------------------------------*)
(* Universal quantifier instantiation in hypothesis *)
(*--------------------------------------------------*)

let instantiate_universal_quantifier_hypothesis_annot t wpo annot step (exp:Lang.F.term) :Wpo.t =
  let pred = get_predicate_from_step step in
  let new_pred = instantiate_quantifier Forall exp (e_prop pred) in
  let new_step =
    make_hypothesis_from_another_hypothesis step (p_bool new_pred) in
  let hyp_list = get_hyps wpo in
  let new_hyp_list = hyp_list @ [new_step] in
  mk_annot_child {
      as_parent = wpo;
      as_annot = annot;
      as_hypotheses = Some new_hyp_list;
      as_goal = None;
      as_tactic = t;
    }

let instantiate_universal_quantifier_hypothesis_lemma t wpo lemma pred exp:Wpo.t =
  let new_pred = p_bool (instantiate_quantifier Forall exp pred) in
  let hyp_list = get_assumptions lemma in
  let new_hyp_list = hyp_list @ [new_pred] in
  mk_lemma_child {
      ls_parent = wpo;
      ls_goal = None;
      ls_hypotheses = (Some new_hyp_list);
      ls_tactic = t;
      ls_lemma = lemma;
    }

let instantiate_universal_quantifier_hypothesis t wpo index exp =
  let tactic = instantiate_universal_quantifier_hyps in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    instantiate_universal_quantifier_hypothesis_lemma t wpo lemma
						      (e_prop pred) exp
  |GoalAnnot annot ->
    let hyp = get_step_from_index wpo index in
    instantiate_universal_quantifier_hypothesis_annot t wpo annot hyp exp
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic))

(*--------------------------------------------------*)
(* Existential quantifier instantiation in goal     *)
(*--------------------------------------------------*)

let instantiate_existential_quantifier_in_goal t wpo var:Wpo.t =
  let goal = get_goal wpo in
  let new_goal = instantiate_quantifier Exists var (e_prop goal) in
  match wpo.Wpo.po_formula with
  |GoalAnnot annot ->
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = None;
	as_goal = Some (p_bool new_goal);
	as_tactic = t;
      }
  |GoalLemma lemma ->
    mk_lemma_child {
	ls_lemma = lemma;
	ls_parent = wpo;
	ls_goal = Some (p_bool new_goal);
	ls_hypotheses = None;
	ls_tactic = t
      }
  | _ -> raise (Failure "This tactic was not implemented for this type of PO")

(*--------------------------------------------------*)
(* Rewrite branch     *)
(*--------------------------------------------------*)

let rewrite_step t wpo step =
  let pred = get_predicate_from_step step in
  let new_step = make_hypothesis_from_another_hypothesis step pred in
  let hyp_list = get_hyps wpo in
  let new_hyp_list = replace_elt hyp_list step new_step in
  match wpo.Wpo.po_formula with
  |GoalAnnot annot ->
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = Some new_hyp_list;
	as_goal = None;
	as_tactic = t;
      }
  |_-> raise (Failure "This tactic supports only POs of type annotation")


let is_implication pred =
  match repr pred with
  |Imply _ -> true
  | _ -> false

let split_implication pred =
  match repr pred with
  | Imply (terms, term) -> (
    match terms with
    |[] -> raise (Failure "left side of implication should not be empty")
    |[t] -> (t,term)
    |t::ts -> (t,e_imply ts term)
  )
  | _ -> raise (Failure "the predicate is not an implication")

let split_equality pred =
  match repr pred with
  | Eq (left,right) -> (left,right)
  | _ -> raise (Failure "the predicate is not an equality")

(* -------------------------- *)
(* Use equality in hypothesis *)
(* -------------------------- *)

let rec equal old neo:bool =
  match repr old,repr neo with
  | Bvar _, _any  -> false
  | _any, Bvar _ -> false
  | Fvar v1, Fvar v2 -> Lang.F.Var.equal v1 v2
  | Fun(f1,ts1),Fun(f2,ts2) -> Lang.F.Fun.equal f1 f2 &&
				 List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Leq (a1,a2),Leq(b1,b2) -> equal a1 b1 && equal a2 b2
  | True,True -> true
  | False,False -> true
  | Not p1,Not p2 -> equal p1 p2
  | Add ts1,Add ts2 ->
     List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Times(t1, a1),Times (t2,a2) -> t1 = t2 && equal a1 a2
  | Eq(a1, a2),Eq(b1,b2) ->  equal a1 b1 && equal a2 b2
  | Neq(a1,a2),Neq(b1,b2) ->  equal a1 b1 && equal a2 b2
  | Lt(a1, a2),Lt(b1,b2) ->  equal a1 b1 && equal a2 b2
  | If(a1, a2, a3),If(b1,b2,b3) ->  equal a1 b1 && equal a2 b2 && equal a3 b3
  | And ts1,And ts2 ->
     List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Or ts1,Or ts2 ->
     List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Bind _,Bind _ -> false
  | Aget(a1, a2),Aget(b1,b2) ->  equal a1 b1 && equal a2 b2
  | Aset(a1, a2, a3),Aset(b1,b2,b3) ->  equal a1 b1 && equal a2 b2 && equal a3 b3
  | Rget(t1, v1),Rget(t2,v2) -> equal t1 t2 && Lang.F.Field.equal v1 v2
  | Rdef fts1,Rdef fts2 ->
     let (fs1, ts1) = List.split fts1 in
     let (fs2, ts2) = List.split fts2 in
     List.for_all2 (fun v1 v2 -> Lang.F.Field.equal v1 v2) fs1 fs2 &&
       List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Apply(p1, hs1),Apply (p2,hs2) ->
     List.for_all2 (fun t1 t2 -> equal t1 t2) (p1::hs1) (p2::hs2)
  | Kint x1,Kint x2 -> x1 = x2
  | Kreal x1,Kreal x2 -> x1 = x2
  | Mul ts1,Mul ts2 ->
     List.for_all2 (fun t1 t2 -> equal t1 t2) ts1 ts2
  | Div(a1, a2),Div (b1,b2) ->  equal a1 b1 && equal a2 b2
  | Mod(a1, a2),Mod(b1,b2) ->  equal a1 b1 && equal a2 b2
  | Imply(hs1, p1), Imply (hs2,p2) -> equal p1 p2 &&
					List.for_all2 (fun t1 t2 -> equal t1 t2) hs1 hs2
  | _,_ -> false

let rec replace_term origin subs term:term =
  if equal origin term then subs else
    match repr term with
    | Bvar (_i,_b) -> term
    | Leq(a, b) ->
       let parsed_a = replace_term origin subs a in
       let parsed_b = replace_term origin subs b in
       e_prop (p_leq parsed_a parsed_b)
    | True -> term
    | False -> term
    | Not p -> e_not (replace_term origin subs p)
    | Add xs -> e_sum (List.map (replace_term origin subs) xs)
    | Times(t, a) -> e_times t (replace_term origin subs a)
    | Eq(a, b) -> e_eq (replace_term origin subs a)
		       (replace_term origin subs b)
    | Neq(a, b) -> e_neq (replace_term origin subs a)
			 (replace_term origin subs b)
    | Lt(a, b) -> e_lt (replace_term origin subs a)
		       (replace_term origin subs b)
    | If(a, b, c) ->
       let parsed_a = replace_term origin subs a in
       let parsed_b = replace_term origin subs b in
       let parsed_c = replace_term origin subs c in
       e_if parsed_a parsed_b parsed_c
    | And ts -> e_and (List.map (replace_term origin subs) ts)
    | Or ts -> e_or (List.map (replace_term origin subs) ts)
    | Fun(f, ts) ->
       let parsed_ts = List.map (replace_term origin subs) ts in
       e_fun f parsed_ts
    | Bind (q, v, b) -> c_bind q v (replace_term origin subs (lc_repr b))
    | Aget(a, b) ->
       let parsed_a = replace_term origin subs a in
       let parsed_b = replace_term origin subs b in
       e_get parsed_a parsed_b
    | Aset(a, b, c) ->
       let parsed_a = replace_term origin subs a in
       let parsed_b = replace_term origin subs b in
       let parsed_c = replace_term origin subs c in
       e_set parsed_a parsed_b parsed_c
    | Rget(term, v) ->
       let parsed_term = replace_term origin subs term in
       e_getfield parsed_term v
    | Rdef fts ->
       let (vars, terms) = List.split fts in
       let parsed_terms = List.map (replace_term origin subs) terms in
       let combined_tuple = List.combine vars parsed_terms in
       e_record combined_tuple
    | Apply(p, hs) ->
       let parsed_p = replace_term origin subs p in
       let parsed_hs = List.map (replace_term origin subs) hs in
       e_apply parsed_p parsed_hs
    | Kint _ -> term
    | Kreal _ -> term
    | Mul xs -> e_prod (List.map (replace_term origin subs) xs)
    | Div(a, b) -> e_div (replace_term origin subs a)
			 (replace_term origin subs b)
    | Mod(a, b) -> e_mod (replace_term origin subs a)
			 (replace_term origin subs b)
    | Fvar _ -> term
    | Imply(hs, p) ->
       let parsed_hs = List.map (replace_term origin subs) hs in
       let parsed_p = replace_term origin subs p in
       e_imply parsed_hs parsed_p


let use_eq_h_annot t wpo annot origin_term subst_term cond:Wpo.t =
  let steps = get_hyps wpo in
  let func s =
    let p = get_predicate_from_step s in
    try
      let (left,right) = split_equality (e_prop p) in
      (equal origin_term left && equal subst_term right)
      ||
	(equal origin_term right && equal subst_term left)
    with Failure _m -> false
  in
  try
    let eq_step = List.find func steps in
    match cond with
    | Goal ->
       let goal = get_goal wpo in
       let new_goal = replace_term origin_term subst_term (e_prop goal) in
       mk_annot_child {
	   as_parent = wpo;
	   as_annot = annot;
	   as_hypotheses =None;
	   as_goal = Some (p_bool new_goal);
	   as_tactic = t;
	 }
    | AllHyp ->
       let replace step =
	 if step == eq_step then step
	 else let pred = get_predicate_from_step step in
	      let new_pred = replace_term origin_term subst_term
					  (e_prop pred) in
	      let new_step = make_hypothesis_from_another_hypothesis
			       step (p_bool new_pred) in
	      new_step
       in
       let new_steps = List.map replace steps in
       mk_annot_child {
	   as_parent = wpo;
	   as_annot = annot;
	   as_hypotheses = Some new_steps;
	   as_goal = None;
	   as_tactic = t;
	 }
    | Hyp index ->
       let step = List.nth steps index in
       let pred = get_predicate_from_step step in
       let new_pred = replace_term origin_term subst_term (e_prop pred) in
       let new_step = make_hypothesis_from_another_hypothesis
			step (p_bool new_pred) in
       let new_steps = replace_elt steps step new_step in
       mk_annot_child {
	   as_parent = wpo;
	   as_annot = annot;
	   as_hypotheses = Some new_steps;
	   as_goal = None;
	   as_tactic = t;
	 }
  with Not_found ->
    raise (Failure "There is no equality between the two terms in hypothesis list")

let use_eq_h_lem t wpo (lemma:Wpo.VC_Lemma.t) origin_term subst_term cond:Wpo.t =
  let assumptions = lemma.Wpo.VC_Lemma.lemma.Definitions.l_assumptions in
  try
    match cond with
    | Goal ->
       let goal = get_goal wpo in
       let new_goal = replace_term origin_term subst_term (e_prop goal) in
       mk_lemma_child {
	   ls_tactic = t;
	   ls_parent = wpo;
	   ls_goal = Some (p_bool new_goal);
	   ls_hypotheses = None;
	   ls_lemma = lemma;
	 }
    | AllHyp ->
       let replace pred = replace_term origin_term subst_term pred in
       let new_preds = List.map replace (e_props assumptions) in
       mk_lemma_child {
	   ls_tactic = t;
	   ls_parent = wpo;
	   ls_goal = None;
	   ls_hypotheses =Some (p_bools new_preds);
	   ls_lemma = lemma;
	 }
    | Hyp index ->
       let pred = List.nth assumptions index in
       let new_pred = replace_term origin_term subst_term (e_prop pred) in
       let new_preds = replace_elt assumptions pred (p_bool new_pred) in
       mk_lemma_child {
	   ls_tactic = t;
	   ls_parent = wpo;
	   ls_goal = None;
	   ls_hypotheses = Some new_preds;
	   ls_lemma = lemma;
	 }
  with Not_found ->
    raise (Failure "There is no equality between the two terms in hypothesis list")

let use_equality_in_hypothesis t wpo origin_term subst_term cond:Wpo.t =
  match wpo.Wpo.po_formula with
  | GoalAnnot a -> use_eq_h_annot t wpo a origin_term subst_term cond
  | GoalLemma l -> use_eq_h_lem t wpo l origin_term subst_term cond
  | _ -> raise (Failure "Use equality no implemented yet for this kind of PO")

(* ----------------------- *)
(* Modus ponens hypothesis *)
(* ----------------------- *)

let modus_ponens_hyp_annot t wpo annot step =
  let pred = get_predicate_from_step step in
  if is_implication (e_prop pred) then
    let (left,right) = split_implication (e_prop pred) in
    let preds = get_hyps wpo in
    let goal = get_goal wpo in
    if List.exists ( fun p ->
		     let pr = get_predicate_from_step p in
		     let res = is_equal (e_prop pr) left in
		     match res with
		     |Qed.Logic.Yes -> true
		     | _ -> false) preds
    then
      let new_goal = p_imply (p_bool right) goal in
      let new_steps =
	List.filter
	  (fun p ->
	   let pr = get_predicate_from_step p in
	   if (equal (e_prop pr) left || equal (e_prop pred) (e_prop pr))
	   then false
	   else true
	  ) preds
      in
      mk_annot_child {
	  as_parent = wpo;
	  as_annot = annot;
	  as_hypotheses = Some new_steps;
	  as_goal = Some new_goal;
	  as_tactic = t;
	}
    else raise (Failure "There is no hypothesis equal to the left side of the implication")
  else raise (Failure "The selected hypothesis is not an implication")

let modus_ponens_hyp_lemma t wpo lemma pred =
  if is_implication pred then
    let (left,right) = split_implication pred in
    let preds = get_assumptions lemma in
    let goal = get_goal wpo in
    if List.exists ( fun p -> let res = is_equal p left in
			      match res with
			      |Qed.Logic.Yes -> true
			      | _ -> false) (e_props preds)
    then
      let new_goal = p_imply (p_bool right) goal in
      let new_preds = List.filter (fun p ->
				   if (equal p left || equal pred p)
				   then false
				   else true) (e_props preds) in
      mk_lemma_child {
	  ls_parent = wpo;
	  ls_hypotheses = Some (p_bools new_preds);
	  ls_goal = Some new_goal;
	  ls_tactic = t;
	  ls_lemma = lemma;
	}
    else
      raise
	(Failure
	   "There is no hypothesis equal to the left side of the implication")
  else raise (Failure "The selected hypothesis is not an implication")

let modus_ponens_hyp t wpo index =
  let tactic = modus_ponens_hyps in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    modus_ponens_hyp_lemma t wpo lemma (e_prop pred)
  |GoalAnnot annot ->
    let hyp = get_step_from_index wpo index in
    modus_ponens_hyp_annot t wpo annot hyp
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic))

(*---------------------------*)
(* Rewrite if then else goal *)
(*---------------------------*)

let rewrite_if_then_else term =
  match repr term with
  |If(a,b,c) -> let a = p_bool a in
		let b = p_bool b in
		let c = p_bool c in
		let neg_a = p_not a in
		p_disj [(p_conj [a;b]); (p_conj [neg_a;c])]
  |_ -> p_bool term

let rewrite_if_then_else_goal t wpo :Wpo.t =
  let goal = get_goal wpo in
  let new_goal = rewrite_if_then_else (e_prop goal) in
  match wpo.Wpo.po_formula with
  |GoalAnnot annot ->
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = None;
	as_goal = Some new_goal;
	as_tactic = t;
      }
  |GoalLemma lemma ->
    mk_lemma_child {
	ls_lemma = lemma;
	ls_parent = wpo;
	ls_hypotheses = None;
	ls_goal = Some new_goal;
	ls_tactic = t
      }
  | _ -> raise (Failure "This tactic was not impemented for this type of PO")


let insert_implication left_pred implied_pred = p_imply left_pred implied_pred

(* -------------- *)
(* proof by cases *)
(* -------------- *)

let proof_by_cases_annot t wpo annot case_predicate: Wpo.t list =
  let neg_predicate = e_not case_predicate in
  let goal = get_goal wpo in
  let new_goal = insert_implication (p_bool case_predicate) goal in
  let new_neg_goal = insert_implication (p_bool neg_predicate) goal in
  mk_annot_children wpo [{
			    as_parent = wpo;
			    as_annot = annot;
			    as_hypotheses = None;
			    as_goal = Some new_goal;
			    as_tactic = t;
			  }; {
			    as_parent = wpo;
			    as_annot = annot;
			    as_hypotheses = None;
			    as_goal = Some new_neg_goal;
			    as_tactic = t;
			  }]

let proof_by_cases_lemma t wpo lemma case_predicate: Wpo.t list =
  let neg_predicate = e_not case_predicate in
  let goal = get_goal wpo in
  let new_goal = insert_implication (p_bool case_predicate) goal in
  let new_neg_goal = insert_implication (p_bool neg_predicate) goal in
  mk_lemma_children wpo [{
			    ls_lemma = lemma;
			    ls_parent = wpo;
			    ls_hypotheses = None;
			    ls_goal = Some new_goal;
			    ls_tactic = t
			  };{
			    ls_lemma = lemma;
			    ls_parent = wpo;
			    ls_goal = Some new_neg_goal;
			    ls_hypotheses = None;
			    ls_tactic = t
			  }]

let proof_by_cases t wpo case_predicate: Wpo.t list =
  match wpo.Wpo.po_formula with
  |GoalAnnot g -> proof_by_cases_annot t wpo g case_predicate
  |GoalLemma l -> proof_by_cases_lemma t wpo l case_predicate
  | _ -> raise (Failure "proof_by_cases not implemented yet in this type of wpo")

let make_new_hypothesis predicate =
  let condition = Have predicate in Conditions.step condition

(* ----- *)
(* intro *)
(* ----- *)

let is_universal_quantifier term =
  match repr term with
  | Bind (q, _, _) -> (match q with Forall -> true | _ -> false)
  | _ -> false

let rec replace_bound_to_free_var tau new_term term:term =
  match repr term with
  | Leq(a, b) ->
     e_leq (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | True -> term
  | False -> term
  | Not p -> e_not (replace_bound_to_free_var tau new_term p)
  | Add xs -> e_sum (List.map (replace_bound_to_free_var tau new_term) xs)
  | Times(t, a) -> e_times t (replace_bound_to_free_var tau new_term a)
  | Eq(a, b) ->
     e_eq (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | Neq(a, b) ->
     e_neq (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | Lt(a, b) ->
     e_lt (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | If(a, b, c) -> let parsed_a = replace_bound_to_free_var tau new_term a in
		   let parsed_b = replace_bound_to_free_var tau new_term b in
		   let parsed_c = replace_bound_to_free_var tau new_term c in
		   e_if parsed_a parsed_b parsed_c
  | And ts -> e_and (List.map (replace_bound_to_free_var tau new_term) ts)
  | Or ts -> e_or (List.map (replace_bound_to_free_var tau new_term) ts)
  | Fun(f, ts) ->
     let parsed_ts = List.map (replace_bound_to_free_var tau new_term) ts in
     e_fun f parsed_ts
  | Bind (q, v, b) ->
     let bt = lc_repr b in
     let parsed_t = replace_bound_to_free_var tau new_term bt in
     c_bind q v parsed_t
  | Aget(a, b) ->
     let parsed_a = replace_bound_to_free_var tau new_term a in
     let parsed_b = replace_bound_to_free_var tau new_term b in
     e_get parsed_a parsed_b
  | Aset(a, b, c) ->
     let parsed_a = replace_bound_to_free_var tau new_term a in
     let parsed_b = replace_bound_to_free_var tau new_term b in
     let parsed_c = replace_bound_to_free_var tau new_term c in
     e_set parsed_a parsed_b parsed_c
  | Rget(term, var) ->
     let parsed_term = replace_bound_to_free_var tau new_term term in
     e_getfield parsed_term var
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (replace_bound_to_free_var tau new_term) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = replace_bound_to_free_var tau new_term p in
     let parsed_hs = List.map (replace_bound_to_free_var tau new_term) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> term
  | Kreal _ -> term
  | Mul xs -> e_prod (List.map (replace_bound_to_free_var tau new_term) xs)
  | Div(a, b) ->
     e_div (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | Mod(a, b) ->
     e_mod (replace_bound_to_free_var tau new_term a) (replace_bound_to_free_var tau new_term b)
  | Fvar _ -> term
  | Bvar (i,q) -> let (index,t) = tau in
		  if q = t && i = index then new_term
		  else term
  | Imply(hs, p) ->
     let parsed_hs = List.map (replace_bound_to_free_var tau new_term) hs in
     let parsed_p = replace_bound_to_free_var tau new_term p in
     e_imply parsed_hs parsed_p

let intro_failure_msg = "There is no implication of universal quantifier to apply intro"

let intro_annot t wpo annot =
  let goal = e_prop (get_goal wpo) in
  if (is_implication goal) then
    let (left_pred,right_pred) = split_implication goal in
    let hypothesis = make_new_hypothesis (p_bool left_pred) in
    let hyp_list = get_hyps wpo in
    let new_hyps_list = hyp_list @ [hypothesis] in
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = Some new_hyps_list;
	as_goal = Some (p_bool right_pred);
	as_tactic = t;
      }
  else if (is_universal_quantifier goal) then
    let varS = get_vars wpo in
    let tau = get_quantifier_tau goal in
    let new_var = mk_var varS tau in
    let new_term = e_var new_var in
    let new_goal = replace_bound_to_free_var (0,tau) new_term goal in
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = None;
	as_goal = Some (p_bool new_goal);
	as_tactic = t;
      }
  else raise (Failure intro_failure_msg)

let intro_lemma t wpo lemma:Wpo.t =
  let assumptions = lemma.Wpo.VC_Lemma.lemma.Definitions.l_assumptions in
  let goal = get_goal wpo in
  if (is_implication (e_prop goal)) then
    let (left_pred,right_pred) = split_implication (e_prop goal) in
    let hypothesis = left_pred in
    let new_assumptions = assumptions @ [p_bool hypothesis] in
    mk_lemma_child {
	ls_lemma = lemma;
	ls_parent = wpo;
	ls_tactic = t;
	ls_hypotheses = Some new_assumptions;
	ls_goal = Some (p_bool right_pred)
      }
  else if (is_universal_quantifier (e_prop goal)) then
    let varS = get_vars wpo in
    let tau = get_quantifier_tau (e_prop goal) in
    let new_var = mk_var varS tau in
    let new_term = e_var new_var in
    let new_goal =
      replace_bound_to_free_var (0,tau) new_term (e_prop goal) in
    mk_lemma_child {
	ls_lemma = lemma;
	ls_parent = wpo;
	ls_goal = Some (p_bool new_goal);
	ls_hypotheses = None;
	ls_tactic = t;
      }
  else raise (Failure intro_failure_msg)

let intro t wpo =
  match wpo.Wpo.po_formula with
  |GoalAnnot a -> intro_annot t wpo a
  |GoalLemma l -> intro_lemma t wpo l
  | _ -> raise (Failure "intro not implemented yet for other types of wpo")

(* ------------------------ *)
(* Apply hypothesis in goal *)
(* ------------------------ *)

(* TODO: Encontrar a justifica para esta regra *)

let apply_hypothesis_in_goal_annot t wpo annot step =
  let goal = get_goal wpo in
  let pred = get_predicate_from_step step in
  let (left_pred,right_pred) = split_implication (e_prop pred) in
  if equal (e_prop goal) right_pred then (
    mk_annot_child {
	as_parent = wpo;
	as_annot = annot;
	as_hypotheses = None;
	as_goal = Some (p_bool left_pred);
	as_tactic = t;
      }
  )
  else raise (Failure ("Cannot apply hypothesis: mismatch between goal and " ^
			 "right side of hypothesis"))

let apply_hypothesis_in_goal_lemma t wpo lemma pred =
  let goal = get_goal wpo in
  let (left_pred,right_pred) = split_implication pred in
  if equal (e_prop goal) right_pred then (
    mk_lemma_child {
	ls_tactic = t;
	ls_parent = wpo;
	ls_goal = Some (p_bool left_pred);
	ls_hypotheses = None;
	ls_lemma = lemma;
      }
  )
  else raise (Failure ("Cannot apply hypothesis: mismatch between goal and " ^
			 "right side of hypothesis"))

let apply_hypothesis_in_goal t wpo index =
  let tactic = apply_hypothesis_in_goal in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    apply_hypothesis_in_goal_lemma t wpo lemma (e_prop pred)
  |GoalAnnot annot ->
    let hyp = get_step_from_index wpo index in
    apply_hypothesis_in_goal_annot t wpo annot hyp
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic))


(* ------------------------- *)
(* split lower or equal goal *)
(* ------------------------- *)

let split_leq_goal t wpo :Wpo.t=
  let goal = get_goal wpo in
  let new_goal = split_leq_in_pred (e_prop goal) in
  let new_wpo =
    match wpo.po_formula with
    | GoalAnnot annot ->
       mk_annot_child {
	   as_parent = wpo;
	   as_annot = annot;
	   as_hypotheses = None;
	   as_goal = Some (p_bool new_goal);
	   as_tactic = t;
	 }
    | GoalLemma lemma -> mk_lemma_child {
			     ls_goal = Some (p_bool new_goal);
			     ls_hypotheses = None;
			     ls_parent = wpo;
			     ls_tactic = t;
			     ls_lemma = lemma;
			   }
    | _ -> raise (Failure "Tactic not yet implemented for this type of PO")
  in new_wpo

(* ------------------------- *)
(* split conjunction in goal *)
(* ------------------------- *)

let split_goal_conjunction t wpo =
  let goal = get_goal wpo in
  let new_goals = get_conjunction_terms (e_prop goal) in
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let ns = List.map (fun new_goal ->
		       {
			 ls_parent = wpo;
			 ls_tactic = t;
			 ls_goal = Some new_goal;
			 ls_hypotheses = None;
			 ls_lemma = lemma;
		       }
		      ) (p_bools new_goals) in
    mk_lemma_children wpo ns
  |GoalAnnot annot ->
    let ns = List.map (fun new_goal ->
		       {
			 as_parent = wpo;
			 as_tactic = t;
			 as_goal = Some new_goal;
			 as_hypotheses = None;
			 as_annot = annot;
		       }
		      ) (p_bools new_goals) in
    mk_annot_children wpo ns
  |_ -> raise (Failure "Tactic not yet implemented for this type of PO")

let make_step pred =
  let have_condition = Conditions.Have pred in
  Conditions.step have_condition

(* --------------------- *)
(* Contrapose hypothesis *)
(* --------------------- *)

let contrapose_hypothesis_annot t wpo annot hyp =
  let (hyps_list, goal) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
  let contradicted_goal = Lang.F.p_not goal in
  let hyps_list_without_hyp = remove_elt hyps_list hyp in
  let hypothesis_pred = get_predicate_from_step hyp in
  let contradicted_hyp_pred = Lang.F.p_not hypothesis_pred in
  let contradicted_goal_step = make_step contradicted_goal in
  mk_annot_child {
      as_parent = wpo;
      as_annot = annot;
      as_hypotheses = Some (hyps_list_without_hyp @ [contradicted_goal_step]);
      as_goal = Some contradicted_hyp_pred;
      as_tactic = t;
    }

let contrapose_hypothesis_lemma t wpo lemma (hyp:Lang.F.term) =
  let hyps_list = get_assumptions lemma in
  let goal = get_goal wpo in
  let contradicted_goal = Lang.F.p_not goal in
  let hyps_list_without_hyp = remove_elt hyps_list (p_bool hyp) in
  let contradicted_hyp_pred = Lang.F.p_not (p_bool hyp) in
  mk_lemma_child {
      ls_tactic = t;
      ls_parent = wpo;
      ls_lemma = lemma;
      ls_hypotheses = (Some (hyps_list_without_hyp @
			       [contradicted_goal]));
      ls_goal = Some contradicted_hyp_pred
    }

let contrapose_hypothesis t wpo index =
  match wpo.Wpo.po_formula with
  |GoalLemma lemma ->
    let pred = List.nth (get_assumptions lemma) index in
    contrapose_hypothesis_lemma t wpo lemma (e_prop pred)
  |GoalAnnot annot ->
    let hyp = get_step_from_index wpo index in
    contrapose_hypothesis_annot t wpo annot hyp
  | _ -> raise (Failure (failed_msg ~po_gid: wpo.Wpo.po_gid ~tactic:(name_of_tactic t)))

(* --------------- *)
(* Contradict *)
(* --------------- *)

let contradict_annot t wpo annot =
  let (hyps, goal) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
  let contradicted_goal = Lang.F.p_not goal in
  let false_bottom = Lang.F.p_false in
  let contradicted_goal_step = make_step contradicted_goal in
  mk_annot_child {
      as_parent = wpo;
      as_tactic = t ;
      as_annot = annot;
      as_hypotheses = Some (hyps @ [contradicted_goal_step]);
      as_goal = Some false_bottom;
    }


let contradict_lemma t wpo lemma =
  let hyps = get_assumptions lemma in
  let goal = get_goal wpo in
  let contradicted_goal = Lang.F.p_not goal in
  let false_bottom = Lang.F.p_false in
  mk_lemma_child {
      ls_parent = wpo;
      ls_tactic = t ;
      ls_lemma = lemma;
      ls_hypotheses = Some (hyps @ [contradicted_goal]);
      ls_goal = Some false_bottom;
    }

let contradict t wpo =
  match wpo.Wpo.po_formula with
  | GoalAnnot annot -> contradict_annot t wpo annot
  | GoalLemma lemma -> contradict_lemma t wpo lemma
  | _ -> raise (Failure "Contradict is not implemented yet for Check")

let is_valid_or_is_failed_to_string result =
  match result.VCS.verdict with
  |VCS.Valid -> "Valid"
  |VCS.NoResult
  |VCS.Invalid
  |VCS.Unknown
  |VCS.Timeout
  |VCS.Stepout
  |VCS.Computing _
  |VCS.Checked
  |VCS.Failed -> "Failed"

let lemma_to_prove (lemma: Wpo.VC_Lemma.t) =
  let l = lemma.Wpo.VC_Lemma.lemma.Definitions.l_lemma in
  let s = Format.str_formatter in
  Lang.F.pp_pred s l;
  Format.flush_str_formatter ()

let string_of_VC_lemma lemma =
  let lemma_string = lemma_to_prove lemma in
  ([], lemma_string)

let string_of_wpo w =
  match w.Wpo.po_formula with
  | GoalAnnot annot ->
     let (hyps, goal) = Wpo.GOAL.compute_descr annot.Wpo.VC_Annot.goal in
     (string_list_of_hyps hyps, string_of_term (e_prop goal))
  | GoalLemma lemma -> string_of_VC_lemma lemma
  | GoalCheck _ -> raise (Failure "GoalCheck not implemented yet")

let rec get_descendants wpo =
  let children = wpo.Wpo.po_leaves in
  let total_descendants = ref children in
  let length = List.length children in
  for i = 0 to (length-1) do
    let child = List.nth children i in
    let descendants = get_descendants child in
    total_descendants := !total_descendants @ descendants
  done;
  !total_descendants

let has_condition pred =
  match repr pred with
  |If (_,_,_) -> true
  |_ -> false

(* ----------------- *)
(* Remove hypotheses *)
(* ----------------- *)

let remove_hypothesis_annot t wpo annot step:Wpo.t =
  let hyps = get_hyps wpo in
  let hyps_minus = remove_elt hyps step in
  mk_annot_child {
      as_parent = wpo;
      as_tactic = t ;
      as_annot = annot;
      as_hypotheses = Some hyps_minus;
      as_goal = None;
    }

let remove_hypothesis_lemma t wpo lemma pred:Wpo.t =
  let hyps = get_assumptions lemma in
  let hyps_minus = remove_elt hyps pred in
  mk_lemma_child {
      ls_tactic = t;
      ls_parent = wpo;
      ls_goal = None;
      ls_hypotheses = Some hyps_minus;
      ls_lemma = lemma;
    }

let get_signature_in_string var =
  let name = String.trim (name_of_var var) in
  let sort = sort_of_var var in
  let sort_string = string_of_sort sort  in
  name ^ " : " ^ sort_string

let get_signature_reprs_in_string wpo =
  let vars_list = Lang.F.Vars.elements (get_vars wpo) in
  let vars_signatures  = List.map (get_signature_in_string) vars_list in
  vars_signatures

(* ------------------ *)
(* assert *)
(* ------------------ *)

let assert_lemma tn w t l =
  let new_hyps = l.Wpo.VC_Lemma.lemma.Definitions.l_assumptions @ [t] in
  mk_lemma_children w [{
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some t;
			  ls_hypotheses = None;
			  ls_lemma = l;
			};{
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = None;
			  ls_hypotheses = Some new_hyps;
			  ls_lemma = l;
			}]

let assert_annot tn w t l =
  let new_hyp = make_new_hypothesis t in
  let new_hyps = get_hyps w @ [new_hyp] in
  mk_annot_children w [{
			  as_tactic = tn;
			  as_parent = w;
			  as_goal = Some t;
			  as_hypotheses = None;
			  as_annot = l;
			};{
			  as_tactic = tn;
			  as_parent = w;
			  as_goal = None;
			  as_hypotheses = Some new_hyps;
			  as_annot = l;
			}]

let assert_ t wpo term =
  if sort term == Qed.Logic.Sbool || sort term == Qed.Logic.Sprop then
    match wpo.Wpo.po_formula with
    | GoalLemma l -> assert_lemma t wpo (p_bool term) l
    | GoalAnnot a -> assert_annot t wpo (p_bool term) a
    | _ -> raise
	     (Failure "assert not implemented yet for this type of formula")
  else
    raise (Failure "trying to assert a non-boolean term")

(* ------------------ *)
(* unfold *)
(* ------------------ *)

let rec subst_terms rel term =
  match repr term with
  | Fvar v -> List.assoc v rel
  | Fun(f, ts) -> e_fun f (List.map (subst_terms rel) ts)
  | Bvar (_i,_b) -> term (* TODO *)
  | Leq(a, b) -> e_prop (p_leq (subst_terms rel a) (subst_terms rel b))
  | True -> term
  | False -> term
  | Not p -> e_not (subst_terms rel p)
  | Add xs -> e_sum (List.map (subst_terms rel) xs)
  | Times(t, a) -> e_times t (subst_terms rel a)
  | Eq(a, b) -> e_eq (subst_terms rel a)
		     (subst_terms rel b)
  | Neq(a, b) -> e_neq (subst_terms rel a)
		       (subst_terms rel b)
  | Lt(a, b) -> e_lt (subst_terms rel a)
		     (subst_terms rel b)
  | If(a, b, c) -> e_if (subst_terms rel a)
			(subst_terms rel b)
			(subst_terms rel c)
  | And ts -> e_and (List.map (subst_terms rel) ts)
  | Or ts -> e_or (List.map (subst_terms rel) ts)
  | Bind (q, v, b) -> c_bind q v (subst_terms rel (lc_repr b))
  | Aget(a, b) -> e_get (subst_terms rel a)
			(subst_terms rel b)
  | Aset(a, b, c) -> e_set (subst_terms rel a)
			   (subst_terms rel b)
			   (subst_terms rel c)
  | Rget(term, v) -> e_getfield (subst_terms rel term) v
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (subst_terms rel) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = (subst_terms rel) p in
     let parsed_hs = List.map (subst_terms rel) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> term
  | Kreal _ -> term
  | Mul xs -> e_prod (List.map (subst_terms rel) xs)
  | Div(a, b) -> e_div (subst_terms rel a)
		       (subst_terms rel b)
  | Mod(a, b) -> e_mod (subst_terms rel a)
		       (subst_terms rel b)
  | Imply(hs, p) ->
     let parsed_hs = List.map (subst_terms rel) hs in
     let parsed_p = (subst_terms rel) p in
     e_imply parsed_hs parsed_p

let replace_fun_by_def dfun param_terms:Lang.F.term =
  let open Definitions in
  let param_vars = dfun.d_params in
  let rel = List.combine param_vars param_terms in
  match dfun.d_definition with
  | Logic _tau -> raise (Failure "Abstract function cannot be unfolded")
  | Value (_tau,_recursion,def) -> subst_terms rel def
  | Predicate (_recursion,def) -> subst_terms rel (e_prop def)
  | Inductive _lemmas -> raise (Failure "Lemma unfold not defined yet")

let rec unfold_in_term lfun dfunt term =
  match repr term with
  | Fun(f, ts) -> if f = lfun then (replace_fun_by_def dfunt ts)
		  else
		    e_fun f (List.map (unfold_in_term lfun dfunt) ts)
  | Bvar (_i,_b) -> term
  | Leq(a, b) ->
     e_prop (p_leq (unfold_in_term lfun dfunt a)
		   (unfold_in_term lfun dfunt b))
  | True -> term
  | False -> term
  | Not p -> e_not (unfold_in_term lfun dfunt p)
  | Add xs -> e_sum (List.map (unfold_in_term lfun dfunt) xs)
  | Times(t, a) -> e_times t (unfold_in_term lfun dfunt a)
  | Eq(a, b) -> e_eq (unfold_in_term lfun dfunt a)
		     (unfold_in_term lfun dfunt b)
  | Neq(a, b) -> e_neq (unfold_in_term lfun dfunt a)
		       (unfold_in_term lfun dfunt b)
  | Lt(a, b) -> e_lt (unfold_in_term lfun dfunt a)
		     (unfold_in_term lfun dfunt b)
  | If(a, b, c) -> e_if (unfold_in_term lfun dfunt a)
			(unfold_in_term lfun dfunt b)
			(unfold_in_term lfun dfunt c)
  | And ts -> e_and (List.map (unfold_in_term lfun dfunt)  ts)
  | Or ts -> e_or (List.map (unfold_in_term lfun dfunt) ts)
  | Bind (q, v, b) -> c_bind q v (unfold_in_term lfun dfunt (lc_repr b))
  | Aget(a, b) -> e_get (unfold_in_term lfun dfunt a)
			(unfold_in_term lfun dfunt b)
  | Aset(a, b, c) -> e_set (unfold_in_term lfun dfunt a)
			   (unfold_in_term lfun dfunt b)
			   (unfold_in_term lfun dfunt c)
  | Rget(term, v) -> e_getfield (unfold_in_term lfun dfunt term) v
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (unfold_in_term lfun dfunt) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = (unfold_in_term lfun dfunt) p in
     let parsed_hs = List.map (unfold_in_term lfun dfunt) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> term
  | Kreal _ -> term
  | Mul xs -> e_prod (List.map (unfold_in_term lfun dfunt) xs)
  | Div(a, b) -> e_div (unfold_in_term lfun dfunt a)
		       (unfold_in_term lfun dfunt b)
  | Mod(a, b) -> e_mod (unfold_in_term lfun dfunt a)
		       (unfold_in_term lfun dfunt b)
  | Fvar _ -> term
  | Imply(hs, p) ->
     let parsed_hs = List.map (unfold_in_term lfun dfunt) hs in
     let parsed_p = (unfold_in_term lfun dfunt) p in
     e_imply parsed_hs parsed_p

(**********)
(* unfold *)
(**********)

let find_fun_def model lfun = Definitions.find_function model lfun

let unfold_in_lemma tn w (lemma:Wpo.VC_Lemma.t) f =
  let model = w.Wpo.po_model in
  let dfun = find_fun_def model f in
  let goal = get_goal w in
  let new_goal = unfold_in_term f dfun (e_prop goal) in
  mk_lemma_child {
      ls_tactic = tn;
      ls_parent = w;
      ls_goal = Some (p_bool new_goal);
      ls_hypotheses = None;
      ls_lemma = lemma;
    }

let unfold_in_annot tn w (annot:Wpo.VC_Annot.t) f =
  let model = w.Wpo.po_model in
  let dfun = find_fun_def model f in
  let goal = get_goal w in
  let new_goal = unfold_in_term f dfun (e_prop goal) in
  mk_annot_child {
      as_tactic = tn;
      as_parent = w;
      as_goal = Some (p_bool new_goal);
      as_hypotheses = None;
      as_annot = annot;
    }

let unfold t wpo f =
  match wpo.Wpo.po_formula with
  | GoalLemma l -> unfold_in_lemma t wpo l f
  | GoalAnnot a -> unfold_in_annot t wpo a f
  | _ -> raise (Failure "unfold not implemented yet for this type of formula")


(**************)
(* unfold_all *)
(**************)

let unfold_all_in_lemma tn w (lemma:Wpo.VC_Lemma.t) f =
  let model = w.Wpo.po_model in
  let dfun = find_fun_def model f in
  let old_hyps = get_assumptions lemma in
  let new_hyps = List.map (fun h -> unfold_in_term f dfun h)
			  (e_props old_hyps) in
  let goal = get_goal w in
  let new_goal = unfold_in_term f dfun (e_prop goal) in
  mk_lemma_child {
      ls_tactic = tn;
      ls_parent = w;
      ls_goal = Some (p_bool new_goal);
      ls_hypotheses = Some (p_bools new_hyps);
      ls_lemma = lemma;
    }

let unfold_all_in_annot tn w (annot:Wpo.VC_Annot.t) f =
  let model = w.Wpo.po_model in
  let dfun = find_fun_def model f in
  let old_hyps = get_hyps w in
  let new_hyps = List.map
		   (fun h ->
		    let pred = get_predicate_from_step h in
		    let new_term = unfold_in_term f dfun (e_prop pred) in
		    make_hypothesis_from_another_hypothesis
		      h (p_bool new_term)
		   ) old_hyps in
  let goal = get_goal w in
  let new_goal = unfold_in_term f dfun (e_prop goal) in
  mk_annot_child {
      as_tactic = tn;
      as_parent = w;
      as_goal = Some (p_bool new_goal);
      as_hypotheses = Some new_hyps;
      as_annot = annot;
    }

let unfold_all t wpo f =
  match wpo.Wpo.po_formula with
  | GoalLemma l -> unfold_all_in_lemma t wpo l f
  | GoalAnnot a -> unfold_all_in_annot t wpo a f
  | _ -> raise (Failure "unfold not implemented yet for this type of formula")

(* ------------------ *)
(* nat_ind *)
(* ------------------ *)

let leq_ref_var reference_term var = Lang.F.e_leq reference_term (Lang.F.e_var var)

let fresh_var ?(basename="z") var vars =
  let pool = Lang.F.pool () in
  Lang.F.add_vars pool vars;
  let tau = Lang.F.tau_of_var var in
  let frvar = Lang.F.fresh pool ~basename tau in
  frvar

let base_case var term reference_term = replace_term (e_var var) reference_term term

let inductive_case w var term reference_term =
  let evar = e_var var in
  let vars = get_vars w in
  let bvar = fresh_var ~basename:"z" var vars in
  let tbvar = Lang.F.e_var bvar in
  let tbvarp1 = Lang.F.e_sum [tbvar;Lang.F.e_one] in
  let f1 = leq_ref_var reference_term bvar in
  let f2 = replace_term evar tbvar term in
  let f3 = replace_term evar tbvarp1 term in
  let impl = Lang.F.e_imply [f1;f2] f3 in
  let res = Lang.F.e_forall [bvar] impl in
  res

let nat_ind_lemma tn w lemma var reference_term =
  let pred = get_goal w in
  let p0 = leq_ref_var reference_term var in
  let p1 = base_case var (e_prop pred) reference_term in
  let p2 = inductive_case w var (e_prop pred) reference_term in
  mk_lemma_children w [
		      {
			ls_tactic = tn;
			ls_parent = w;
			ls_goal = Some (p_bool p0);
			ls_hypotheses = None;
			ls_lemma = lemma;
		      };
		      {
			ls_tactic = tn;
			ls_parent = w;
			ls_goal = Some (p_bool p1);
			ls_hypotheses = None;
			ls_lemma = lemma;
		      };{
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some (p_bool p2);
			  ls_hypotheses = None;
			  ls_lemma = lemma;
			}]

let nat_ind_annot tn w annot var reference_term =
  let pred = get_goal w in
  let p0 = leq_ref_var reference_term var in
  let p1 = base_case var (e_prop pred) reference_term in
  let p2 = inductive_case w var (e_prop pred) reference_term in
  mk_annot_children w [{
			  as_parent = w;
			  as_tactic = tn ;
			  as_annot = annot;
			  as_hypotheses = None ;
			  as_goal = Some (p_bool p0);
			};{
			  as_parent = w;
			  as_tactic = tn ;
			  as_annot = annot;
			  as_hypotheses = None;
			  as_goal = Some (p_bool p1);
			};{
			   as_parent = w;
			   as_tactic = tn ;
			   as_annot = annot;
			   as_hypotheses = None;
			   as_goal = Some (p_bool p2);
			 }]

let nat_ind t wpo var (reference_term:Lang.F.term option) =
  let ref_term = match reference_term with
    |None -> Lang.F.e_zero
    |Some t -> t
  in  
  match wpo.Wpo.po_formula with
  | GoalLemma l -> nat_ind_lemma t wpo l var ref_term
  | GoalAnnot a -> nat_ind_annot t wpo a var ref_term
  | _ -> raise
	   (Failure "nat_ind not implemented yet for this type of formula")

(* ------------------ *)
(* int_ind *)
(* ------------------ *)

let rec replace_term_in_sequence termReplaced newExp seq =
  let newSteps = List.map(replace_term_in_step termReplaced newExp) seq.Conditions.seq_list in
  { (seq) with seq_list = newSteps }

and replace_term_in_step termReplaced newExp step =
  let newConditions = replace_term_in_condition termReplaced newExp step.Conditions.condition in
  { (step) with condition = newConditions }

and replace_term_in_condition termReplaced newExp annot =
  match annot with
  | Type pred
  | Have pred
  | When pred
  | Core pred
  | Init pred -> Type (p_bool (replace_term termReplaced newExp (e_prop pred)))
  | Branch (pred,seq1,seq2) ->
     Branch (p_bool (replace_term termReplaced newExp (e_prop pred)),
	     replace_term_in_sequence termReplaced newExp seq1,
	     replace_term_in_sequence termReplaced newExp seq2)
  | Either seq_list ->
     Either (List.map (replace_term_in_sequence termReplaced newExp) seq_list)

let replace_term_in_lemma_hypotheses termReplaced newExp hyps =
  List.map (fun term -> replace_term termReplaced newExp term) hyps

let replace_term_in_annot_hypotheses termReplaced newExp hyps =
  List.map (fun annot -> replace_term_in_step termReplaced newExp annot) hyps

let base_case_int_ind_in_lemma varReplaced newExp term hyps =
  let termReplaced = (e_var varReplaced) in
  let replacedHyps = replace_term_in_lemma_hypotheses termReplaced newExp hyps  in
  let replacedGoal = replace_term termReplaced newExp term in
  (replacedHyps,replacedGoal)

let base_case_int_ind_in_annot varReplaced newExp term =
  let termReplaced = (e_var varReplaced) in
  let replacedHyps = replace_term_in_annot_hypotheses termReplaced newExp in
  let replacedGoal = replace_term termReplaced newExp term in
  (replacedHyps,replacedGoal)

let mk_leq_subterm term1 term2 = Lang.F.e_leq term1 term2

let mk_base_case hyps selectedTerm referenceTerm term =
  let replacedHypTerms = replace_term_in_lemma_hypotheses selectedTerm referenceTerm hyps in
  let replacedHyps = List.map(p_bool) replacedHypTerms in
  let replacedGoal = replace_term selectedTerm referenceTerm term in
  (replacedHyps,replacedGoal)

let mk_inductive_case hyps selectedTerm term =
  let plus_one_term = Lang.F.e_sum [selectedTerm;Lang.F.e_one] in
  let replacedGoal = replace_term selectedTerm plus_one_term term in
  let replacedHyps = List.map(p_bool) (hyps@[term]) in
  (replacedHyps,replacedGoal)

let int_ind tn w selectedVar (referenceTerm:Lang.F.term) =
  let term = e_prop (get_goal w) in
  let selectedTerm = Lang.F.e_var selectedVar in
  match w.Wpo.po_formula with
  | GoalLemma lemma -> (
    let hyp_preds = lemma.VC_Lemma.lemma.l_assumptions in
    let hyp_terms = List.map(e_prop) hyp_preds in

    let goal0 = mk_leq_subterm referenceTerm selectedTerm in
    let (hyps1,goal1) = mk_base_case hyp_terms selectedTerm referenceTerm term in
    let (hyps2,goal2) = mk_inductive_case hyp_terms selectedTerm term in
    mk_lemma_children w [
			{
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some (p_bool goal0);
			  ls_hypotheses = None;
			  ls_lemma = lemma;
			};
			{
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some (p_bool goal1);
			  ls_hypotheses = Some hyps1;
			  ls_lemma = lemma;
			};{
			    ls_tactic = tn;
			    ls_parent = w;
			    ls_goal = Some (p_bool goal2);
			    ls_hypotheses = Some hyps2;
			    ls_lemma = lemma;
			  }]
  )
  | _ -> raise (Failure "Int ind not implemented for others than Lemma")

(*---------*)
(* Rewrite *)
(*---------*)

module TermPairs =
  struct type t = term
         let compare x y = comparep (p_bool x) (p_bool y)
  end

module PairsMap = Map.Make(TermPairs)

let rec apply_subs subs term =
  match repr term with
  | Fvar _
  | Bvar _ -> (
    try let s = PairsMap.find term subs in s
    with Not_found -> term
  )
  | Leq(a, b) ->
     let parsed_a = apply_subs subs a in
     let parsed_b = apply_subs subs b in
     e_prop (p_leq parsed_a parsed_b)
  | True -> term
  | False -> term
  | Not p -> e_not (apply_subs subs p)
  | Add xs -> e_sum (List.map (apply_subs subs) xs)
  | Times(t, a) -> e_times t (apply_subs subs a)
  | Eq(a, b) -> e_eq (apply_subs subs a)
		     (apply_subs subs b)
  | Neq(a, b) -> e_neq (apply_subs subs a)
		       (apply_subs subs b)
  | Lt(a, b) -> e_lt (apply_subs subs a)
		     (apply_subs subs b)
  | If(a, b, c) ->
     let parsed_a = apply_subs subs a in
     let parsed_b = apply_subs subs b in
     let parsed_c = apply_subs subs c in
     e_if parsed_a parsed_b parsed_c
  | And ts -> e_and (List.map (apply_subs subs) ts)
  | Or ts -> e_or (List.map (apply_subs subs) ts)
  | Fun(f, ts) ->
     let parsed_ts = List.map (apply_subs subs) ts in
     e_fun f parsed_ts
  | Bind (_q, _v, _b) -> term
  | Aget(a, b) ->
     let parsed_a = apply_subs subs a in
     let parsed_b = apply_subs subs b in
     e_get parsed_a parsed_b
  | Aset(a, b, c) ->
     let parsed_a = apply_subs subs a in
     let parsed_b = apply_subs subs b in
     let parsed_c = apply_subs subs c in
     e_set parsed_a parsed_b parsed_c
  | Rget(term, v) ->
     let parsed_term = apply_subs subs term in
     e_getfield parsed_term v
  | Rdef fts ->
     let (vars, terms) = List.split fts in
     let parsed_terms = List.map (apply_subs subs) terms in
     let combined_tuple = List.combine vars parsed_terms in
     e_record combined_tuple
  | Apply(p, hs) ->
     let parsed_p = apply_subs subs p in
     let parsed_hs = List.map (apply_subs subs) hs in
     e_apply parsed_p parsed_hs
  | Kint _ -> term
  | Kreal _ -> term
  | Mul xs -> e_prod (List.map (apply_subs subs) xs)
  | Div(a, b) -> e_div (apply_subs subs a)
		       (apply_subs subs b)
  | Mod(a, b) -> e_mod (apply_subs subs a)
		       (apply_subs subs b)
  | Imply(hs, p) ->
     let parsed_hs = List.map (apply_subs subs) hs in
     let parsed_p = apply_subs subs p in
     e_imply parsed_hs parsed_p

let vequal x y = if Lang.F.Var.compare x y = 0 then true else false

let occurs_check (x:var) (t:term) subs  =
  (*let S be a stack, *)
  let stack = Stack.create () in
  (* initially containting t *)
  Stack.push t stack;
  (*while (S is non-empty do*)
  while not (Stack.is_empty stack) do
    (* t := pop (S) *)
    let t = Stack.pop stack in
    (* foreach variably y in t do*)
    let vars = varsp (p_bool t) in
    Lang.F.Vars.iter
      ( fun y ->
	(*if x = y then return false *)
	if vequal y x then raise (Failure "Failed in occurs_check")
	else let yterm = Lang.F.e_var y in
	     if PairsMap.exists (
		    (* if y is bound to subs then push ysubs onto S *)
		    fun a b -> if equal yterm a then (
				 Stack.push (apply_subs subs b) stack;true)
			       else false
		  ) subs then ()) vars;
  done;
  subs

(*In this method, "s" is subterm of lemma and "t" the subterm of goal*)
let evaluate (s:Lang.F.term) (t:Lang.F.term) (stack:(Lang.F.term * Lang.F.term) Stack.t)
	     subs:((Lang.F.term * Lang.F.term) Stack.t * Lang.F.term PairsMap.t) =
  try
    match repr s,repr t with
    | Fvar _, Fvar _
    | Bvar _, Fvar _
    | Bvar _, Bvar _
    | Fvar _, Bvar _ -> (stack,PairsMap.add s t subs)
    | Fvar a, _any -> let subs =  occurs_check a t subs in
		      (stack,PairsMap.add s t subs)
    | _any,Fvar a -> let subs = occurs_check a t subs in
		     (stack,PairsMap.add s t subs)
    | Leq(a1, a2),Leq(b1,b2) -> Stack.push (a1,b1) stack;
				Stack.push (a2,b2) stack;
				(stack,subs)
    | Fun(f1, ts1),Fun(f2,ts2) ->
       if Lang.F.Fun.equal f1 f2 then(
	 List.iter2 (fun t1 t2 -> Stack.push (t1,t2) stack) ts1 ts2;
	 (stack,subs)
       ) else
	 raise (Failure "functions are not equal")
    | True,True -> (stack,subs)
    | False,False -> (stack,subs)
    | Not a,Not b -> Stack.push (a,b) stack;
		     (stack,subs)
    | Add tl1,Add tl2 -> List.iter2 (fun a b -> Stack.push (a,b) stack) tl1 tl2;
			 (stack,subs)
    | Times(t1,a1),Times(t2,a2) ->
       (*TODO: verify if this implementation is correct *)
       if Lang.F.Z.equal t1 t2 then  (
	 Stack.push (a1,a2) stack;
	 (stack,subs)
       )
       else raise (Failure "")
    | Eq(a1, b1),Eq(a2,b2) -> Stack.push (a1,a2) stack;
			      Stack.push (b1,b2) stack;
			      (stack,subs)
    | Neq(a1, b1),Neq(a2,b2) -> Stack.push (a1,a2) stack;
				Stack.push (b1,b2) stack;
				(stack,subs)
    | Lt(a1, b1),Lt(a2,b2)  -> Stack.push (a1,a2) stack;
			       Stack.push (b1,b2) stack;
			       (stack,subs)
    | If(a1, b1, c1),If(a2, b2, c2) ->
       Stack.push (a1,a2) stack;
       Stack.push (b1,b2) stack;
       Stack.push (c1,c2) stack;
       (stack,subs)
    | And tl1,And tl2 -> List.iter2 (fun a b -> Stack.push (a,b) stack) tl1 tl2;
			 (stack,subs)
    | Or tl1,Or tl2 -> List.iter2 (fun a b -> Stack.push (a,b) stack) tl1 tl2;
		       (stack,subs)
    | Bind (_q1, _v1, _b1),Bind (_q2, _v2, _b2) ->
       raise (Failure "Unification not yet implemented for quantifiers")
    | Aget(a1, a2),Aget(b1,b2) -> Stack.push (a1,b1) stack;
				  Stack.push (a2,b2) stack;
				  (stack,subs)
    | Aset(a1, a2, a3),Aset(b1,b2,b3) ->
       Stack.push (a1,b1) stack;
       Stack.push (a2,b2) stack;
       Stack.push (a3,b3) stack;
       (stack,subs)
    | Rget(t1, t2),Rget(v1,v2) ->
       (* TODO: verify implementation of Rget *)
       if Lang.F.Field.equal t2 v2 then (
	 Stack.push (t1,v1) stack;
	 (stack,subs)
       ) else
	 raise (Failure ("fields of Rget are not the same"))
    | Rdef ft1,Rdef ft2 ->
       (* TODO: verify implementation of Rdef *)
       List.iter2
	 (fun (f1,t1) (f2,t2) ->
	  if Lang.F.Field.equal f1 f2 then
	    Stack.push (t1,t2) stack
	  else raise (Failure "fields of Rdef are not the same")
	 ) ft1 ft2;
       (stack,subs)
    | Apply(p1, hs1),Apply(p2,hs2) ->
       Stack.push (p1,p2) stack;
       List.iter2(fun t1 t2 -> Stack.push (t1,t2) stack) hs1 hs2;
       (stack,subs)
    | Kint _ , Kint _ -> (stack,subs)
    | Kreal _ , Kreal _ -> (stack,subs)
    | Mul xs1,Mul xs2 -> List.iter2 (fun t1 t2 -> Stack.push (t1,t2) stack) xs1 xs2;
			 (stack,subs)
    | Div (a1, a2),Div(b1,b2) -> Stack.push (a1,b1) stack;
				 Stack.push (a2,b2) stack;
				 (stack,subs)
    | Mod(a1,a2), Mod(b1,b2) -> Stack.push (a1,b1) stack;
				Stack.push (a2,b2) stack;
				(stack,subs)
    | Imply(hs1, p1),Imply(hs2,p2) ->
       List.iter2 (fun t1 t2 -> Stack.push (t1,t2) stack) hs1 hs2;
       Stack.push (p1,p2) stack;
       (stack,subs)
    | _,_ -> raise (Failure ("Symbols " ^ (string_of_term s) ^ " and " ^ (string_of_term t)  ^
			       " are not equals"))
  with Failure m -> raise (Failure m)

let rec unify (s:term) (t:term) =
  let stack = ref (Stack.create ()) in
  Stack.push (s,t) !stack;
  let subs = ref PairsMap.empty in
  while not (Stack.is_empty !stack) do
    let ((s:Lang.F.term),(t:Lang.F.term)) = Stack.pop !stack in
    let s = apply_subs !subs s in
    let t = apply_subs !subs t in
    if not (equal s t) then
      try
	let (st,su) = evaluate s t !stack !subs in
	stack := st;
	subs := su
      with Failure x ->
	raise (Failure x)
  done;
  !subs

let rec analyze_rewriting_term ?forall_vars:(forall_vars=[]) ?deps:(deps=[]) term  =
  match repr term with
  | Bind (q, v, b) -> (
    match q with
    |Forall -> let forall_vars = forall_vars @ [v] in
	       analyze_rewriting_term ~forall_vars ~deps (lc_repr b)
    | _ -> analyze_rewriting_term ~forall_vars ~deps (lc_repr b)
  )
  | Imply(hs, p) ->
     let deps = deps @ hs in
     analyze_rewriting_term ~forall_vars ~deps p
  | Eq(a, b) -> (a,b,forall_vars,deps)
  | _ -> raise (Failure "No equality found")

let find_lemma model lem_name:Definitions.dlemma =
  try Definitions.find_lemma_by_name model lem_name
  with Not_found -> raise (Failure ("Lemma " ^ lem_name ^ " was not found"))

let no_match_msg = "Rewrite: no match"

let is_universally_quantified el quant_vars = List.exists (fun q -> q = el) quant_vars

(* matc is the subterm of lemma and term is the subterm of the goal *)
let rec check_match  ~quant_vars ~(matc:term) ~(term:term):bool =
  match repr matc,repr term with
  | Bvar (_i,b),_any ->
     if is_universally_quantified b quant_vars then(
       if sort term = sort matc then(
	 true
       )else(
	 false
       )
     )
     else(
       false
     )
  | Fvar v1, Fvar v2 ->
     v1 = v2
  | Fun(f1,ts1),Fun(f2,ts2) -> Lang.F.Fun.equal f1 f2 &&
				 List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
  | Leq (a1,a2),Leq(b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
				check_match ~matc:a2 ~term:b2 ~quant_vars
  | True,True -> true
  | False,False -> true
  | Not p1,Not p2 -> check_match ~matc:p1 ~term:p2 ~quant_vars
  | Add ts1,Add ts2 ->
     List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
  | Times(t1, a1),Times (t2,a2) ->
     t1 = t2 && check_match ~matc:a1 ~term:a2 ~quant_vars
  | Eq(a1, a2),Eq(b1,b2) ->  check_match ~quant_vars ~matc:a1 ~term:b1 &&
			       check_match ~matc:a2 ~term:b2 ~quant_vars
  | Neq(a1,a2),Neq(b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
			       check_match ~matc:a2 ~term:b2 ~quant_vars
  | Lt(a1, a2),Lt(b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
			      check_match ~matc:a2 ~term:b2 ~quant_vars
  | If(a1, a2, a3),If(b1,b2,b3) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
				     check_match ~matc:a2 ~term:b2 ~quant_vars &&
				       check_match ~matc:a3 ~term:b3 ~quant_vars
  | And ts1,And ts2 ->
     List.for_all2 (fun t1 t2 -> check_match ~matc:t1 ~term:t2 ~quant_vars) ts1 ts2
  | Or ts1,Or ts2 ->
     List.for_all2 (fun t1 t2 -> check_match ~matc:t1 ~term:t2 ~quant_vars) ts1 ts2
  | Bind (a1, a2, a3),Bind (b1,b2,b3) ->
     a1 = b1 && a2 = b2 && check_match ~matc:(lc_repr a3) ~term:(lc_repr b3) ~quant_vars
  | Aget(a1, a2),Aget(b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
				  check_match ~matc:a2 ~term:b2 ~quant_vars
  | Aset(a1, a2, a3),Aset(b1,b2,b3) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
					 check_match ~matc:a2 ~term:b2 ~quant_vars &&
					   check_match ~matc:a3 ~term:b3 ~quant_vars
  | Rget(t1, v1),Rget(t2,v2) -> v1 = v2 && check_match ~matc:t1 ~term:t2 ~quant_vars
  | Rdef fts1,Rdef fts2 ->
     let (fs1, ts1) = List.split fts1 in
     let (fs2, ts2) = List.split fts2 in
     List.for_all2 (fun v1 v2 -> v1 = v2)fs1 fs2 &&
       List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
  | Apply(p1, hs1),Apply (p2,hs2) ->
     List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) (p1::hs1) (p2::hs2)
  | Kint x1,Kint x2 -> x1 = x2
  | Kreal x1,Kreal x2 -> x1 = x2
  | Mul ts1,Mul ts2 ->
     List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
  | Div(a1, a2),Div (b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
				 check_match ~matc:a2 ~term:b2 ~quant_vars
  | Mod(a1, a2),Mod(b1,b2) -> check_match ~quant_vars ~matc:a1 ~term:b1 &&
				check_match ~matc:a2 ~term:b2 ~quant_vars
  | Imply(hs1, p1), Imply (hs2,p2) ->
     List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) (hs1@[p1]) (hs2@[p2])
  | _,_ -> false

let no_match_msg a b =
  "(" ^ (string_of_term a) ^ ") and (" ^ (string_of_term b) ^ ") didn't match"


let rec match_sub_term (matc:term) (term:term) quant_vars =
  match repr matc,repr term with
  | Bvar (_i,b),_any ->
     if is_universally_quantified b quant_vars && sort term = sort matc then term
     else match_sub_term_aux matc term quant_vars
  | Fvar v1, Fvar v2 -> if v1 = v2 then term else raise (Failure (no_match_msg matc term))
  | Fun(f1,ts1),Fun(f2,ts2) ->
     if Lang.F.Fun.equal f1 f2 then (
       if List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2 then term
       else match_sub_term_aux matc term quant_vars
     )else match_sub_term_aux matc term quant_vars
  | Leq (a1,a2),Leq(b1,b2) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
				   check_match ~matc:a2 ~term:b2 ~quant_vars then term
			      else match_sub_term_aux matc term quant_vars
  | True,True -> term
  | False,False -> term
  | Not p1,Not p2 -> if check_match ~matc:p1 ~term:p2 ~quant_vars then term else
		       match_sub_term_aux matc term quant_vars
  | Add ts1,Add ts2 ->
     if (List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2)
     then term else match_sub_term_aux matc term quant_vars
  | Times(t1, a1),Times (t2,a2) ->
     if t1 = t2 && check_match ~matc:a1 ~term:a2 ~quant_vars then term
     else match_sub_term_aux matc term quant_vars
  | Eq(a1, a2),Eq(b1,b2) ->  if check_match ~quant_vars ~matc:a1 ~term:b1 &&
				  check_match ~matc:a2 ~term:b2 ~quant_vars then term
			     else match_sub_term_aux matc term quant_vars
  | Neq(a1,a2),Neq(b1,b2) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
				  check_match ~matc:a2 ~term:b2 ~quant_vars then term
			     else match_sub_term_aux matc term quant_vars
  | Lt(a1, a2),Lt(b1,b2) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
				 check_match ~matc:a2 ~term:b2 ~quant_vars then term
			    else 	       match_sub_term_aux matc term quant_vars
  | If(a1, a2, a3),If(b1,b2,b3) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
					check_match ~matc:a2 ~term:b2 ~quant_vars &&
					  check_match ~matc:a3 ~term:b3 ~quant_vars
				   then term
				   else match_sub_term_aux matc term quant_vars
  | And ts1,And ts2 ->
     if List.for_all2 (fun t1 t2 -> check_match ~matc:t1 ~term:t2 ~quant_vars) ts1 ts2
     then term else 	       match_sub_term_aux matc term quant_vars
  | Or ts1,Or ts2 ->
     if List.for_all2 (fun t1 t2 -> check_match ~matc:t1 ~term:t2 ~quant_vars) ts1 ts2
     then term else 	       match_sub_term_aux matc term quant_vars
  | Bind (a1, a2, a3),Bind (b1,b2,b3) ->
     if a1 = b1 && a2 = b2 && check_match ~matc:(lc_repr a3) ~term:(lc_repr b3) ~quant_vars
     then term else 	       match_sub_term_aux matc term quant_vars
  | Aget(a1, a2),Aget(b1,b2) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
				     check_match ~matc:a2 ~term:b2 ~quant_vars then term
				else	       match_sub_term_aux matc term quant_vars
  | Aset(a1, a2, a3),Aset(b1,b2,b3) -> if check_match ~quant_vars ~matc:a1 ~term:b1 &&
					    check_match ~matc:a2 ~term:b2 ~quant_vars &&
					      check_match ~matc:a3 ~term:b3 ~quant_vars then term
				       else 	 match_sub_term_aux matc term quant_vars
  | Rget(t1, v1),Rget(t2,v2) ->
     if v1 = v2 && check_match ~matc:t1 ~term:t2 ~quant_vars then term
     else 	       match_sub_term_aux matc term quant_vars
  | Rdef fts1,Rdef fts2 ->
     let (fs1, ts1) = List.split fts1 in
     let (fs2, ts2) = List.split fts2 in
     if List.for_all2 (fun v1 v2 -> v1 = v2)fs1 fs2 &&
	  List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
     then term else 	       match_sub_term_aux matc term quant_vars
  | Apply(p1, hs1),Apply (p2,hs2) ->
     if List.for_all2
	  (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) (p1::hs1) (p2::hs2) then
       term else 	       match_sub_term_aux matc term quant_vars
  | Kint x1,Kint x2 -> if x1 = x2 then term else match_sub_term_aux matc term quant_vars
  | Kreal x1,Kreal x2 -> if x1 = x2 then match_sub_term_aux matc term quant_vars
			 else match_sub_term_aux matc term quant_vars
  | Mul ts1,Mul ts2 ->
     if List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2) ts1 ts2
     then term else match_sub_term_aux matc term quant_vars
  | Div(a1, a2),Div (b1,b2) ->
     if check_match ~quant_vars ~matc:a1 ~term:b1 && check_match ~matc:a2 ~term:b2 ~quant_vars
     then term else match_sub_term_aux matc term quant_vars
  | Mod(a1, a2),Mod(b1,b2) ->
     if check_match ~quant_vars ~matc:a1 ~term:b1 && check_match ~matc:a2 ~term:b2 ~quant_vars
     then term else match_sub_term_aux matc term quant_vars
  | Imply(hs1, p1), Imply (hs2,p2) ->
     if List.for_all2 (fun t1 t2 -> check_match ~quant_vars ~matc:t1 ~term:t2)
		      (hs1@[p1]) (hs2@[p2]) then term
     else match_sub_term_aux matc term quant_vars
  | _,_ -> match_sub_term_aux matc term quant_vars

and match_sub_term_in_sequence matc (terms:Lang.F.term list) quant_vars = (
  match terms with
  |[] -> raise (Failure "No match")
  |term::others -> try match_sub_term matc term quant_vars
		   with Failure _ -> match_sub_term_in_sequence matc others quant_vars
)

and match_sub_term_aux matc term quant_vars =
  match repr term with
  | Bind (_, _, b) -> match_sub_term matc (lc_repr b) quant_vars
  | Fvar _ -> raise (Failure (no_match_msg matc term))
  | Fun(_, ts) -> match_sub_term_in_sequence matc ts quant_vars
  | Bvar (_i,_b) -> raise (Failure (no_match_msg matc term))
  | Leq(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | True -> raise (Failure (no_match_msg matc term))
  | False -> raise (Failure (no_match_msg matc term))
  | Not p -> match_sub_term_in_sequence matc [p] quant_vars
  | Add xs -> match_sub_term_in_sequence matc xs quant_vars
  | Times(_, a) -> match_sub_term_in_sequence matc [a] quant_vars
  | Eq(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | Neq(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | Lt(a, b) ->  match_sub_term_in_sequence matc [a;b] quant_vars
  | If(a, b, c) ->  match_sub_term_in_sequence matc [a;b;c] quant_vars
  | And xs -> match_sub_term_in_sequence matc xs quant_vars
  | Or xs -> match_sub_term_in_sequence matc xs quant_vars
  | Aget(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | Aset(a, b, c) -> match_sub_term_in_sequence matc [a;b;c] quant_vars
  | Rget(t, _) -> match_sub_term_in_sequence matc [t] quant_vars
  | Rdef fts ->
     let (_, terms) = List.split fts in
     match_sub_term_in_sequence matc terms quant_vars
  | Apply(_, hs) -> match_sub_term_in_sequence matc hs quant_vars
  | Kint _ -> raise (Failure (no_match_msg matc term))
  | Kreal _ -> raise (Failure (no_match_msg matc term))
  | Mul xs -> match_sub_term_in_sequence matc xs quant_vars
  | Div(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | Mod(a, b) -> match_sub_term_in_sequence matc [a;b] quant_vars
  | Imply(xs, p) -> match_sub_term_in_sequence matc (xs@[p]) quant_vars

let turn_fvars_into_bvars term =
  let vars = varsp term in
  let varlist = Vars.elements vars in
  let new_term = Lang.F.p_forall varlist term in
  new_term

let rewrite_lemma_selected_subterm tn w lem_name direction selected_term:Wpo.t list =
  let termo = get_goal w in
  if is_subterm selected_term (e_prop termo) then
    let d_lemma = find_lemma w.Wpo.po_model lem_name  in
    let old_lem_term = d_lemma.Definitions.l_lemma in
    let lem_term = turn_fvars_into_bvars old_lem_term in
    let (left,right,forall_vars,dep_terms) = analyze_rewriting_term (e_prop lem_term) in
    let (before,after) = match direction with
      | Right -> (left,right)
      | Left -> (right,left)
    in
    let (matched_sub_term:term) = match_sub_term before selected_term forall_vars in
    let (subs: term PairsMap.t) = unify before matched_sub_term in
    let substituted_equivalent_term = apply_subs subs after in
    let replaced_main_term = replace_term matched_sub_term
					  substituted_equivalent_term
					  (e_prop termo) in
    let dependencies = List.map (apply_subs subs) dep_terms in
    match w.Wpo.po_formula with
    | GoalLemma lemma ->
       let wpos = List.map (
		      fun t -> {
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some t;
			  ls_hypotheses = None;
			  ls_lemma = lemma;
			})(p_bools (replaced_main_term::dependencies)) in
       mk_lemma_children w wpos
    | GoalAnnot annot ->
       let wpos = List.map (
		      fun t -> {
			  as_tactic = tn;
			  as_parent = w;
			  as_goal = Some t;
			  as_hypotheses = None;
			  as_annot = annot;
			})(p_bools (replaced_main_term::dependencies)) in
       mk_annot_children w wpos
    | _ -> raise (Failure "rewrite not implemented yet for Check formula")
  else
    raise (Failure "Selected term is not subterm of the goal")

(*
let rewrite_lemma_no_selected_subterm tn w lem_name direction:Wpo.t list =
  let term = get_goal w in
  let d_lemma = find_lemma w.Wpo.po_model lem_name  in
  let lem_term = d_lemma.Definitions.l_lemma in
  let (left,right,forall_vars,dep_terms) = analyze_rewriting_term (e_prop lem_term) in
  let (before,after) = match direction with
    | Left -> (left,right)
    | Right -> (right,left)
  in
     let (matched_sub_term:term) = match_sub_term before (e_prop term) forall_vars in
     let (subs: term PairsMap.t) = unify matched_sub_term before in
     let substituted_equivalent_term = apply_subs subs after in
     let replaced_main_term = replace_term matched_sub_term
					   substituted_equivalent_term
					   (e_prop term) in
     let dependencies = List.map (apply_subs subs) dep_terms in
  match w.Wpo.po_formula with
  | GoalLemma lemma ->
     let wpos = List.map (
		    fun t -> {
			ls_tactic = tn;
			ls_parent = w;
			ls_goal = Some t;
			ls_hypotheses = None;
			ls_lemma = lemma;
		  })(p_bools (replaced_main_term::dependencies)) in
     mk_lemma_children w wpos
  | GoalAnnot annot ->
     let wpos = List.map (
		    fun t -> {
			as_tactic = tn;
			as_parent = w;
			as_goal = Some t;
			as_hypotheses = None;
			as_annot = annot;
		  })(p_bools (replaced_main_term::dependencies)) in
     mk_annot_children w wpos
  | _ -> raise (Failure "rewrite not implemented yet for Check formula")
 *)

let rewrite_lemma tn w lem_name direction selected_term:Wpo.t list =
  match selected_term with
  | None -> raise (Failure "rewrite_lemma_no_selected_subterm not yet implemented" )
  (*rewrite_lemma_no_selected_subterm tn w lem_name direction*) (*TODO*)
  | Some st -> rewrite_lemma_selected_subterm tn w lem_name direction st

(***************)
(* Rewrite_hyp *)
(***************)

(*
let rewrite_hyp_no_selected_sub_term tn w index direction:Wpo.t list =
  let rew_term =
    (match w.Wpo.po_formula with
     |GoalAnnot _ ->
       let hyps = get_hyps w in
       let hyp = List.nth hyps index in
       get_predicate_from_step hyp
     |GoalLemma lemma ->
       let hyps = get_assumptions lemma in
       List.nth hyps index
     |_ -> raise (Failure ("rewrite_hyp is not implemented for this type of formula"))
    ) in
  let term = get_goal w in
  let (left,right,forall_vars,dep_terms) = analyze_rewriting_term
					     (e_prop rew_term) in
  let (before,after) = match direction with
    | Left -> (left,right)
    | Right -> (right,left)
  in
  let (matched_sub_term:term) = match_sub_term before (e_prop term)
					       forall_vars in
  let (subs: term PairsMap.t) = unify matched_sub_term (e_prop rew_term) in
     let substituted_equivalent_term = apply_subs subs after in
     let replaced_main_term = replace_term (e_prop term) matched_sub_term
					   substituted_equivalent_term in
     let dependencies = List.map (apply_subs subs) dep_terms in
  match w.Wpo.po_formula with
  | GoalLemma lemma ->
     let wpos = List.map (
		    fun t -> {
			ls_tactic = tn;
			ls_parent = w;
			ls_goal = Some t;
			ls_hypotheses = None;
			ls_lemma = lemma;
		  })(p_bools (replaced_main_term::dependencies)) in
     mk_lemma_children w wpos
  | GoalAnnot annot ->
     let wpos = List.map (
		    fun t -> {
			as_tactic = tn;
			as_parent = w;
			as_goal = Some t;
			as_hypotheses = None;
			as_annot = annot;
		  })(p_bools (replaced_main_term::dependencies)) in
     mk_annot_children w wpos
  | _ -> raise (Failure "rewrite not implemented yet for this type of formula")
 *)

let rewrite_hyp_selected_subterm tn w index direction selected_term:Wpo.t list =
  let rew_term =
    (match w.Wpo.po_formula with
     |GoalAnnot _ ->
       let hyps = get_hyps w in
       let hyp = List.nth hyps index in
       get_predicate_from_step hyp
     |GoalLemma lemma ->
       let hyps = get_assumptions lemma in
       List.nth hyps index
     |_ -> raise (Failure ("rewrite_hyp is not implemented for GoalCheck"))
    ) in
  let term = get_goal w in
  if is_subterm selected_term (e_prop term) then
    let (left,right,forall_vars,dep_terms) = analyze_rewriting_term (e_prop rew_term) in
    let (before,after) = match direction with
      | Right -> (left,right)
      | Left -> (right,left)
    in
    let (matched_sub_term:term) = match_sub_term before selected_term forall_vars in
    let (subs: term PairsMap.t) = unify before matched_sub_term in
    let substituted_equivalent_term = apply_subs subs after in
    let replaced_main_term = replace_term matched_sub_term
					  substituted_equivalent_term
					  (e_prop term) in
    let dependencies = List.map (apply_subs subs) dep_terms in
    match w.Wpo.po_formula with
    | GoalLemma lemma ->
       let wpos = List.map (
		      fun t -> {
			  ls_tactic = tn;
			  ls_parent = w;
			  ls_goal = Some t;
			  ls_hypotheses = None;
			  ls_lemma = lemma;
			})(p_bools (replaced_main_term::dependencies)) in
       mk_lemma_children w wpos
    | GoalAnnot annot ->
       let wpos = List.map (
		      fun t -> {
			  as_tactic = tn;
			  as_parent = w;
			  as_goal = Some t;
			  as_hypotheses = None;
			  as_annot = annot;
			})(p_bools (replaced_main_term::dependencies)) in
       mk_annot_children w wpos
    | _ -> raise (Failure "rewrite not implemented yet for GoalCheck")
  else
    raise (Failure "Selected term is not subterm of the goal")

let rewrite_hyp tn w index direction selected_term:Wpo.t list =
  match selected_term with
  | None -> raise (Failure "rewrite_hyp_no_selected_subterm not yet implemented" )
  (*rewrite_lemma_no_selected_subterm tn w lem_name direction*) (*TODO*)
  | Some st -> rewrite_hyp_selected_subterm tn w index direction st

(***********)
(* Replace *)
(***********)

let rec contains_quantified_var t =
  match repr t with
  | Bind _ -> true
  | Fvar _ -> false
  | Fun(_, ts) -> List.exists (contains_quantified_var) ts
  | Bvar (_i,_b) -> true
  | Leq(a, b) -> contains_quantified_var a || contains_quantified_var b
  | True -> false
  | False -> false
  | Not a -> contains_quantified_var a
  | Add ts -> List.exists (contains_quantified_var) ts
  | Times(_, a) -> contains_quantified_var a
  | Eq(a, b) -> contains_quantified_var a || contains_quantified_var b
  | Neq(a, b) -> contains_quantified_var a || contains_quantified_var b
  | Lt(a, b) ->  contains_quantified_var a || contains_quantified_var b
  | If(a, b, c) ->  contains_quantified_var a || contains_quantified_var b
		    || contains_quantified_var c
  | And ts -> List.exists (contains_quantified_var) ts
  | Or ts -> List.exists (contains_quantified_var) ts
  | Aget(a, b) -> contains_quantified_var a || contains_quantified_var b
  | Aset(a, b, c) -> contains_quantified_var a || contains_quantified_var b
		     || contains_quantified_var c
  | Rget(t, _) -> contains_quantified_var t
  | Rdef fts ->
     let (_, ts) = List.split fts in List.exists (contains_quantified_var) ts
  | Apply(_, ts) ->  List.exists (contains_quantified_var) ts
  | Kint _ -> false
  | Kreal _ -> false
  | Mul ts -> List.exists (contains_quantified_var) ts
  | Div(a, b) -> contains_quantified_var a || contains_quantified_var b
  | Mod(a, b) -> contains_quantified_var a || contains_quantified_var b
  | Imply(ts, a) -> List.exists (contains_quantified_var) (ts@[a])

let replace tn w t1 t2:Wpo.t list =
  let goal = get_goal w in
  if not (contains_quantified_var t1) && not (contains_quantified_var t2) then
    let new_goal1 = replace_term t1 t2 (e_prop goal) in
    let new_goal2 = p_equal t1 t2 in
    match w.Wpo.po_formula with
    | GoalLemma lemma ->
       mk_lemma_children w [{
			       ls_tactic = tn;
			       ls_parent = w;
			       ls_goal = Some (p_bool new_goal1);
			       ls_hypotheses = None;
			       ls_lemma = lemma;
			     };{
			       ls_tactic = tn;
			       ls_parent = w;
			       ls_goal = Some new_goal2;
			       ls_hypotheses = None;
			       ls_lemma = lemma;
			     }]
    | GoalAnnot annot ->
       mk_annot_children w [{
			       as_tactic = tn;
			       as_parent = w;
			       as_goal = Some (p_bool new_goal1);
			       as_hypotheses = None;
			       as_annot = annot;
			     };{
			       as_tactic = tn;
			       as_parent = w;
			       as_goal = Some new_goal2;
			       as_hypotheses = None;
			       as_annot = annot;
			     }]
    | _ -> raise (Failure "Not implemented for this type of PO")
  else
    raise (Failure "Quantified variables is not allowed in this tactic")

(***********)
(* To goal *)
(***********)

let not_implemented_type = "Not implemented for this type of PO"

let to_goal tn i w =
  match w.Wpo.po_formula with
  |GoalLemma lemma ->
    let hyps = get_assumptions lemma in
    let selected_hyp = List.nth hyps i in
    let new_hyps = List.filter (fun f -> if f == selected_hyp then false else true) hyps in
    let goal = get_goal w in
    let new_goal = p_imply selected_hyp goal in
    mk_lemma_child {
	ls_tactic = tn;
	ls_parent = w;
	ls_goal = Some new_goal;
	ls_hypotheses = Some new_hyps;
	ls_lemma = lemma;
      }
  |GoalAnnot annot ->
    let hyps = get_hyps w in
    let selected_hyp = List.nth hyps i in
    let new_hyps = List.filter (fun f -> if f == selected_hyp then false else true) hyps in
    let goal = get_goal w in
    let left = get_predicate_from_step selected_hyp in
    let new_goal = p_imply left goal in
    mk_annot_child {
	as_tactic = tn;
	as_parent = w;
	as_goal = Some new_goal;
	as_hypotheses = Some new_hyps;
	as_annot = annot;
      }
  | _ -> raise (Failure not_implemented_type)

(* ----------------- *)
(* Tactic executions *)
(* ----------------- *)

let execute_inf_rule ~po_gid t :Wpo.t =
  let tactic = {id=po_gid;tactic=t} in
  (*  add_tactic tactic; *)
  let po_gid = tactic.id in
  let wpo = get_wpo_by_po_gid po_gid in
  match tactic.tactic with
  | UnfoldAll f -> unfold_all t wpo f
  | To_Goal i -> to_goal t i wpo
  | Replace (t1,t2) -> List.nth (replace t wpo t1 t2) 0
  | Rewrite_Lemma (lem_name,direction,selected_term) ->
     List.nth  (rewrite_lemma t wpo lem_name direction selected_term) 0
  | Rewrite_Hyp (hyp_index,direction,selected_term) ->
     List.nth  (rewrite_hyp t wpo hyp_index direction selected_term) 0
  | Nat_ind (v,ot) -> List.nth (nat_ind t wpo v ot) 0
  | Int_ind (v1,e1) -> List.nth (int_ind t wpo v1 e1) 0
  | Unfold f -> unfold t wpo f
  | Assert a -> List.nth (assert_ t wpo a) 0
  | Use_equality_in_hypothesis (ot,st,cond) -> use_equality_in_hypothesis t wpo ot st cond
  |Modus_ponens_hyps index -> modus_ponens_hyp t wpo index
  |Intro -> intro t wpo
  |Apply_hypothesis_in_goal (index) -> apply_hypothesis_in_goal t wpo index
  |Proof_by_cases (term) -> List.nth (proof_by_cases t wpo term) 0
  |Instantiate_universal_quantifier_hyps (index,term) ->
    instantiate_universal_quantifier_hypothesis t wpo index term
  |Instantiate_existential_quantifier_goal (term) ->
    instantiate_existential_quantifier_in_goal t wpo term
  |Rewrite_if_then_else_goal -> rewrite_if_then_else_goal t wpo
  |Rewrite_branch (hyp_name) -> let step = get_step_from_index wpo hyp_name in
				rewrite_step t wpo step
  |Subset_hyps (indexes) -> mk_wpo_from_hyp_indexes t wpo indexes
  | Split_and_goal -> List.nth (split_goal_conjunction t wpo) 0
  | Split_term_disj_hyp (index) -> List.nth (split_disjunction_in_hyp t wpo index) 0
  | Remove_term_disj_goal (term_index) -> remove_term_from_disjunction_goal t
									    wpo term_index
  | Split_and_hyp (index) -> split_conjunction_in_hypothesis t wpo index
  | Contrapose index -> contrapose_hypothesis t wpo index
  | Contradict  -> contradict t wpo

(*******************)
(* Other functions *)
(*******************)

let rec rem_tactics po_gid tactics =
  match tactics with
  |[] -> []
  |hd::tl ->
    if(hd.id = po_gid) then tl else rem_tactics po_gid tl

let rec get_wpo_tactics po_gid tactics =
  match tactics with
  |[] -> []
  |hd::tl ->
    if hd.id = po_gid then hd::get_wpo_tactics po_gid tl
    else get_wpo_tactics po_gid tl

let rec get_precursor_tactics_aux wpo =
  match wpo.Wpo.po_partac with
  | None -> []
  | Some (_,p,tac) -> tac::(get_precursor_tactics_aux p)

let get_precursor_tactics wpo = List.rev (get_precursor_tactics_aux wpo)

let string_of_cond c =
  match c with
  |Goal -> "Goal"
  |AllHyp -> "AllHyp"
  |Hyp i -> "Hyp " ^ (string_of_int i)

let string_of_inf_rule t:string =
  let sb = String.concat " " in
  let n = name_of_tactic t in
  match t with
  |Rewrite_Hyp (i,d,st) -> sb[n;string_of_int i;string_of_direction d;
			      match st with
			      | None -> ""
			      | Some t -> string_of_term t
			     ]
  |UnfoldAll f -> sb[n;name_of_fun f]
  |To_Goal i -> sb[n;string_of_int i]
  |Replace (t1,t2) -> sb[n;string_of_term t1;string_of_term t2]
  |Rewrite_Lemma (l,d,st) -> sb[n;l;string_of_direction d;
				match st with | None -> "" | Some t -> string_of_term t ]
  |Nat_ind (f,ot) -> sb[n;name_of_var f;
			match ot with |None -> "" |Some term -> string_of_term term]
  |Int_ind (v1,e1) -> sb[n;name_of_var v1;string_of_term e1]
  |Unfold f -> sb[n;name_of_fun f]
  |Assert t -> sb [n;string_of_term t]
  |Use_equality_in_hypothesis (l,r,c) ->
    let s = String.concat "," [string_of_term l;string_of_term r;string_of_cond c] in
    sb [n;s]
  |Modus_ponens_hyps i -> sb [n;string_of_int i]
  |Instantiate_universal_quantifier_hyps (i,v) -> sb [n;string_of_int i;string_of_term v]
  |Instantiate_existential_quantifier_goal v -> sb [n;string_of_term v]
  |Rewrite_if_then_else_goal -> n
  |Rewrite_branch i -> sb [n;string_of_int i]
  |Subset_hyps li ->
    let lis = ref "" in
    List.iter (fun i -> lis := !lis ^ " " ^ (string_of_int i)) li;
    sb [n;!lis]
  |Split_and_goal -> n
  | Split_term_disj_hyp i -> sb [n;string_of_int i]
  | Remove_term_disj_goal i -> sb [n;string_of_int i]
  | Split_and_hyp i -> sb [n;string_of_int i]
  | Contrapose i -> sb [n;string_of_int i]
  | Contradict -> n
  | Proof_by_cases term -> sb [n;string_of_term term]
  | Apply_hypothesis_in_goal i -> sb [n;string_of_int i]
  | Intro -> n
